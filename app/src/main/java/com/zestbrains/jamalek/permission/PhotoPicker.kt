package com.zestbrains.jamalek.permission

import android.app.Activity
import android.content.Intent
import com.zestbrains.jamalek.Interfaces.GetImage


class PhotoPicker(mContext : Activity, _getImage: GetImage) {

    init {
        getImage = _getImage
        mContext.startActivity(Intent(mContext, PhotoPickerActivity::class.java))
    }

    companion object {
        lateinit var getImage: GetImage
    }

}