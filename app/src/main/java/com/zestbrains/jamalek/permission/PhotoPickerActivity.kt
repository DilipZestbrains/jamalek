package com.zestbrains.jamalek.permission

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AppCompatActivity
import com.yalantis.ucrop.UCrop
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.camerapicker.ImagePicker
import com.zestbrains.jamalek.permission.PhotoPicker.Companion.getImage
import com.zestbrains.jamalek.utils.CommonFunctions.getStr

import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class PhotoPickerActivity : BaseActivity() {

    private var profilePath: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_picker)

        chooseProfileDialog()

    }

    private fun chooseProfileDialog() {
        AlertDialog.Builder(mContext, 5).setTitle("Choose from")
            .setItems(arrayOf("Camera", "Gallery")) { _dialog, i ->
                run {
                    _dialog.dismiss()
                    when (i) {
                        1 -> chooseFromGallery()
                        0 -> chooseFromCamera()
                    }
                }
            }
            .setOnCancelListener {
                getImage.onCancel()
                finish()
                overridePendingTransition(0, 0)
            }
            .create()
            .show()
    }

    private fun chooseFromCamera() {
        ImagePicker.pickImage(this, getStr(mContext, R.string.str_select_image))
    }

    private fun chooseFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, 123)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_CANCELED) {
            getImage.onCancel()
            finish()
            overridePendingTransition(0, 0)
            return
        }
        when (requestCode) {
            RESULT_CANCELED -> {
                getImage.onCancel()
                finish()
                overridePendingTransition(0, 0)
            }
            123 -> if (data != null) {
                profilePath = getProfilePath()!!
                val op = UCrop.Options()
                op.setCircleDimmedLayer(true)
                UCrop.of(data.data!!, Uri.parse(profilePath)).withAspectRatio(1f, 1f)
                    .withOptions(op).start(mContext as AppCompatActivity)
            }
            UCrop.REQUEST_CROP -> {
                try {
                    val resultUri: Uri = UCrop.getOutput(data!!)!!
                    getImage.getImage(resultUri, profilePath)
                    finish()
                    overridePendingTransition(0, 0)
//                    civ_edit_profile_profile.setImageBitmap(null)
//                    civ_edit_profile_profile!!.setImageURI(resultUri)
                } catch (e: Exception) {
                    profilePath = ""
                    getImage.onError(profilePath)
                    finish()
                    overridePendingTransition(0, 0)
                }
            }
            ImagePicker.PICK_IMAGE_REQUEST_CODE -> {
                try {
                    val bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data)
                    profilePath = saveImage(bitmap!!)
                } catch (e: Exception) {
                    profilePath = ""
                    getImage.onError(profilePath)
                    finish()
                    overridePendingTransition(0, 0)
                }
            }
        }
    }

    private fun getProfilePath(): String? {
        val folder = File(filesDir, ".Jamlek'ste")
        if (!folder.exists()) {
            folder.mkdirs()
        }
        val f = File(folder, "profile.jpg")
        f.createNewFile()
        return f.absolutePath
    }

    private fun saveImage(myBitmap: Bitmap): String {

        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val lyftedFolder = File(filesDir, ".Jamlek'ste")
        // have the object build the directory structure, if needed.
        if (!lyftedFolder.exists()) {
            lyftedFolder.mkdirs()
        }

        try {
            val f = File(lyftedFolder, "profile.jpg")
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this, arrayOf(f.path), arrayOf("image/*"), null)
            fo.close()

            val op = UCrop.Options()
            op.setCircleDimmedLayer(true)

            UCrop.of(Uri.fromFile(f), Uri.parse(getProfilePath())).withAspectRatio(16f, 16f)
                .withOptions(op).start(mContext as AppCompatActivity)

            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

}
