package com.zestbrains.jamalek.DataSource;


import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.zestbrains.jamalek.model.login.FilterModelData;
import com.zestbrains.jamalek.restapi.ApiInterface;

import java.util.concurrent.Executor;


public class SalonsDataSourceFactory extends DataSource.Factory {
    private static final String TAG = "MoviesInTheaterDataSour";
    SalonsTheaterDataSource moviesDataSource;
    MutableLiveData<SalonsTheaterDataSource> mutableLiveData;
    Executor executor;
    ApiInterface webService;
    FilterModelData mData;

    public SalonsDataSourceFactory(Executor executor, ApiInterface webService, FilterModelData mData) {
      this.executor = executor;
      this.webService = webService;
      mutableLiveData = new MutableLiveData<>();
      this.mData=mData;
    }

    @Override
    public DataSource create() {
               // moviesDataSource = new SalonsTheaterDataSource(executor,webService,mData);
                mutableLiveData.postValue(moviesDataSource);
                return moviesDataSource;
    }

    public MutableLiveData<SalonsTheaterDataSource> getMutableLiveData() {
        return mutableLiveData;
    }
}
