package com.zestbrains.jamalek.DataSource


import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource

import com.zestbrains.jamalek.model.login.FilterModelData
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.restapi.ApiInterface


import java.util.concurrent.Executor


class HomePostDataSourceFactory(private val executor: Executor, private val webService: ApiInterface, private val context: Context,private  val mData:FilterModelData)
    : DataSource.Factory<Long, Data>() {
    val mutableLiveData: MutableLiveData<SalonsTheaterDataSource> = MutableLiveData()

    override fun create(): DataSource<Long, Data> {
        val moviesDataSource = SalonsTheaterDataSource(executor, webService, context,mData)
        mutableLiveData.postValue(moviesDataSource)
        return moviesDataSource
    }
}