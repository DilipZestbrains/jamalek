package com.zestbrains.jamalek.DataSource

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.zestbrains.jamalek.model.login.FilterModelData
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.model.salon.SalonResponse
import com.zestbrains.jamalek.restapi.NetworkState
import com.zestbrains.jamalek.restapi.ServiceGenerator
import com.zestbrains.jamalek.restapi.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor


class SalonsTheaterDataSource(private val retryExecutor: Executor, private val tmdbWebService: ApiInterface, val context: Context, private var mData: FilterModelData) :
        PageKeyedDataSource<Long, Data>() {
    val networkState: MutableLiveData<NetworkState> = MutableLiveData()
    private val initialLoading: MutableLiveData<NetworkState> = MutableLiveData()

    fun getInitialLoading(): MutableLiveData<*> {


        return initialLoading
    }

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Data>) {
        Log.d(TAG, "loadInitial: ")
        initialLoading.postValue(NetworkState.LOADING)
        networkState.postValue(NetworkState.LOADING)

        tmdbWebService.homeDetails(mData.ws_lang!!, mData.Vauthtoken!!, mData.lat!!, mData.lng!!,
                10, ServiceGenerator.API_DEFAULT_PAGE_KEY, mData.time_zone!!, mData.for_men!!, mData.for_women!!, mData.for_kids!!, mData.salon!!,
                mData.in_house!!, mData.stylist!!, mData.distance!!,mData.start_time!!,mData.category_ids!!).enqueue(object : Callback<SalonResponse> {
            override fun onResponse(call: Call<SalonResponse>, response: Response<SalonResponse>) {


                if (response.isSuccessful && response.code() == 200)
                {

                    try {

                        if(response.body()!!.data.size > 0)
                        {
                            initialLoading.postValue(NetworkState.LOADING)
                            networkState.postValue(NetworkState.LOADED)
                            callback.onResult(response.body()!!.data, null, response.body()!!.data.size.toLong())
                        }
                        else{
                            networkState.postValue(NetworkState(NetworkState.Status.FAILED, "NODATA"))
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                } else {
                    Log.e(TAG, "onResponse error " + response.message())
                    initialLoading.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                    networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                }
            }

            override fun onFailure(call: Call<SalonResponse>, t: Throwable) {
                val errorMessage = t.message
                Log.e(TAG, "onFailure: " + errorMessage!!)
                networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage))
            }
        })
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Data>) {}

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Data>) {

        if(params.key > 10){
            networkState.postValue(NetworkState.LOADING)
            tmdbWebService.homeDetails(mData.ws_lang!!, mData.Vauthtoken!!, mData.lat!!, mData.lng!!,
                    10, params.key.toInt(), mData.time_zone!!, mData.for_men!!, mData.for_women!!, mData.for_kids!!, mData.salon!!,
                    mData.in_house!!, mData.stylist!!, mData.distance!!,mData.start_time!!,mData.category_ids!!).enqueue(object : Callback<SalonResponse> {
                override fun onResponse(call: Call<SalonResponse>, response: Response<SalonResponse>) {
                    val nextKey: Long?

                    if (response.isSuccessful && response.code() == 200) {
                        initialLoading.postValue(NetworkState.LOADING)
                        networkState.postValue(NetworkState.LOADED)
                        nextKey = if (params.key % 10L !=0L) null else params.key + response.body()!!.data.size
                        callback.onResult(response.body()!!.data, nextKey)
                    } else {
                        Log.e(TAG, "onResponse error " + response.message())
                        networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                    }
                }

                override fun onFailure(call: Call<SalonResponse>, t: Throwable) {
                    val errorMessage = t.message
                    Log.e(TAG, "onFailure: " + errorMessage!!)
                    networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage))
                }
            })
        }

    }


    companion object {
        private val TAG = "MoviesInTheaterDataSou"
    }


}

