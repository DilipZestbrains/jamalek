package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.Interfaces.OnItemBookingSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.AvailabilityData
import com.zestbrains.jamalek.utils.Helper.getTimeWithAMPM


class TimeAdapter(private val onClick: OnItemBookingSelectListener, private val mActivity: Context) : RecyclerView.Adapter<TimeAdapter.MyViewHolder>() {

    var isOpenPos: Int = 0

    private var mModel = ArrayList<AvailabilityData.Time>()
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvTime)
        var rvMain: RelativeLayout = itemView.findViewById(R.id.cv_main)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_time, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        val textView = holder.tvTime

        textView.text = getTimeWithAMPM(mModel[listPosition].start!!)

        if (isOpenPos == listPosition) {
            holder.rvMain.setBackgroundDrawable(mActivity.getDrawable(R.drawable.bg_time_border))
            onClick.OnItemSelectListener(listPosition, "TIME", mModel[listPosition])
        }
        else {
            holder.rvMain.setBackgroundDrawable(mActivity.getDrawable(R.drawable.bg_time_unselected_border))
        }

        holder.rvMain.setOnClickListener {
            if (isOpenPos != listPosition) {
                isOpenPos = listPosition
                notifyDataSetChanged()

            }
        }
    }

    override fun getItemCount(): Int {
        return mModel.size
    }

    fun addAllTimes(mData: ArrayList<AvailabilityData.Time>) {
        mModel?.clear()
        mModel.addAll(mData)
//        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun getStartTime(time: String): String {

        if (mModel.size > 0) {
            return if (time == "Start")
                mModel[isOpenPos].start.toString()
            else
                mModel[isOpenPos].end.toString()
        }
        return ""
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }
}