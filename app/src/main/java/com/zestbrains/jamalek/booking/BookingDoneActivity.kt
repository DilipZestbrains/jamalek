package com.zestbrains.jamalek.booking


import android.annotation.SuppressLint
import android.os.Bundle
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_booking_done.*

class BookingDoneActivity : BaseActivity() {

    var mData: Data? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_done)


        val b = intent.extras
        if (b == null || !b.containsKey(AppConstant.SALON_DATA))
            onBackPressed()
        mData = b!!.getSerializable(AppConstant.SALON_DATA) as Data
        setData(mData!!)

        btnAddNewServices.setSafeOnClickListener { start<MainActivity>() }
    }


    @SuppressLint("SetTextI18n")
    private fun setData(mData: Data) {


        tv_item_shop_main_price.text = mData.name

        tv_location!!.text = mData.address
        tv_start_review!!.text = mData.ratings + " (" + mData.review + " " + getStr(this, R.string.reviews) + ")"


        Glide.with(this@BookingDoneActivity).load(mData.logo)
                .placeholder(getRes(this@BookingDoneActivity, R.drawable.img_place_holder))
                .error(getRes(this@BookingDoneActivity, R.drawable.img_place_holder)).into(btnshow_item_available!!)

        if (mData.vendorTiming!!.isNotEmpty())
            tv_open_time!!.text = getStr(this, R.string.open) + " (" + Helper.getTimeWithAMPM(mData.vendorTiming!![0].startTime!!) + " - " +
                    Helper.getTimeWithAMPM(mData.vendorTiming!![0].endTime!!) + ")"


        iv_kid_icon!!.setVisible((mData.forKids != "0"))
        iv_men_icon!!.setVisible((mData.forMen != "0"))
        iv_female_icon!!.setVisible((mData.forWomen != "0"))
        iv_home_icon!!.setVisible((mData.inHouse != "0"))
        iv_salon_icon!!.setVisible((mData.salon != "0"))

    }

    override fun onBackPressed() {}
}
