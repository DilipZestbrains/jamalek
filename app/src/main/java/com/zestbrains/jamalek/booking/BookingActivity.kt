package com.zestbrains.jamalek.booking


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.kal.rackmonthpicker.RackMonthPicker
import com.shreyaspatil.MaterialDialog.MaterialDialog
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog
import com.zestbrains.jamalek.Interfaces.OnItemBookingSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.booking.TemporaryBookingResponse
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.model.salon.AvailabilityData
import com.zestbrains.jamalek.model.salon.AvailabilityResponse
import com.zestbrains.jamalek.model.salon.MenuData
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.CommonFunctions.getCurrentDateTime
import com.zestbrains.jamalek.utils.CommonFunctions.getCurrentMonthYear
import kotlinx.android.synthetic.main.activity_booking.*
import java.text.SimpleDateFormat
import java.util.*

class BookingActivity : BaseActivity(), OnItemBookingSelectListener, ApiResponseInterface {

    private var picker: RackMonthPicker? = null

    private var mServiceSelected: MenuData.Services? = null
    private var menuDetails: MenuData? = null
    private var mStartTime: String? = null
    private var mEndTime: String? = null
    private var mSelectedDate: String? = null
    private var mSelectedStylistId: String? = null
    private var mBookingId: String? = null
    private var mSelectedPos: Int? = 0
    private var yearMonthPickerDialog: YearMonthPickerDialog? = null
    var mSalonHOMEANDHOUSE = 1
    var categoryID = "1"
    var mcurrentPos = 0
    private var mBookingResponse: TemporaryBookingResponse? = null

    private var isBack = false

    var mDefaultStylistSelectPos = 0


    var mServicesBookedAdapter: BookingAdapter? = null
    var mStylistAdapter: StylistAdapter? = null
    var myTimeAdapter: TimeAdapter? = null
    private var myDateAdapter: DateAdapter? = null
    private var linearLayoutManager: LinearLayoutManager? = null

    var mDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)

        val b = intent.extras
        if (b == null || !b.containsKey(AppConstant.SALON_SERVICE))
            onBackPressed()
        mServiceSelected = b!!.getSerializable(AppConstant.SALON_SERVICE) as MenuData.Services
        menuDetails = b.getSerializable(AppConstant.SALON_CATEGORY) as MenuData
        categoryID = b.getString(AppConstant.SALON_ID).toString()
        mSalonHOMEANDHOUSE = Prefs.getValue(this, AppConstant.SERVICE_FOR, 1)

        //date Picker

        tvCalendar.text = getCurrentMonthYear()

        mSelectedDate = getCurrentDateTime()
        callAvailabilityAPI()

        picker = RackMonthPicker(this)
            .setLocale(Locale.ENGLISH)
            .setPositiveButton { month, startDate, endDate, year, monthLabel ->
                val mTemp = Calendar.getInstance()
                val calendar = Calendar.getInstance()

                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, month - 1)


                if (calendar.time.before(mTemp.time)) {
                    showToast(
                        resources.getString(R.string.str_select_feature_time),
                        this@BookingActivity
                    )
                } else {
                    val dateFormat1 = SimpleDateFormat("yyyy-MM")
                    val dateFormat = SimpleDateFormat("MMMM yyyy")
                    tvCalendar.text = dateFormat.format(calendar.time)
                    mSelectedDate = dateFormat1.format(calendar.time)
                    mSelectedDate = "$mSelectedDate-01"
                    if ((mTemp.get(Calendar.MONTH) + 1) == month) {
                        mSelectedDate = getCurrentDateTime()
                    }
                    callAvailabilityAPI()
                }
            }
            .setNegativeButton { picker?.dismiss() }

//        dialogFragment = MonthYearPickerDialogFragment
//                .getInstance(monthSelected!!, yearSelected!!, System.currentTimeMillis(), maxCal.timeInMillis)
//
//        dialogFragment?.setOnDateSetListener(object : MonthYearPickerDialog.OnDateSetListener {
//            override fun onDateSet(year: Int, monthOfYear: Int) {
//                val mTemp = Calendar.getInstance()
//                val calendar = Calendar.getInstance()
//
//                calendar.set(Calendar.YEAR, year)
//                calendar.set(Calendar.MONTH, monthOfYear)
//
//
//                if (calendar.time.before(mTemp.time)) {
//                    showToast(resources.getString(R.string.str_select_feature_time), this@BookingActivity)
//                } else {
//                    val dateFormat1 = SimpleDateFormat("yyyy-MM")
//                    val dateFormat = SimpleDateFormat("MMMM yyyy")
//                    tvCalendar.text = dateFormat.format(calendar.time)
//                    mSelectedDate = dateFormat1.format(calendar.time)
//                    mSelectedDate = "$mSelectedDate-01"
//                    callAvailabilityAPI()
//                }
//            }
//        })

//        val calendar = Calendar.getInstance()
//        yearMonthPickerDialog = YearMonthPickerDialog(this, calendar, YearMonthPickerDialog.OnDateSetListener { year, month ->
//            val mTemp = Calendar.getInstance()
//            val calendar = Calendar.getInstance()
//
//            calendar.set(Calendar.YEAR, year)
//            calendar.set(Calendar.MONTH, month)
//
//
//            if (calendar.time.before(mTemp.time)) {
//                showToast(resources.getString(R.string.str_select_feature_time), this)
//            } else {
//                val dateFormat1 = SimpleDateFormat("yyyy-MM")
//                val dateFormat = SimpleDateFormat("MMMM yyyy")
//                tvCalendar.text = dateFormat.format(calendar.time)
//                mSelectedDate = dateFormat1.format(calendar.time)
//                mSelectedDate = "$mSelectedDate-01"
//                callAvailabilityAPI()
//            }
//
//        })
//        yearMonthPickerDialog!!.setMinYear(2020)

        tvCalendar.setSafeOnClickListener { selectMonth() }
        iv_order_list_back.setSafeOnClickListener { onBackPressed() }

        btnAddNewServices.setSafeOnClickListener {
            var intent = Intent(this@BookingActivity, BookingAddSerivcesActivity::class.java)
            intent.putExtra(AppConstant.SALON_SERVICE, mServiceSelected)
            intent.putExtra(AppConstant.SALON_CATEGORY, menuDetails)
            startActivityForResult(intent, 100)
        }

        btnConfirm.setSafeOnClickListener {

            if (mServicesBookedAdapter!!.itemCount > 0) {
                var intt = Intent(this@BookingActivity, ReviewAndPayActivity::class.java)
                intt.putExtra(AppConstant.BOOKING_DATA, mBookingResponse!!.data)
                startActivity(intt)
            } else
                showToast(getStr(this@BookingActivity, R.string.stylist_validation), this)

        }

        tvback.setSafeOnClickListener { scrollPre() }
        tvNext.setSafeOnClickListener { scrollNext() }

    }

    fun setData(mData: AvailabilityResponse) {
        var poss = 0
        for (i in 0 until mData.data.size) {
            if (mData.data[i].available == 1 && mData.data[i].mTime!!.size > 0) {
                poss = i
                break
            }
        }


        myDateAdapter = DateAdapter(mData.data, this, this, poss)
        linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvDateList.layoutManager = linearLayoutManager
        rcvDateList.adapter = myDateAdapter
        rcvDateList.smoothScrollToPosition(poss)
        //       Booking Service
        mServicesBookedAdapter = BookingAdapter(this, this)
        val linearLayoutManager3 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvBookingList.layoutManager = linearLayoutManager3
        rcvBookingList.adapter = mServicesBookedAdapter

        //Time

        myTimeAdapter = TimeAdapter(this, this)
        val linearLayoutManager1 = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvTimeList.layoutManager = linearLayoutManager1
        rcvTimeList.adapter = myTimeAdapter
        //Stylist

        mStylistAdapter = StylistAdapter(this, this)
        val linearLayoutManager2 = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvStylistList.layoutManager = linearLayoutManager2
        rcvStylistList.adapter = mStylistAdapter

    }

    private fun selectMonth() {
        if (mServicesBookedAdapter!!.itemCount >= 1) {
            showExitDiloguage()
            return
        }
        picker?.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && Activity.RESULT_OK == resultCode) {
            val b = data?.extras
            if (b != null || b?.containsKey(AppConstant.SALON_SERVICE)!!) {
                if (b.getString("MESSAGE") == "Update") {
                    mServiceSelected =
                        b.getSerializable(AppConstant.SALON_SERVICE) as MenuData.Services
                    menuDetails = b.getSerializable(AppConstant.SALON_CATEGORY) as MenuData
                    checkNewServiceAvailabilityAPI()
                    if (Activity.RESULT_OK == resultCode) mSalonHOMEANDHOUSE =
                        Prefs.getValue(this, AppConstant.SERVICE_FOR, 1)
                }
            }
        }
    }


    //Time  Select
    override fun OnItemSelectListener(pos: Int, title: String?, mData: AvailabilityData.Time?) {
        mStylistAdapter?.clear()
        mData?.mStylist!!.map { t -> t.isSelected = false }
        mStylistAdapter?.addAllService(mData.mStylist!!)
    }

    //Day  Select
    override fun OnItemSelectListener(pos: Int, title: String?, mData: AvailabilityData?) {

        if (title == "0") {
            rcvTimeList.gone()
            rcvStylistList.gone()
            tvNoTimeAvailable.visible()
            tvNoStylistAvailable.visible()
        } else {
            rcvTimeList.visible()
            rcvStylistList.visible()
            tvNoTimeAvailable.gone()
            tvNoStylistAvailable.gone()

            mSelectedDate = mData!!.date
            mSelectedPos = pos
            myTimeAdapter?.clear()
            mData.mTime?.let {
                myTimeAdapter?.addAllTimes(it)
            }
        }

    }

    //Stylist Select
    override fun OnItemSelectListener(pos: Int, serviceId: String?, bookingId: String?) {

        mDefaultStylistSelectPos = pos

        if (serviceId == "Stylist") {
            mSelectedStylistId = pos.toString()
            mStartTime = myTimeAdapter?.getStartTime("Start")
            mEndTime = myTimeAdapter?.getStartTime("End")

            if (mServicesBookedAdapter?.itemCount == 0)
                temporaryBooking()
            else {
                addService(mBookingId!!)
            }
        } else {
            mStylistAdapter?.unSelect(pos.toString())
            serviceId?.let { bookingId?.let { it1 -> removeService(it1, it) } }
        }

    }

    private fun callAvailabilityAPI() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().getSalonAvailability(
                    ws_lang = Prefs.getValue(this@BookingActivity, AppConstant.SELECT_LNG, "en")!!,
                    authtoken = Prefs.getValue(
                        this,
                        AppConstant.AUTHORIZATION_TOKEN, ""
                    )!!,
                    date = mSelectedDate.toString(),
                    id = menuDetails?.salon_id.toString(),
                    service_id = mServiceSelected?.id.toString()
                ),
                TYPE = WebConstant.GET_STYLIST_AVAILABILITY,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            SnackBar.show(
                this@BookingActivity,
                true,
                getStr(this@BookingActivity, R.string.str_network_error),
                false,
                "",
                null
            )
        }
    }

    //add New Service
    private fun checkNewServiceAvailabilityAPI() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().getSalonAvailability(
                    ws_lang = Prefs.getValue(this@BookingActivity, AppConstant.SELECT_LNG, "en")!!,
                    authtoken = Prefs.getValue(
                        this,
                        AppConstant.AUTHORIZATION_TOKEN, ""
                    )!!,
                    date = getCurrentDateTime(),
                    id = menuDetails?.salon_id.toString(),
                    service_id = mServiceSelected?.id.toString()
                ),
                TYPE = WebConstant.GET_ADD_NEW_STYLIST_AVAILABILITY,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            SnackBar.show(
                this@BookingActivity,
                true,
                getStr(this@BookingActivity, R.string.str_network_error),
                false,
                "",
                null
            )
        }
    }

    private fun temporaryBooking() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().generateTemporaryBooking(
                    ws_lang = Prefs.getValue(this@BookingActivity, AppConstant.SELECT_LNG, "en")!!,
                    authtoken = Prefs.getValue(this, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                    date = mSelectedDate.toString(),
                    salon_id = menuDetails?.salon_id.toString()
                ),
                TYPE = WebConstant.TEMPORARY_BOOKING,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            showToast(resources.getString(R.string.str_network_error), this)
        }
    }

    // Service
    private fun removeBookingService() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().deleteBooking(
                    Prefs.getValue(
                        this,
                        AppConstant.AUTHORIZATION_TOKEN, ""
                    )!!
                ),
                TYPE = WebConstant.BOOKING_REMOVE,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            SnackBar.show(
                this@BookingActivity,
                true,
                getStr(this@BookingActivity, R.string.str_network_error),
                false,
                "",
                null
            )
        }
    }

    // Service
    private fun addService(bookingId: String) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().addService(
                    ws_lang = Prefs.getValue(this@BookingActivity, AppConstant.SELECT_LNG, "en")!!,
                    authtoken = Prefs.getValue(
                        this,
                        AppConstant.AUTHORIZATION_TOKEN, ""
                    )!!,
                    booking_id = bookingId,
                    service_id = mServiceSelected?.id.toString(),
                    stylist_id = mSelectedStylistId.toString(),
                    date = mSelectedDate.toString(),
                    start_time = mStartTime.toString(),
                    end_time = mEndTime.toString(),
                    salon_id = menuDetails?.salon_id.toString(),
                    service_for = mSalonHOMEANDHOUSE.toString()
                ),
                TYPE = WebConstant.ADD_SERVICE,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            SnackBar.show(
                this@BookingActivity,
                true,
                getStr(this@BookingActivity, R.string.str_network_error),
                false,
                "",
                null
            )
        }
    }


    //Remove Service
    private fun removeService(bookingId: String, serviceId: String) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                activity = this,
                objectType = ApiInitialize.initialize().removeServices(
                    ws_lang = Prefs.getValue(this@BookingActivity, AppConstant.SELECT_LNG, "en")!!,
                    authtoken = Prefs.getValue(
                        this,
                        AppConstant.AUTHORIZATION_TOKEN, ""
                    )!!,
                    booking_id = bookingId,
                    additional_items_id = serviceId
                ),
                TYPE = WebConstant.REMOVE_SERVICE,
                isShowProgressDialog = true,
                apiResponseInterface = this
            )
        } else {
            SnackBar.show(
                this@BookingActivity,
                true,
                getStr(this@BookingActivity, R.string.str_network_error),
                false,
                "",
                null
            )
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.GET_STYLIST_AVAILABILITY -> {
                val response = apiResponseManager.response as AvailabilityResponse
                when (response.status) {
                    200 -> {
                        setData(response)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
            WebConstant.GET_ADD_NEW_STYLIST_AVAILABILITY -> {
                val response = apiResponseManager.response as AvailabilityResponse
                when (response.status) {
                    200 -> {
                        myTimeAdapter?.clear()
                        response.data[mSelectedPos!!].mTime?.let {
                            myTimeAdapter?.addAllTimes(it)
                        }
                        mStylistAdapter?.clear()
                        response.data[mSelectedPos!!].mTime?.get(0)
                            ?.mStylist?.let { mStylistAdapter?.addAllService(it) }
                    }
                    else -> showToast(response.message!!, this)
                }
            }
            WebConstant.TEMPORARY_BOOKING -> {
                mBookingResponse = apiResponseManager.response as TemporaryBookingResponse
                when (mBookingResponse?.status) {
                    200 -> {
                        mBookingId = mBookingResponse?.data?.id
                        addService(mBookingResponse?.data?.id.toString())
                    }
                    else -> showToast(mBookingResponse?.message!!, this)
                }
            }
            WebConstant.ADD_SERVICE -> {
                mBookingResponse = apiResponseManager.response as TemporaryBookingResponse
                when (mBookingResponse?.status) {
                    200 -> {
                        mBookingResponse?.data?.serviceList?.get(mBookingResponse?.data!!.serviceList?.size!! - 1)
                            ?.let { mServicesBookedAdapter?.addService(it) }
                    }
                    else -> {
                        showToast(mBookingResponse?.message!!, this)
                    }
                }
            }
            WebConstant.REMOVE_SERVICE -> {
                mBookingResponse = apiResponseManager.response as TemporaryBookingResponse
                when (mBookingResponse?.status) {
                    200 -> {
                        mServicesBookedAdapter?.clear()
                        mBookingResponse?.data?.serviceList?.let {
                            mServicesBookedAdapter?.addAllService(
                                it
                            )
                        }
                    }
                    else -> showToast(mBookingResponse?.message!!, this)
                }
            }
            WebConstant.BOOKING_REMOVE -> {
                val mResponse = apiResponseManager.response as CommonResponse
                when (mResponse.status) {
                    200 -> {
                        mDialog?.dismiss()
                        finish()
                    }
                    else -> showToast(mResponse.message!!, this)
                }
            }

            1000 -> {
                mStylistAdapter?.unSelect(mDefaultStylistSelectPos.toString())
            }
        }
    }

    private fun showExitDiloguage() {
        mDialog = MaterialDialog.Builder(this@BookingActivity)
            .setTitle(getStr(this@BookingActivity, R.string.app_name))
            .setMessage(getStr(this@BookingActivity, R.string.remove_booking_item))
            .setPositiveButton(
                getStr(this@BookingActivity, R.string.delete),
                R.drawable.ic_row_delete
            ) { _, i ->
                removeBookingService()
            }
            .setNegativeButton(
                getStr(this@BookingActivity, R.string.cancel),
                R.drawable.ic_delete
            ) { _, i ->
                isBack = false
                mDialog?.dismiss()
            }
            .setAnimation("remove_cancel_card.json")
            .build()

        mDialog?.show()
    }

    private fun scrollNext() {
        mcurrentPos = linearLayoutManager?.findLastCompletelyVisibleItemPosition()!!.toInt()
        mcurrentPos += 3
        if (myDateAdapter!!.itemCount > mcurrentPos) {
            rcvDateList.smoothScrollToPosition(mcurrentPos)
        } else {
            mcurrentPos = myDateAdapter!!.itemCount
            rcvDateList.smoothScrollToPosition(myDateAdapter!!.itemCount)
        }
    }

    private fun scrollPre() {

        mcurrentPos = linearLayoutManager?.findFirstCompletelyVisibleItemPosition()!!.toInt()

        mcurrentPos -= 3

        if (mcurrentPos < 0)
            mcurrentPos = 0

        if (myDateAdapter!!.itemCount > 0)
            rcvDateList.smoothScrollToPosition(mcurrentPos)
        else {
            mcurrentPos = 0
            rcvDateList.smoothScrollToPosition(0)
        }
    }

    override fun onBackPressed() {
        if (mServicesBookedAdapter!!.itemCount >= 1) {
            isBack = true
            showExitDiloguage()
        } else {
            finish()
        }
    }
}
