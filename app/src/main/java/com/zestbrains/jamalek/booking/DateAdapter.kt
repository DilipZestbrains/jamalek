package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shreyaspatil.MaterialDialog.MaterialDialog
import com.zestbrains.jamalek.Interfaces.OnItemBookingSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.model.salon.AvailabilityData
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.Helper.getDay
import java.util.*

class DateAdapter(var mModel: ArrayList<AvailabilityData>, private val onClick: OnItemBookingSelectListener,
                  private val mActivity: BookingActivity, private var availabilityData: Int) : RecyclerView.Adapter<DateAdapter.MyViewHolder>(), ApiResponseInterface {
    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.BOOKING_DAY_REMOVE -> {


                val mResponse = apiResponseManager.response as CommonResponse
                when (mResponse.status) {
                    200 -> {
                        mActivity.mServicesBookedAdapter?.clear()

                        notifyDataSetChanged()
                        mDialog?.dismiss()

                    }
                    else -> showToast(mResponse.message!!, mActivity)
                }
            }
        }
    }


    var isOpenPos: Int = availabilityData
    var firstTime: Boolean= true
    var mOldPos: Int = 1000

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvDate: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvDate)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(parent.inflate(R.layout.rv_date))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.setIsRecyclable(false)

        holder.tvDate.text = getDay(mModel[listPosition].date!!)


        if (mModel[listPosition].available == 1 && mModel[listPosition].mTime!!.size > 0) {
            holder.tvDate.isEnabled = true
            holder.tvDate.alpha = 1.0f
        } else {
            holder.tvDate.isEnabled = true
            holder.tvDate.alpha = .7f
        }


        if (firstTime && mModel[listPosition].available != 0){
            firstTime = false
            if (mModel[listPosition].available == 0) {
                onClick.OnItemSelectListener(listPosition, "0", mModel[listPosition])
            } else {
                onClick.OnItemSelectListener(listPosition, "1", mModel[listPosition])
            }
        }

        if (isOpenPos == listPosition) {
            holder.tvDate.background = mActivity.getDrawable(R.drawable.ic_solid_circle)
            holder.tvDate.setTextColor(getClr(mActivity, R.color.color_white))
//            if (mModel[listPosition].available == 0) {
//                onClick.OnItemSelectListener(listPosition, "0", mModel[listPosition])
//            } else {
//                onClick.OnItemSelectListener(listPosition, "1", mModel[listPosition])
//            }
        } else {
            holder.tvDate.background = mActivity.getDrawable(R.drawable.ic_trasparent_circle)
            holder.tvDate.setTextColor(getClr(mActivity, R.color.unselectedTabTextColor))

        }

        holder.tvDate.setSafeOnClickListener {
            if (isOpenPos != listPosition) {
                if (mActivity.mServicesBookedAdapter?.itemCount!! > 0)
                {
                    showExitDiloguage(mModel[listPosition], listPosition)
                } else {
                    isOpenPos = listPosition
                    if (mModel[listPosition].available == 0) {
                        onClick.OnItemSelectListener(listPosition, "0", mModel[listPosition])
                    } else {
                        onClick.OnItemSelectListener(listPosition, "1", mModel[listPosition])
                    }

                    notifyDataSetChanged()
                }

            }
        }
    }


    override fun getItemCount(): Int {
        return mModel.size

    }


    var mDialog: MaterialDialog? = null

    private fun showExitDiloguage(mData: AvailabilityData?, pos: Int) {
        mDialog = MaterialDialog.Builder(mActivity)
                .setTitle(getStr(mActivity, R.string.app_name))
                .setMessage(getStr(mActivity, R.string.remove_booking_item))
                .setPositiveButton(getStr(mActivity, R.string.delete), R.drawable.ic_row_delete) { _, i ->

                    isOpenPos = pos

                    removeBookingService()
                }
                .setNegativeButton(getStr(mActivity, R.string.cancel), R.drawable.ic_delete) { _, i ->
                    mDialog?.dismiss()
                }
                .setAnimation("remove_cancel_card.json")
                .build()

        mDialog?.show()
    }


    // Service
    private fun removeBookingService() {
        if (isNetworkAvailable(mActivity)) {
            Helper.hideKeyboard(mActivity)
            ApiRequest<Any>(
                    activity = mActivity,
                    objectType = ApiInitialize.initialize().deleteBooking(Prefs.getValue(mActivity,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!),
                    TYPE = WebConstant.BOOKING_DAY_REMOVE,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(mActivity, true, getStr(mActivity, R.string.str_network_error), false, "", null)
        }
    }


}