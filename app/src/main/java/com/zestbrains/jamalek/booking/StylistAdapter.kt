package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnItemBookingSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.AvailabilityData
import com.zestbrains.jamalek.utils.setVisible
import java.util.*

class StylistAdapter(private val onClick: OnItemBookingSelectListener, private val mActivity: Context) : RecyclerView.Adapter<StylistAdapter.MyViewHolder>() {


    private var mModel = ArrayList<AvailabilityData.Stylist>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvName)
        var tvProfile: de.hdodenhof.circleimageview.CircleImageView = itemView.findViewById(R.id.tvProfile)
        var ivSlectStylist: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.ivSlectStylist)
        var cv_main: RelativeLayout = itemView.findViewById(R.id.cv_main)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_stylist, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.tvName.text = mModel[listPosition].name

        Glide.with(mActivity).load(mModel[listPosition].profile_image)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_placeholder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_placeholder, null)).into(holder.tvProfile)

        holder.ivSlectStylist.setVisible(mModel[listPosition].isSelected)

        if (mModel[listPosition].isSelected) {
            holder.cv_main.isEnabled = false
            holder.tvProfile.borderWidth = 8
        } else {
            holder.cv_main.isEnabled = true
            holder.tvProfile.borderWidth = 0
        }
        holder.cv_main.setOnClickListener {

            mModel[listPosition].isSelected = true
            notifyItemChanged(listPosition)
            onClick.OnItemSelectListener(mModel[listPosition].id!!.toInt(),"Stylist", mModel[listPosition].name)
        }

    }

    override fun getItemCount(): Int {
        return mModel.size

    }


    fun addAllService(mData: ArrayList<AvailabilityData.Stylist>) {
        mModel?.clear()
       for(i in 0 until mData.size){
           mModel.add(mData[i])
           notifyItemInserted(mModel.size-1)
           notifyDataSetChanged()
       }
    }

    fun unSelect(id: String) {
       mModel.find { stylist -> stylist.id==id }?.isSelected=false
        notifyDataSetChanged()
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

}