package com.zestbrains.jamalek.booking

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.salons.AppointmentFragment
import com.zestbrains.jamalek.salons.CancelFragment
import com.zestbrains.jamalek.salons.CompletedFragment
import com.zestbrains.jamalek.salons.SheduleFragment
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.inflate
import kotlinx.android.synthetic.main.activity_provider_details.viewPagerTab
import kotlinx.android.synthetic.main.activity_provider_details.viewpager
import kotlinx.android.synthetic.main.fragment_booking.*


class BookingFragment : Fragment() {


    private lateinit var adapter: FragmentPagerItemAdapter

    private var mActivity: MainActivity? = null

    private lateinit var demoCollectionPagerAdapter: DemoCollectionPagerAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.fragment_booking)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        mActivity = this.activity as MainActivity
        demoCollectionPagerAdapter = DemoCollectionPagerAdapter(childFragmentManager,mActivity!!)
        viewpager?.adapter = demoCollectionPagerAdapter
        viewpager?.let {
            tab_layout?.setupWithViewPager(viewpager)
        }


//        loadList()
        setTabTitle()
    }

    private fun loadList() {
        adapter = FragmentPagerItemAdapter(
                mActivity!!.supportFragmentManager, FragmentPagerItems.with(mActivity)
                .add(R.string.all, AppointmentFragment::class.java)
                .add(R.string.scheduled, SheduleFragment::class.java)
                .add(R.string.completed, CompletedFragment::class.java)
                .add(R.string.cancelled, CancelFragment::class.java)
                .create())

        viewpager?.adapter = adapter

        viewpager?.let {
            tab_layout?.setupWithViewPager(viewpager)
        }

        viewPagerTab?.isSelected
        viewPagerTab?.isActivated = true




        Log.e("BookingFragment--", "loadList")


    }

    private fun setTabTitle() {
        mActivity!!.navView!!.menu.findItem(R.id.navigation_dashboard).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_booking).title = mActivity!!.resources.getString(R.string.title_booking)
        mActivity!!.navView!!.menu.findItem(R.id.navigation_notifications).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_profile).title = ""
    }


    class DemoCollectionPagerAdapter(fm: FragmentManager,val mainActivity: MainActivity) : FragmentStatePagerAdapter(fm) {

        override fun getCount(): Int = 4

        override fun getItem(i: Int): Fragment
        {
            when(i){
                0->{
                    return AppointmentFragment()
                }
                1->{
                    return SheduleFragment()
                }
                2->{
                    return CompletedFragment()
                }
                else ->{
                    return CancelFragment()
                }

            }

        }

        override fun getPageTitle(position: Int): CharSequence {
            when(position){
                0->{

                    return getStr(mainActivity,R.string.all)
                }
                1->{
                    return getStr(mainActivity,R.string.scheduled)
                }
                2->{
                    return getStr(mainActivity,R.string.completed)
                }
                else ->{
                    return getStr(mainActivity,R.string.cancelled)
                }

            }
        }
    }
}

