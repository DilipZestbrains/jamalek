package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.booking.ServiceData
import java.util.*

class ReviewandPayServiceAdapter(
    var mModel: ArrayList<ServiceData>,
    private val mActivity: Context
) : RecyclerView.Adapter<ReviewandPayServiceAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvServiceName: androidx.appcompat.widget.AppCompatTextView =
            itemView.findViewById(R.id.tvServiceName)
        var tvPrice: androidx.appcompat.widget.AppCompatTextView =
                itemView.findViewById(R.id.tvPrice)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.rv_pay_services, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.tvPrice.text = mModel[listPosition].price+" AED"
        holder.tvServiceName.text = mModel[listPosition].serviceName
    }

    override fun getItemCount(): Int {
        return mModel.size

    }

}