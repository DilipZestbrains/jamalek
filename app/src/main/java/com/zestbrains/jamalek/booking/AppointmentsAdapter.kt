package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnBookingListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.booking.MyBookingData
import com.zestbrains.jamalek.utils.Helper.getOfferEndTime
import com.zestbrains.jamalek.utils.getClr
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.gone
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class AppointmentsAdapter(private val onClick: OnBookingListener, private val mActivity: Context) : RecyclerView.Adapter<AppointmentsAdapter.MyViewHolder>() {

    private var mModel = ArrayList<MyBookingData>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mcv_item_bookmark_image: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.btnshow_item_available)
        var iv_date: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.iv_date)
        var iv_status: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.iv_status)
        var tv_services: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_services)
        var tv_item_shop_main_price: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_item_shop_main_price)
        var cv_root: RelativeLayout = itemView.findViewById(R.id.cv_root)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_appointments, parent, false)

        return MyViewHolder(view)
    }

    @SuppressLint("DefaultLocale")
    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.cv_root.setSafeOnClickListener { onClick.OnItemSelectListener(listPosition, "View", mModel[listPosition]) }

        holder.tv_item_shop_main_price.text = mModel[listPosition].name

        holder.iv_date.text = getOfferEndTime(mModel[listPosition].date.toString())


        when {
            mModel[listPosition].serviceList?.size!! == 0 -> holder.tv_services.gone()
            mModel[listPosition].serviceList?.size!! == 1 -> holder.tv_services.text = mModel[listPosition].serviceList?.get(0)!!.serviceName
            mModel[listPosition].serviceList?.size!! == 2 -> holder.tv_services.text = (mModel[listPosition].serviceList?.get(0)!!.serviceName + ", " +
                    mModel[listPosition].serviceList?.get(1)!!.serviceName)
            else -> holder.tv_services.text = (mModel[listPosition].serviceList?.get(0)!!.serviceName + ", " +
                    mModel[listPosition].serviceList?.get(1)!!.serviceName + ", " + (mModel[listPosition].serviceList?.size!!.minus(2)).toString() + "+")
        }


        if (mModel[listPosition].status.toString().toLowerCase() == "cancelled")//Cancelled
        {
            holder.iv_status.text = getStr(mActivity, R.string.status_cancelled)
            holder.iv_status.setTextColor(getClr(mActivity, R.color.colorAccent))
        } else if (mModel[listPosition].status.toString().toLowerCase() == "pending" || mModel[listPosition].status.toString().toLowerCase() == "schedule")
        {
            holder.iv_status.text = getStr(mActivity, R.string.status_schedule)
            holder.iv_status.setTextColor(getClr(mActivity, R.color.selectedTabColor))
        } else {
            holder.iv_status.text = getStr(mActivity, R.string.status_completed)
            holder.iv_status.setTextColor(getClr(mActivity, R.color.green))
        }

        Glide.with(mActivity).load(mModel[listPosition].profileImage)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(holder.mcv_item_bookmark_image)
    }


    override fun getItemCount(): Int {
        return mModel.size
    }


    fun addAllItem(mData: ArrayList<MyBookingData>) {
        for (i in 0 until mData.size) {
            mModel.add(mData[i])
            notifyItemInserted(mModel.size - 1)
            notifyDataSetChanged()
        }
    }


    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

}