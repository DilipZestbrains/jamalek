package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnItemBookingSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.booking.ServiceData
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class BookingAdapter(private val onClick: OnItemBookingSelectListener, private val mActivity: BookingActivity) :
        RecyclerView.Adapter<BookingAdapter.MyViewHolder>() {

    private var mModel = ArrayList<ServiceData>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvTime)
        var tvName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvName)
        var tvServiceName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvServiceName)
        var tvPrice: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvPrice)
        var tvProfile: de.hdodenhof.circleimageview.CircleImageView = itemView.findViewById(R.id.tvProfile)
        var ivRemove: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.ivRemove)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_stylist_book, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {
        holder.tvName.text = mModel[listPosition].name.toString()
        holder.tvServiceName.text = mModel[listPosition].serviceName.toString()
        holder.tvPrice.text = mModel[listPosition].price.toString() + " AED"
        holder.tvTime.text = (com.zestbrains.jamalek.utils.Helper.getTimeWithAMPM(mModel[listPosition].startTime.toString()) + " - " +
                com.zestbrains.jamalek.utils.Helper.getTimeWithAMPM(mModel[listPosition].endStart.toString()))

        Glide.with(mActivity).load(mModel[listPosition].profileImage)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_placeholder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_placeholder, null)).into(holder.tvProfile)

        holder.ivRemove.setSafeOnClickListener {
            onClick.OnItemSelectListener(mModel[listPosition].stylistId!!.toInt(), mModel[listPosition].id, mModel[listPosition].bookingId)
        }
    }


    override fun getItemCount(): Int {
        return mModel.size

    }

    fun addAllService(mData: ArrayList<ServiceData>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun addService(mData: ServiceData) {
        mModel.add(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

}