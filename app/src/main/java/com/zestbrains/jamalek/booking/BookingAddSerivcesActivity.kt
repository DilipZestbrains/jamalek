package com.zestbrains.jamalek.booking


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.zestbrains.jamalek.Interfaces.OnServiceItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.salon.MenuData
import com.zestbrains.jamalek.model.salon.MenuResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.salons.SaloonDetailsListAdapter
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_booking_add_serivces.*


class BookingAddSerivcesActivity : BaseActivity(), ApiResponseInterface, (View) -> Unit, OnServiceItemSelectListener {

    override fun onServiceItemSelectListener(pos: Int, title: String?, mModel: MenuData.Services?, mMune: MenuData?) {


        when (mSelectedPos) {
            "for_men" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 1)
            "for_women" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 2)
            "for_kids" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 3)
        }


        var intent = Intent()
        intent.putExtra("MESSAGE", "Update")
        intent.putExtra(AppConstant.SALON_CATEGORY, mMune)
        intent.putExtra(AppConstant.SALON_SERVICE, mModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private var menuDetails: MenuData? = null
    private lateinit var mCategoriesAdapter: SaloonDetailsListAdapter
    var mSelectedPos: String? = "for_men"
    var mActivity: Activity? = null
    private var mID: MenuData.Services? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_add_serivces)

        mActivity = this@BookingAddSerivcesActivity

        val b = intent.extras
        if (b == null || !b.containsKey(AppConstant.SALON_SERVICE))
            onBackPressed()
        mID = b!!.getSerializable(AppConstant.SALON_SERVICE) as MenuData.Services
        menuDetails = b.getSerializable(AppConstant.SALON_CATEGORY) as MenuData

        loaddata()
    }

    private fun loaddata() {
        mCategoriesAdapter = SaloonDetailsListAdapter(this@BookingAddSerivcesActivity, "Service", this)
        val linearLayoutManager = LinearLayoutManager(this@BookingAddSerivcesActivity, LinearLayoutManager.VERTICAL, false)
        Services_list.layoutManager = linearLayoutManager
        Services_list.adapter = mCategoriesAdapter
        iv_female_icon.setSafeOnClickListener(this)
        iv_kid_icon.setSafeOnClickListener(this)
        iv_men_icon.setSafeOnClickListener(this)
        iv_add_services_back.setSafeOnClickListener(this)

        callAPI()
    }

    override fun invoke(p1: View) {
        when (p1) {
            iv_men_icon -> {
                selectedCategory(1)
            }
            iv_female_icon -> {
                selectedCategory(2)
            }
            iv_kid_icon -> {
                selectedCategory(3)
            }
            iv_add_services_back -> {

                var intent = Intent()
                intent.putExtra("MESSAGE", "OK")
                setResult(100, intent)
                finish()
            }
        }
    }

    private fun callAPI() {

        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(mActivity!!)
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().getMenuBySalonId(ws_lang = Prefs.getValue(this@BookingAddSerivcesActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!, glimit = 10, goffset = 0,
                            id = menuDetails?.salon_id.toString()),
                    TYPE = WebConstant.GET_SALON_MENU_ITEM_LIST_MORE,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@BookingAddSerivcesActivity, true, getStr(this@BookingAddSerivcesActivity, R.string.str_network_error), false, "", null)
        }
    }

    private fun filterAdapter(mModel: MenuResponse) {

        mCategoriesAdapter.clear()
        var aaa: List<MenuData.Services>? = null
        mModel.data.mapIndexed { index, menuData ->
            when (mSelectedPos) {
                "for_men" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_men == 1 }
                "for_women" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_women == 1 }
                "for_kids" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_kids == 1 }
            }


            if (aaa!!.isNotEmpty()) {
                viewEmptyData.gone()
                mCategoriesAdapter.addDataITEM(menuData)
            } else {
                if (mCategoriesAdapter.itemCount == 0)
                    viewEmptyData.visible()

            }
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_SALON_MENU_ITEM_LIST_MORE -> {
                val response = apiResponseManager.response as MenuResponse
                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mActivity!!, key = AppConstant.MENU_DATA, value = response)
                        filterAdapter(response)
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
        }
    }

    private fun selectedCategory(pos: Int) {
        when (pos) {
            1 -> {

                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                mSelectedPos = "for_men"
            }
            2 -> {
                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                mSelectedPos = "for_women"
            }
            3 -> {
                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                mSelectedPos = "for_kids"
            }
        }

        val temp = Prefs.getObject(mActivity!!, AppConstant.MENU_DATA, "NODATA", MenuResponse::class.java) as MenuResponse
        filterAdapter(temp)

    }
}
