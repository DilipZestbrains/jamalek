package com.zestbrains.jamalek.booking


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.braintreepayments.cardform.view.CardForm
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.address.SelectAddressActivity
import com.zestbrains.jamalek.model.address.AddressData
import com.zestbrains.jamalek.model.booking.TemporaryBookingData
import com.zestbrains.jamalek.model.booking.TemporaryBookingResponse
import com.zestbrains.jamalek.model.salon.OfferData
import com.zestbrains.jamalek.offers.OffersAndPromotionActivity
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.ID
import com.zestbrains.jamalek.utils.AppConstant.SALON_DATA
import kotlinx.android.synthetic.main.activity_review_and_pay.*

class ReviewAndPayActivity : BaseActivity(), ApiResponseInterface {

    private lateinit var progressDialog: ProgressDialog
    private var mBookingData: TemporaryBookingData? = null
    var mSalonHOMEANDHOUSE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_and_pay)


        val b = intent.extras
        if (b == null || !b.containsKey(AppConstant.BOOKING_DATA))
            onBackPressed()
        mBookingData = b!!.getSerializable(AppConstant.BOOKING_DATA) as TemporaryBookingData
        mSalonHOMEANDHOUSE = Prefs.getValue(this, AppConstant.SALON_INHOUSE, 1)

        viewSalonOption.isEnabled = false
        viewHomeOption.isEnabled = false

        when (mSalonHOMEANDHOUSE) {
            1 -> {
                viewHomeOption.gone()
                viewAddress.gone()
            }
            2 -> {

                viewSalonOption.gone()
                viewAddress.visible()
            }
            else -> {
                selectOption(0)
                viewSalonOption.visible()
                viewHomeOption.visible()
                viewSalonOption.isEnabled = true
                viewHomeOption.isEnabled = true
            }
        }

        viewOfferSection.setSafeOnClickListener {
            val intent = Intent(this, OffersAndPromotionActivity::class.java)
                    .putExtra(AppConstant.OFFER_SELECT, "Select")
                    .putExtra(ID, mBookingData!!.salonId)
            startActivityForResult(intent, 200)
        }

        tvAddress.setSafeOnClickListener {
            val intent = Intent(this, SelectAddressActivity::class.java).putExtra(
                    AppConstant.ADDRESS_ACTION,
                    "Select"
            )
            startActivityForResult(intent, 100)
        }

        btnPay.setSafeOnClickListener {
            if (isValid()) {
                createToken()
            }
        }

        iv_pay_back.setSafeOnClickListener { onBackPressed() }


        val myServicesAdapter2 = mBookingData!!.serviceList?.let { ReviewandPayServiceAdapter(it, this) }
        val linearLayoutManager2 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvPayServicesList.layoutManager = linearLayoutManager2
        rcvPayServicesList.adapter = myServicesAdapter2

        setAmountDetails(mBookingData!!)

        card_form.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .cardholderName(CardForm.FIELD_REQUIRED)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .setup(this)


        viewSalonOption.setOnClickListener {
            selectOption(0)
        }

        viewHomeOption.setOnClickListener {
            selectOption(1)
        }

    }

    private fun createToken() {
        Helper.hideKeyboard(this)

        progressDialog = getProgressDialog(mContext)

        val card = Card.Builder(
                card_form.cardNumber,
                card_form.expirationMonth.toInt(),
                card_form.expirationYear.toInt(),
                card_form.cvv.toString()
        )

        Stripe(mContext, getStr(mContext, R.string.stripe_publish_key)).createToken(
                card.build(),
                object : TokenCallback {
                    override fun onSuccess(token: Token) {
                        dismissDialog1(mContext, progressDialog)
                        completeBooking(token.id)
                    }

                    override fun onError(error: Exception) {
                        dismissDialog1(mContext, progressDialog)
                        ShowToast.show(mContext, error.message?.ifEmpty { "Something is wrong" }!!)
                        Log.e(TAG, "onError: ${error.message} ")
                    }
                })
    }


    @SuppressLint("SetTextI18n")
    private fun setAmountDetails(mBookingData: TemporaryBookingData) {

        var promoprice = 0f
        if (mBookingData.promocodePrice == "0") {
            viewDiscount.gone()
        } else {
            viewDiscount.visible()
            dicountCode.text = getStr(this@ReviewAndPayActivity, R.string.discount_jamalek10) + " (" + mBookingData.promocode + ")"
            if (mBookingData.promocodeType == "discount") {
                promoprice = (mBookingData.price?.toFloat()?.times(mBookingData.promocodePrice!!.toFloat()))?.div(100)!!
                tv_jamalek_discount.text = "-$promoprice AED"
            } else {
                promoprice = mBookingData.promocodePrice!!.toFloat()
                tv_jamalek_discount.text = "-" + mBookingData.promocodePrice + " AED"
            }

        }


        val serviceCharge = (mBookingData.price?.toFloat()?.times(mBookingData.serviceCharge!!.toFloat()))?.div(100)
        tv_service_charge.text = serviceCharge.toString() + " AED"
        val total = serviceCharge?.let { mBookingData.price?.toFloat()?.plus(it) }
        tv_grand_total.text = promoprice.let { total?.minus(it).toString() + " AED" }

    }


    //Remove Service
    private fun completeBooking(mToke: String) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().completeBooking(ws_lang = Prefs.getValue(this@ReviewAndPayActivity, AppConstant.SELECT_LNG, "en")!!,
                            authtoken = Prefs.getValue(this, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            booking_id = mBookingData?.id.toString(), stripe_token = mToke),
                    TYPE = WebConstant.COMPLETED_BOOKING,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@ReviewAndPayActivity, true, getStr(this@ReviewAndPayActivity, R.string.str_network_error), false, "", null)
        }
    }


    //Remove Service
    private fun addAddress(mAddress: AddressData) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().addAddressToBooking(ws_lang = Prefs.getValue(this@ReviewAndPayActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            booking_id = mBookingData?.id.toString(), address = mAddress.address.toString(), house_number = mAddress.house_number.toString()
                            , landmark = mAddress.landmark.toString(), instructions = mAddress.instructions.toString(), longitude = mAddress.longitude.toString(),
                            latitude = mAddress.latitude.toString()),
                    TYPE = WebConstant.ADD_ADDRESS_BOOKING,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@ReviewAndPayActivity, true, getStr(this@ReviewAndPayActivity, R.string.str_network_error), false, "", null)
        }
    }

    private fun applyPromocode(mOffer: OfferData) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().applyPromocode(ws_lang = Prefs.getValue(this@ReviewAndPayActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            booking_id = mBookingData?.id.toString(), promocode_id = mOffer.id.toString()),
                    TYPE = WebConstant.APPLY_PROMOCODE,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@ReviewAndPayActivity, true, getStr(this@ReviewAndPayActivity, R.string.str_network_error), false, "", null)
        }
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.ADD_ADDRESS_BOOKING -> {
                val response = apiResponseManager.response as TemporaryBookingResponse
                when (response.status) {
                    200 -> {
                        tvAddress.text = response.data.address
                    }
                    else -> showToast(response.message!!, this)
                }
            }

            WebConstant.APPLY_PROMOCODE -> {
                val response = apiResponseManager.response as TemporaryBookingResponse
                when (response.status) {
                    200 -> {
                        showToast(response.message!!, this)
                        tvOffers.text = response.data.promocode
                        setAmountDetails(response.data)
                    }
                    else -> showToast(response.message!!, this)
                }
            }

            WebConstant.COMPLETED_BOOKING -> {
                val response = apiResponseManager.response as TemporaryBookingResponse
                when (response.status) {
                    200 -> {
                        val detailIntent = Intent(this@ReviewAndPayActivity, BookingDoneActivity::class.java)
                        detailIntent.putExtra(SALON_DATA, response.data.mSalonData)
                        startActivity(detailIntent)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> {
                val b = data!!.extras

                try {
                    if (b != null || b!!.containsKey("ACTION")) {

                        if (b.get("ACTION").toString() == "NOSELECT") {
                            return
                        } else {
                            if (b.containsKey(AppConstant.ADDRESS_DATA)) {
                                val data = b.getSerializable(AppConstant.ADDRESS_DATA) as AddressData
                                addAddress(data)
                            }
                            return
                        }
                    }
                } catch (e: java.lang.Exception) {
                }
            }
            200 -> {
                val b = data!!.extras
                if (b != null || b!!.containsKey("Action")) {
                    if (b.getString("Action").toString() == "Select") {
                        val data = b.getSerializable(AppConstant.OFFER_DATA) as OfferData
                        applyPromocode(data)
                    }

                }
                return
            }
        }
    }

    private fun selectOption(pos: Int) {
        if (pos == 0) {
            viewSalonOption.background = getDrawable(R.drawable.login_register_field_bg)
            ivSalon.setImageDrawable(getDrawable(R.drawable.ic_store_selected))
            tv_inSalon.setTextColor(getClr(this@ReviewAndPayActivity, R.color.colorStrat))

            viewAddress.gone()

            viewHomeOption.background = getDrawable(R.drawable.bg_skip_button)
            ivHome.setImageDrawable(getDrawable(R.drawable.ic_house))
            tv_Home_option.setTextColor(getClr(this@ReviewAndPayActivity, R.color.textcolor))
        } else {
            viewAddress.visible()
            viewSalonOption.background = getDrawable(R.drawable.bg_skip_button)
            ivSalon.setImageDrawable(getDrawable(R.drawable.ic_store))
            tv_inSalon.setTextColor(getClr(this@ReviewAndPayActivity, R.color.textcolor))

            viewHomeOption.background = getDrawable(R.drawable.login_register_field_bg)
            ivHome.setImageDrawable(getDrawable(R.drawable.ic_house_selected))
            tv_Home_option.setTextColor(getClr(this@ReviewAndPayActivity, R.color.colorStrat))
        }
    }


    private fun isValid(): Boolean {
        return when {

            (TextUtils.isEmpty(tvAddress.text.toString().trim()) && viewAddress.isVisible) -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_address_selecte), false, "", null)
                return false
            }

            TextUtils.isEmpty(card_form.cardholderName) -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_name_validation), false, "", null)
                return false
            }

            TextUtils.isEmpty(card_form.cardNumber) -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_number_validation), false, "", null)
                return false
            }

            TextUtils.isEmpty(card_form.expirationMonth) || TextUtils.isEmpty(card_form.expirationYear) -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_month_year_validation), false, "", null)
                return false
            }

            TextUtils.isEmpty(card_form.cvv) -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_cv_validation), false, "", null)
                return false
            }

            card_form.cardNumber.length !in 13..19 -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_number_valide), false, "", null)
                return false
            }
            card_form.cvv.length !in 3..4 -> {
                SnackBar.show(mContext, true, getStr(this@ReviewAndPayActivity, R.string.str_card_cv_valide), false, "", null)
                return false
            }

            else -> true
        }
    }
}
