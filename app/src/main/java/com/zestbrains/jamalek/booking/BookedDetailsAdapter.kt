package com.zestbrains.jamalek.booking

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.booking.ServiceData
import com.zestbrains.jamalek.utils.getRes
import com.zestbrains.jamalek.utils.inflate

class BookedDetailsAdapter(private val mActivity: Activity, var mModel: ArrayList<ServiceData>) : RecyclerView.Adapter<BookedDetailsAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvServiceName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvServiceName)
        var tvServiceAmount: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvServiceAmount)
        var tvTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvTime)
        var tvStylistProfile: de.hdodenhof.circleimageview.CircleImageView = itemView.findViewById(R.id.tvStylistProfile)
        var tvStylistName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvStylistName)
        var tvServiceIcon: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.tvServiceIcon)
        var rvMain: RelativeLayout = itemView.findViewById(R.id.cv_main)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(parent.inflate(R.layout.rv_booked_services))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {


        holder.tvServiceName.text = mModel[listPosition].serviceName
        holder.tvStylistName.text = mModel[listPosition].name
        holder.tvServiceAmount.text = mModel[listPosition].price+ " AED"


        Glide.with(mActivity).load(mModel[listPosition].profileImage)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(holder.tvStylistProfile)

        holder.tvTime.text = (com.zestbrains.jamalek.utils.Helper.getTimeWithAMPM(mModel[listPosition].startTime.toString()) +" - "+
                com.zestbrains.jamalek.utils.Helper.getTimeWithAMPM(mModel[listPosition].endStart.toString()))


        when {
            mModel[listPosition].serviceFor == 1 -> holder.tvServiceIcon.setImageDrawable(getRes(mActivity, R.drawable.ic_men))
            mModel[listPosition].serviceFor == 2 -> holder.tvServiceIcon.setImageDrawable(getRes(mActivity, R.drawable.ic_women))
            mModel[listPosition].serviceFor == 3 -> holder.tvServiceIcon.setImageDrawable(getRes(mActivity, R.drawable.ic_kid_new))
        }


    }

    override fun getItemCount(): Int {
        return mModel.size
    }

}