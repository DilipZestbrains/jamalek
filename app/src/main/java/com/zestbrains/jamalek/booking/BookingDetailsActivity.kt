package com.zestbrains.jamalek.booking


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.model.booking.MyBookingData
import com.zestbrains.jamalek.model.booking.SingleBookingResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.salons.ProviderDetailsActivity
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_booking_details.*


class BookingDetailsActivity : BaseActivity(), ApiResponseInterface {

    private var mBookingData: MyBookingData? = null
    private var mCallFrom: String = "App"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)


        val b = intent.extras
        if (b != null) {
            when {
                b.containsKey(AppConstant.BOOKING_DATA) -> {
                    mBookingData = b.getSerializable(AppConstant.BOOKING_DATA) as MyBookingData
                    setAmountDetails(mBookingData!!)
                }
                b.containsKey(AppConstant.ID) -> {
                    if (b.containsKey(AppConstant.CALL_FROM))
                    {
                        mCallFrom = b.getString(AppConstant.CALL_FROM).toString()
                    }
                    var id = b.getString(AppConstant.ID)
                    getBookingdata(id.toString())
                }
                else -> onBackPressed()
            }
        } else
            onBackPressed()

        iv_booked_back.setSafeOnClickListener {
            if (mCallFrom == "Push") {
                finish()
                start<MainActivity>()
            } else
                finish()
        }

        viewSalonDetails.setSafeOnClickListener {
            val detailIntent = Intent(this@BookingDetailsActivity, ProviderDetailsActivity::class.java)
            detailIntent.putExtra(AppConstant.SALON_ID, mBookingData?.salonId)
            startActivity(detailIntent)
            finish()
        }
    }

    private fun setAmountDetails(mBookingData: MyBookingData) {

        Glide.with(this@BookingDetailsActivity).load(mBookingData.profileImage)
                .placeholder(ResourcesCompat.getDrawable(resources, R.drawable.img_place_holder, null))
                .error(ResourcesCompat.getDrawable(resources, R.drawable.img_place_holder, null)).into(iv_salon_logo)


        when {
            mBookingData.serviceList?.size!! == 0 -> tv_services.gone()
            mBookingData.serviceList?.size!! == 1 -> tv_services.text = mBookingData.serviceList?.get(0)!!.serviceName
            mBookingData.serviceList?.size!! == 2 -> tv_services.text = (mBookingData.serviceList?.get(0)!!.serviceName + ", " +
                    mBookingData.serviceList?.get(1)!!.serviceName)
            else -> tv_services.text = (mBookingData.serviceList?.get(0)!!.serviceName + ", " +
                    mBookingData.serviceList!![1].serviceName + ", " + (mBookingData.serviceList?.size!!.minus(2)).toString() + "+")
        }

        if (mBookingData.status.toString().toLowerCase() == "cancelled") {
            btn_booking_details.text = getStr(this@BookingDetailsActivity, R.string.status_cancelled)

        } else if (mBookingData.status.toString().toLowerCase() == "pending" ||
                mBookingData.status.toString().toLowerCase() == "schedule") {
            btn_booking_details.text = getStr(this@BookingDetailsActivity, R.string.status_schedule)

        } else {
            btn_booking_details.text = getStr(this@BookingDetailsActivity, R.string.status_completed)
        }

        tv_item_shop_name.text = mBookingData.name
        tv_Booking_id.text = mBookingData.bookingId
        tv_title.text = "#" + mBookingData.bookingId
        tv_Booking_date_time.text = Helper.getOfferEndTime(mBookingData.date.toString())

        tv_Booking_info.text = getStr(this@BookingDetailsActivity, R.string.booking_information)
        viewChooseCategory.text = getStr(this@BookingDetailsActivity, R.string.booked_services)
        tvTotalLabel.text = getStr(this@BookingDetailsActivity, R.string.grand_total)
        tvServiceChnagesLabel.text = getStr(this@BookingDetailsActivity, R.string.service_charge)
        tvDateLabel.text = getStr(this@BookingDetailsActivity, R.string.date_time)
        tvbookingIdlabel.text = getStr(this@BookingDetailsActivity, R.string.booking_id)


        var promoprice = 0f
        if (mBookingData.promocodePrice == "0") {
            viewDiscount.gone()
        } else {
            viewDiscount.visible()
            dicountCode.text = getStr(this@BookingDetailsActivity, R.string.discount_jamalek10) + " (" + mBookingData.promocode + ")"
            if (mBookingData.promocodeType == "discount") {
                promoprice = (mBookingData.price?.toFloat()?.times(mBookingData.promocodePrice!!.toFloat()))?.div(100)!!
                tv_jamalek_discount.text = "-$promoprice AED"
            } else {
                promoprice = mBookingData.promocodePrice!!.toFloat()
                tv_jamalek_discount.text = "-" + mBookingData.promocodePrice + " AED"
            }

        }


        val serviceCharge = (mBookingData.price?.toFloat()?.times(mBookingData.serviceCharge!!.toFloat()))?.div(100)
        tv_service_charge.text = serviceCharge.toString() + " AED"
        val total = serviceCharge?.let { mBookingData.price?.toFloat()?.plus(it) }
        tv_grand_total.text = promoprice.let { total?.minus(it).toString() + " AED" }


        val myServicesAdapter2 = mBookingData.serviceList?.let { ReviewandPayServiceAdapter(it, this) }
        val linearLayoutManager2 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvPayServicesList.layoutManager = linearLayoutManager2
        rcvPayServicesList.adapter = myServicesAdapter2


        val myServicesAdapter1 = mBookingData.serviceList?.let { BookedDetailsAdapter(this, it) }
        val linearLayoutManager1 = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rcvBookedServicesList.layoutManager = linearLayoutManager1
        rcvBookedServicesList.adapter = myServicesAdapter1

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI()
    }

    private fun getBookingdata(mId: String) {
        if (isNetworkAvailable()) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().get_single_booking(ws_lang = "en", authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            booking_id = mId.toString()),
                    TYPE = WebConstant.BOOKING_DETAIL,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@BookingDetailsActivity, true, getStr(this@BookingDetailsActivity, R.string.str_network_error), false, "", null)
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.BOOKING_DETAIL -> {
                val response = apiResponseManager.response as SingleBookingResponse
                when (response.status) {
                    200 -> {
                        mBookingData = response.data
                        setAmountDetails(mBookingData!!)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
        }
    }

    override fun onBackPressed() {
        if (mCallFrom == "Push")
        {
            finishAffinity()
            start<MainActivity>()
        } else
            finish()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE)

    }
}
