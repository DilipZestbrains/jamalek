package com.zestbrains.jamalek.salons

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.LoginActivity
import com.zestbrains.jamalek.model.salon.ReviewsResponse
import com.zestbrains.jamalek.model.salon.WriteReviewsResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.SALON_ID
import com.zestbrains.jamalek.utils.AppConstant.SALON_OWN_RATING
import com.zestbrains.jamalek.utils.AppConstant.SALON_RATINGS
import com.zestbrains.jamalek.utils.AppConstant.SALON_REVIEW
import kotlinx.android.synthetic.main.bottomsheet_add_review.*
import kotlinx.android.synthetic.main.fragment_saloon_reviews.*


class SaloonReviewsFragment : Fragment(), ApiResponseInterface {

    private var mID: String? = null
    private var mRatings: String? = null
    private var mReview: String? = null
    private var mRatingCount: Float = 0f
    private var mOwnRated: String = "0"


    private lateinit var mReviewsAdapter: RatingReviewAdapter
    var mActivity: ProviderDetailsActivity? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            mID = it.getString(SALON_ID)
            mRatings = it.getString(SALON_RATINGS)
            mReview = it.getString(SALON_REVIEW)
            mOwnRated = it.getString(SALON_OWN_RATING).toString()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return container!!.inflate(R.layout.fragment_saloon_reviews)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = activity as ProviderDetailsActivity

        loadData()

        btnWriteReview.setSafeOnClickListener {
            if (mActivity!!.bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                mActivity!!.bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {

                mActivity!!.bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
            }
        }


        mActivity!!.bottomSheetBehavior.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {

                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mActivity!!.rating_bar_bottom.setStar(0f)
                    mActivity!!.tv_positive_comment.setText("")
                    mActivity!!.tv_negitive_comment.setText("")
                    hideKeyboard(mActivity!!)

                }

            }
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

                if (slideOffset == 0.0f) {
                    mActivity!!.rating_bar_bottom.setStar(0f)
                    mActivity!!.tv_positive_comment.setText("")
                    mActivity!!.tv_negitive_comment.setText("")
                    hideKeyboard(mActivity!!)

                }
            }
        })
    }

    private fun loadData() {

        tv_start_review.text = mRatings + " (" + mReview + " " + getStr(mActivity!!, R.string.reviews) + ")"
        mReviewsAdapter = RatingReviewAdapter(mActivity!!)
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        rvcReviewList.layoutManager = linearLayoutManager
        rvcReviewList.adapter = mReviewsAdapter



        if (mOwnRated != "0") {
            btnWriteReview.isEnabled = false
            btnWriteReview.alpha = .5f
        }

        callAPI()

        mActivity!!.rating_bar_bottom.setOnRatingChangeListener { RatingCount ->
            mRatingCount = RatingCount
        }

        mActivity!!.btnReviewSubmit.setSafeOnClickListener {

            if (Prefs.contain(mActivity!!, AppConstant.AUTHORIZATION_TOKEN)) {
                if (isNetworkAvailable(mActivity!!)) {
                    Helper.hideKeyboard(mActivity!!)
                    if (isValid()) {
                        ApiRequest<Any>(
                                activity = mActivity!!,
                                objectType = ApiInitialize.initialize().writeReviewBySalonId(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!,
                                        AppConstant.AUTHORIZATION_TOKEN, "")!!, glimit = mRatingCount, dislike_description = getDisLikeComment(),
                                        like_description = getLikeComment(),
                                        id = 0, salon_id = mID!!),
                                TYPE = WebConstant.PUT_SALON_REVIEW,
                                isShowProgressDialog = true,
                                apiResponseInterface = this)
                    }
                } else {
                    showToast(resources.getString(R.string.str_network_error), mActivity!!)
                }
            } else {
                showToast(getStr(mActivity!!, R.string.please_login_to_continue), mActivity!!)
                mActivity!!.finishAffinity()
                mActivity!!.start<LoginActivity>()
            }
        }


    }


    private fun callAPI() {

        if (isNetworkAvailable(mActivity!!)) {
            Helper.hideKeyboard(mActivity!!)
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().getReviewBySalonId(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!, glimit = 10, goffset = 0,
                            id = mID!!),
                    TYPE = WebConstant.GET_SALON_REVIEW,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mActivity!!)
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_SALON_REVIEW -> {
                val response = apiResponseManager.response as ReviewsResponse
                when (response.status) {
                    200 -> {
                        mReviewsAdapter.addData(response.data)
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
            WebConstant.PUT_SALON_REVIEW -> {
                val response = apiResponseManager.response as WriteReviewsResponse
                when (response.status) {
                    200 -> {
                        mReviewsAdapter.addData(response.data, 0)
                        btnWriteReview.isEnabled = false
                        btnWriteReview.alpha = .5f

                        if (mActivity!!.bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                            mActivity!!.bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                        }
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
        }
    }


    private fun isValid(): Boolean {
        Helper.hideKeyboard(mActivity!!)

        return when {
            mRatingCount == 0f -> {
                Helper.hideKeyboard(mActivity!!)
                SnackBar.show(mActivity!!, true, getStr(mActivity!!, R.string.str_validation_ratings_error), false, "", null)
                false
            }

            TextUtils.isEmpty(getLikeComment()) && TextUtils.isEmpty(getDisLikeComment()) -> {
                Helper.hideKeyboard(mActivity!!)
                SnackBar.show(mActivity!!, true, getStr(mActivity!!, R.string.str_validation_comment), false, "", null)
                false
            }

            else -> true
        }
    }

    private fun getLikeComment(): String {
        return mActivity!!.tv_positive_comment.text.toString().trim()
    }

    private fun getDisLikeComment(): String {
        return mActivity!!.tv_negitive_comment.text.toString().trim()
    }

}
