package com.zestbrains.jamalek.salons


import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.ReviewsData
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.inflate
import com.zestbrains.jamalek.utils.setVisible
import java.util.*

class RatingReviewAdapter(private val mActivity: Context) :
        RecyclerView.Adapter<RatingReviewAdapter.MyViewHolder>() {

    private val mModel = ArrayList<ReviewsData>()


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var label_user_name: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.label_user_name)
        var label_like_description: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.label_like_description)
        var label_dis_like_description: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.label_dis_like_description)
        var label_reviews_date: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.label_reviews_date)
        var image_user_profile: de.hdodenhof.circleimageview.CircleImageView = itemView.findViewById(R.id.image_user_profile)
        var rating_bar: com.hedgehog.ratingbar.RatingBar = itemView.findViewById(R.id.rating_bar)

    }

    override fun onCreateViewHolder(viewHolder: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(viewHolder.inflate(R.layout.rv_rating_reviews))
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        Glide.with(viewHolder.itemView).load(mModel[listPosition].profile_image).placeholder(mActivity.getDrawable(R.drawable.img_placeholder)).into(viewHolder.image_user_profile)
        viewHolder.label_user_name.text = mModel[listPosition].name
        viewHolder.label_reviews_date.text = Helper.getDateAndTimeForOrderDetails(mModel[listPosition].created.toString())
        viewHolder.label_like_description.text = mModel[listPosition].like_description
        viewHolder.label_dis_like_description.text = mModel[listPosition].dislike_description
        viewHolder.rating_bar.setStar(mModel[listPosition].rating!!.toFloat())
        viewHolder.rating_bar.isEnabled = false
        viewHolder.rating_bar.setmClickable(false)
        viewHolder.rating_bar.setOnClickListener(null)


        viewHolder.label_dis_like_description.setVisible(mModel[listPosition].dislike_description!!.isNotEmpty())
        viewHolder.label_like_description.setVisible(mModel[listPosition].like_description!!.isNotEmpty())


    }


    fun addData(mData: ArrayList<ReviewsData>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun addData(mData: ReviewsData, i: Int) {
        mModel.add(0, mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun addDataITEM(mData: ReviewsData) {
        mModel.add(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return mModel.size
    }
}