package com.zestbrains.jamalek.salons


import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnServiceItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.MenuData
import com.zestbrains.jamalek.utils.inflate
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class SaloonDetailsListAdapter(private val mActivity: Activity,private val mAction: String,var onClick: OnServiceItemSelectListener) : RecyclerView.Adapter<SaloonDetailsListAdapter.MyViewHolder>()
{

    var isOpenPos: Int = -99
    private val mModel = ArrayList<MenuData>()


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var iv_UpDown: androidx.appcompat.widget.AppCompatImageView? = null
        var rvServices: RecyclerView
        var tv_title: androidx.appcompat.widget.AppCompatTextView
        var iv_icon: androidx.appcompat.widget.AppCompatImageView

        init {
            this.iv_UpDown = itemView.findViewById(R.id.iv_UpDown)
            this.rvServices = itemView.findViewById(R.id.rvServices)
            this.tv_title = itemView.findViewById(R.id.tv_title)
            this.iv_icon = itemView.findViewById(R.id.iv_icon)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(parent.inflate(R.layout.rv_header_list))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        val myServicesAdapter = PriceServicesAdapter(mModel[listPosition].mServices!!,mModel[listPosition], onClick, mActivity,mAction)
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        holder.rvServices.layoutManager = linearLayoutManager
        holder.rvServices.adapter = myServicesAdapter
        holder.tv_title.text = mModel[listPosition].name

        Glide.with(mActivity).load(mModel[listPosition].image)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.ic_hair_cut, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.ic_hair_cut, null)).into(holder.iv_icon)

        if (isOpenPos == listPosition) {

            holder.iv_UpDown!!.setImageDrawable(mActivity.getDrawable(R.drawable.ic_collapes))
            holder.rvServices.visibility = View.VISIBLE
            holder.tv_title.setTextColor(ContextCompat.getColor(mActivity, R.color.selectedTabColor))

        } else {
            holder.rvServices.visibility = View.GONE
            holder.iv_UpDown!!.setImageDrawable(mActivity.getDrawable(R.drawable.ic_down_arror))
            holder.tv_title.setTextColor(ContextCompat.getColor(mActivity, R.color.unselectedTabTextColor))
        }
        holder.iv_UpDown!!.setSafeOnClickListener {
            if (holder.rvServices.visibility == View.VISIBLE)
            {
                holder.rvServices.visibility = View.GONE
                holder.iv_UpDown!!.setImageDrawable(mActivity.getDrawable(R.drawable.ic_down_arror))
                holder.tv_title.setTextColor(ContextCompat.getColor(mActivity, R.color.unselectedTabTextColor))

            } else {
                isOpenPos = listPosition
                notifyDataSetChanged()
            }
        }
    }

    fun addData(mData: ArrayList<MenuData>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun addDataITEM(mData: MenuData) {
        mModel.add(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }


    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return mModel.size
    }

}