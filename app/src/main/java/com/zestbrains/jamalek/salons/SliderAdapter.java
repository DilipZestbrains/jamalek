package com.zestbrains.jamalek.salons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.zestbrains.jamalek.R;
import com.zestbrains.jamalek.model.salon.Data;

import java.util.ArrayList;

public class SliderAdapter extends SliderViewAdapter<SliderAdapter.SliderAdapterVH> {

    private Context context;
    private String mTSId;
    ArrayList<Data.GalleryImages> mData;

    public SliderAdapter(Context context, ArrayList<Data.GalleryImages> galleryImages,String tsid) {
        this.context = context;
        this.mTSId = tsid;
        this.mData = galleryImages;

    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        viewHolder.imageViewBackground.setTransitionName(mTSId);
        if (mData.size() == 0)
        {
            Glide.with(viewHolder.itemView).load(context.getDrawable(R.drawable.img_place_holder)).into(viewHolder.imageViewBackground);
        } else {
            Glide.with(viewHolder.itemView).load(mData.get(position).getImage()).placeholder(context.getDrawable(R.drawable.img_place_holder)).into(viewHolder.imageViewBackground);
        }
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size

        if (mData.size() == 0)
            return 1;
        return mData.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.Show_item_available);

            this.itemView = itemView;
        }
    }
}
