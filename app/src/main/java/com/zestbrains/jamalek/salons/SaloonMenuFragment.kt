package com.zestbrains.jamalek.salons

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.zestbrains.jamalek.Interfaces.OnServiceItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.booking.BookingActivity
import com.zestbrains.jamalek.model.salon.MenuData
import com.zestbrains.jamalek.model.salon.MenuResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.SALON_ID
import kotlinx.android.synthetic.main.fragment_saloon_menu.*


class SaloonMenuFragment : Fragment(), ApiResponseInterface, (View) -> Unit, OnServiceItemSelectListener {


    private var mID: String? = null
    private lateinit var mCategoriesAdapter: SaloonDetailsListAdapter
    var mActivity: Activity? = null
    var mSelectedPos: String? = "for_men"


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.fragment_saloon_menu)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.let {
            mID = it.getString(SALON_ID)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mActivity = activity

        loaddata()
        callAPI()

        iv_female_icon.setSafeOnClickListener(this)
        iv_kid_icon.setSafeOnClickListener(this)
        iv_men_icon.setSafeOnClickListener(this)
    }

    private fun loaddata() {
        mCategoriesAdapter = SaloonDetailsListAdapter(mActivity!!, "Menu", this)
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        home_item_list.layoutManager = linearLayoutManager
        home_item_list.adapter = mCategoriesAdapter
    }

    override fun invoke(p1: View) {
        when (p1) {
            iv_men_icon -> {
                selectedCategory(1)
            }
            iv_female_icon -> {
                selectedCategory(2)
            }
            iv_kid_icon -> {
                selectedCategory(3)
            }
        }
    }

    private fun callAPI() {

        if (isNetworkAvailable(mActivity!!)) {
            Helper.hideKeyboard(mActivity!!)
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().getMenuBySalonId(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            glimit = 10, goffset = 0,
                            id = mID!!),
                    TYPE = WebConstant.GET_SALON_MENU_ITEM_LIST,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mActivity!!)
        }
    }

    private fun filterAdapter(mModel: MenuResponse) {

        mCategoriesAdapter.clear()
        var aaa: List<MenuData.Services>? = null
        mModel.data.mapIndexed { index, menuData ->
            when (mSelectedPos) {
                "for_men" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_men == 1 }
                "for_women" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_women == 1 }
                "for_kids" -> aaa = menuData.mServices!!.filterIndexed { indexService, services -> services.for_kids == 1 }
            }


            if (aaa!!.isNotEmpty()) {
                viewEmptyData.gone()
                mCategoriesAdapter.addDataITEM(menuData)
            } else {
                if (mCategoriesAdapter.itemCount == 0)
                    viewEmptyData.visible()

            }
        }
    }

    override fun onServiceItemSelectListener(pos: Int, title: String?, mModel: MenuData.Services?, mMune: MenuData?) {

        when (mSelectedPos) {
            "for_men" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 1)
            "for_women" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 2)
            "for_kids" -> Prefs.setValue(context = mActivity!!, key = AppConstant.SERVICE_FOR, value = 3)
        }
        var intt = Intent(mActivity, BookingActivity::class.java)
        intt.putExtra(AppConstant.SALON_CATEGORY, mMune)
        intt.putExtra(AppConstant.SALON_SERVICE, mModel)
        intt.putExtra(SALON_ID, mID)
        mActivity!!.startActivity(intt)

    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_SALON_MENU_ITEM_LIST -> {
                val response = apiResponseManager.response as MenuResponse
                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mActivity!!, key = AppConstant.MENU_DATA, value = response)
                        filterAdapter(response)
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
        }
    }

    private fun selectedCategory(pos: Int) {
        when (pos) {
            1 -> {

                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                mSelectedPos = "for_men"
            }
            2 -> {
                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                mSelectedPos = "for_women"
            }
            3 -> {
                iv_men_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_female_icon.background = mActivity!!.getDrawable(R.drawable.bg_unselected_categoty)
                iv_kid_icon.background = mActivity!!.getDrawable(R.drawable.bg_selected_categoty)
                mSelectedPos = "for_kids"
            }
        }

        val temp = Prefs.getObject(mActivity!!, AppConstant.MENU_DATA, "NODATA", MenuResponse::class.java) as MenuResponse
        filterAdapter(temp)

    }
}
