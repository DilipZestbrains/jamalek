package com.zestbrains.jamalek.salons

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.zestbrains.jamalek.Interfaces.OnBookingListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.booking.AppointmentsAdapter
import com.zestbrains.jamalek.booking.BookingDetailsActivity
import com.zestbrains.jamalek.model.booking.MyBookingData
import com.zestbrains.jamalek.model.booking.MyBookingResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.fragment_all_appointment.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class SheduleFragment : Fragment(), ApiResponseInterface, OnBookingListener {

    private var param1: String? = null
    private var param2: String? = null
    private var isUSERVISIBLE: Boolean = false

    private lateinit var mAppointmentAdapter: AppointmentsAdapter
    var mActivity: Activity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mActivity = activity
        return container!!.inflate(R.layout.fragment_all_appointment)
    }

    override fun onStart() {
        super.onStart()
        mActivity = activity
        isUSERVISIBLE = true
        callAPI()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = activity

        rvcAppointment.setLoadingListener(object : XRecyclerView.LoadingListener {
            override fun onRefresh() {
                mAppointmentAdapter.clear()
                rvcAppointment.refreshComplete()
                callAPI()
            }

            override fun onLoadMore() {
                callAPI(mAppointmentAdapter.itemCount)
            }
        })
    }

    private fun callAPI() {
        if (isNetworkAvailable(mActivity!!)) {
            mAppointmentAdapter = AppointmentsAdapter(this, mActivity!!)
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
            rvcAppointment.layoutManager = linearLayoutManager
            rvcAppointment.adapter = mAppointmentAdapter

            ApiRequest<Any>(activity = mActivity!!, objectType = ApiInitialize.initialize().myBooking(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                    limit = 10, offset = 0,
                    type = 1),
                    TYPE = WebConstant.MY_SHAEDULE_BOOKING,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mActivity!!)
        }
    }

    private fun callAPI(pos: Int) {
        if (isNetworkAvailable(mActivity!!)) {
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().myBooking(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            limit = 10, offset = pos,
                            type = 1),
                    TYPE = WebConstant.MY_BOOKING_Shedule_LOAD_MORE,
                    isShowProgressDialog = false,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mActivity!!)
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.MY_SHAEDULE_BOOKING -> {
                val response = apiResponseManager.response as MyBookingResponse
                when (response.status) {
                    200 -> {
                        try {
                            mAppointmentAdapter.clear()
                            mAppointmentAdapter.addAllItem(response.data)
                            if (response.data.size >= 1)
                                tvNodata.gone()
                            else tvNodata.visible()
                        } catch (e: Exception) {
                        }
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
            WebConstant.MY_BOOKING_Shedule_LOAD_MORE -> {
                val response = apiResponseManager.response as MyBookingResponse

                try {
                    rvcAppointment.loadMoreComplete()
                }catch (e: java.lang.Exception){

                }
                when (response.status) {
                    200 -> {
                        mAppointmentAdapter.addAllItem(response.data)
                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }
        }

    }

    companion object {

        @JvmStatic
        fun newInstance() =
                SheduleFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, "")
                        putString(ARG_PARAM2, "")
                    }
                }
    }

    override fun OnItemSelectListener(pos: Int, title: String?, mModel: MyBookingData?) {

        var intt = Intent(mActivity!!, BookingDetailsActivity::class.java)
        intt.putExtra(AppConstant.BOOKING_DATA, mModel)
        startActivity(intt)

    }

    override fun setMenuVisibility(menuVisible: Boolean)
    {
        if (mActivity != null && isUSERVISIBLE) {
            if (menuVisible)
                callAPI()
        }
        super.setMenuVisibility(menuVisible)
    }

}
