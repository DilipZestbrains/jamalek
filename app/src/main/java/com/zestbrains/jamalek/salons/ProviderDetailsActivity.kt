package com.zestbrains.jamalek.salons


import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.widget.NestedScrollView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.model.salon.SalonByIDResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.SALON_DATA
import com.zestbrains.jamalek.utils.AppConstant.SALON_DESCRIPTION
import com.zestbrains.jamalek.utils.AppConstant.SALON_ID
import com.zestbrains.jamalek.utils.AppConstant.SALON_OWN_RATING
import com.zestbrains.jamalek.utils.AppConstant.SALON_RATINGS
import com.zestbrains.jamalek.utils.AppConstant.SALON_REVIEW
import kotlinx.android.synthetic.main.activity_provider_details.*
import kotlinx.android.synthetic.main.bottomsheet_add_review.*
import java.lang.Exception
import java.text.DecimalFormat


class ProviderDetailsActivity : BaseActivity(), ApiResponseInterface {

    private var changeStatusColor = false

    var mTransId: String = ""
    var mData: Data? = null
    var InHouseAndSolon = 1

    lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }

        setContentView(R.layout.activity_provider_details)
        findViewById<View>(R.id.container).setPadding(0, 0, 0, getNavBarHeight(mContext))

        nsv_green_grocer_details.isFillViewport = true


        intent.extras?.let {
            when {
                it.containsKey(SALON_ID) -> {
                    val id = it.getString(SALON_ID) as String
                    callAPI(id)
                }
                it.containsKey(SALON_DATA) -> {
                    mData = it.getSerializable(SALON_DATA) as Data
                    setData(mData!!)
                }
                else -> onBackPressed()
            }
        }

        //mTransId = b.getString("trans_id") as String

        iv_shop_back.setOnClickListener { finish() }
        ivBackArrow.setOnClickListener { finish() }


        changeStatusBarColor()

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_ll)

        bottomSheetBehavior.halfExpandedRatio = 0.7f

        bottomSheetBehavior.isHideable = false

        appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBar, offset ->
            when {
                appBar.totalScrollRange == Math.abs(offset) -> {
                    // Fully Collapsed
                    cl.visibility = View.INVISIBLE
                    tvTitleHead.text = tv_item_shop_main_price.text.toString().trim()
                    toolbarNodata.visibility = View.VISIBLE
                }
                0 == offset -> {
                    // fully expanded
                    cl.visibility = View.VISIBLE
                    tvTitleHead.text = ""
                    toolbarNodata.visibility = View.GONE
                }
                else -> {
                    // Not fully expanded or collapsed
                    cl.visibility = View.VISIBLE
                    tvTitleHead.text = ""
                    toolbarNodata.visibility = View.GONE
                }
            }
        })

    }


    @SuppressLint("SetTextI18n")
    private fun setData(mData: Data) {


        iv_shop_image.sliderAdapter = SliderAdapter(this, mData.gallery_images, mTransId)
        tv_item_shop_main_price.text = mData.name

        try {
            if (mData.distance!!.isNotEmpty()) {
                tv_location!!.text = mData.address + " - " + DecimalFormat("##.##").format(mData.distance!!.toDouble()) + getStr(this, R.string.km)
            } else {
                tv_location!!.text = mData.address
            }
        } catch (e: Exception) {
            tv_location!!.text = mData.address
        }


        tv_start_review!!.text = mData.ratings + " (" + mData.review + " " + getStr(this, R.string.reviews) + ")"

        if (mData.vendorTiming!!.isNotEmpty())
            tv_open_time!!.text = getStr(this, R.string.open) + " (" + Helper.getTimeWithAMPM(mData.vendorTiming!![0].startTime!!) + " to " +
                    Helper.getTimeWithAMPM(mData.vendorTiming!![0].endTime!!) + ")"



        iv_kid_icon!!.setVisible((mData.forKids != "0"))
        iv_men_icon!!.setVisible((mData.forMen != "0"))
        iv_female_icon!!.setVisible((mData.forWomen != "0"))
        iv_home_icon!!.setVisible((mData.inHouse != "0"))
        iv_salon_icon!!.setVisible((mData.salon != "0"))

        if (mData.inHouse == "1" && mData.salon == "1")
            InHouseAndSolon = 3
       else if (mData.inHouse != "1" && mData.salon != "0")
            InHouseAndSolon = 1
       else if (mData.inHouse != "0" && mData.salon != "1")
            InHouseAndSolon = 2
       else if (mData.inHouse != "0" && mData.salon != "0")
            InHouseAndSolon = 0

        Prefs.setValue(context = mContext, key = AppConstant.SALON_INHOUSE, value = InHouseAndSolon)


        val bd = Bundle()
        bd.putString(SALON_ID, mData.id)
        bd.putString(SALON_RATINGS, mData.ratings)
        bd.putString(SALON_REVIEW, mData.review)
        bd.putString(SALON_OWN_RATING, mData.own_rated)
        bd.putString(SALON_DESCRIPTION, mData.details)
        bd.putString(SALON_DESCRIPTION, mData.details)


        val adapter = FragmentPagerItemAdapter(supportFragmentManager, FragmentPagerItems.with(this)
                .add(R.string.manu, SaloonMenuFragment::class.java, bd)
                .add(R.string.reviews, SaloonReviewsFragment::class.java, bd)
                .add(R.string.details, SaloonDetailsFragment::class.java, bd)
                .create())

        viewpager.adapter = adapter
        viewPagerTab.setViewPager(viewpager)


        iv_home_Direction.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + GPSTracker(this@ProviderDetailsActivity).latitude
                        + "," + GPSTracker(this@ProviderDetailsActivity).longitude + "&daddr=" + mData.latitude + "," + mData.longitude))
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                showToast("Activity not found", this@ProviderDetailsActivity)
            }
        }


    }


    private fun changeStatusBarColor() {
        nsv_green_grocer_details.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->

            if (!changeStatusColor && scrollY > 10) {
                changeStatusColor = true
                val colorFrom = Color.TRANSPARENT
                val colorTo = getClr(mContext, R.color.colorAccent)
                val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
                colorAnimation.duration = 500 // milliseconds
                colorAnimation.addUpdateListener {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.statusBarColor = it.animatedValue as Int
                    }
                }
                colorAnimation.start()
            }

            if (changeStatusColor && scrollY < 10) {
                changeStatusColor = false
                val colorFrom = Color.TRANSPARENT
                val colorTo = getClr(mContext, R.color.colorAccent)
                val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorTo, colorFrom)
                colorAnimation.duration = 500 // milliseconds
                colorAnimation.addUpdateListener {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.statusBarColor = it.animatedValue as Int
                    }
                }
                colorAnimation.start()
            }
        })
    }


    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI()

    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (

                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }


    private fun callAPI(id: String) {

        if (isNetworkAvailable(this@ProviderDetailsActivity)) {
            Helper.hideKeyboard(this@ProviderDetailsActivity)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().getSingleSalon(ws_lang = "en", authtoken = Prefs.getValue(this, AppConstant.AUTHORIZATION_TOKEN, "")!!,
                            time_zone = Prefs.getValue(this, AppConstant.TIME_ZONE, "Asia/Kolkata")!!, id = id),
                    TYPE = WebConstant.GET_SALON_BY_ID,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mContext)
        }
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_SALON_BY_ID -> {
                val mSalonResponse = apiResponseManager.response as SalonByIDResponse
                when (mSalonResponse.status) {
                    200 -> {

                        mData = mSalonResponse.data
                        setData(mSalonResponse.data)

                    }
                    else -> showToast(mSalonResponse.message!!, this)
                }
            }
        }
    }

}
