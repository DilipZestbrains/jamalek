package com.zestbrains.jamalek.salons

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.VendorCategoies
import com.zestbrains.jamalek.utils.inflate
import java.util.*

class ServicesListAdapter(var mModel: ArrayList<VendorCategoies>, private val mActivity: Context) : RecyclerView.Adapter<ServicesListAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var iv_service_icon: AppCompatImageView = itemView.findViewById(R.id.iv_service_icon)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(parent.inflate(R.layout.rv_services))
    }

    override fun onBindViewHolder(holder: MyViewHolder,
                                  @SuppressLint("RecyclerView") listPosition: Int) {


        Glide.with(mActivity).load(mModel[listPosition].image)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.ic_hair_cut, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.ic_hair_cut, null)).into(holder.iv_service_icon)


    }

    override fun getItemCount(): Int {
        return mModel.size
    }

}