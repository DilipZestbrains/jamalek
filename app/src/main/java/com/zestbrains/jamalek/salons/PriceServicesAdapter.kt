package com.zestbrains.jamalek.salons

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.Interfaces.OnServiceItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.MenuData
import com.zestbrains.jamalek.utils.setSafeOnClickListener


class PriceServicesAdapter(var mModel: ArrayList<MenuData.Services>, var mMenuModel: MenuData, var onClick: OnServiceItemSelectListener, private val mActivity: Context, private val mAction: String) : RecyclerView.Adapter<PriceServicesAdapter.MyViewHolder>() {
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cv_root: RelativeLayout = itemView.findViewById(R.id.cv_root)
        var tv_item_title: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_item_title)
        var tv_item_price: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_item_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_details_list, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {


        holder.cv_root.setSafeOnClickListener {
            onClick.onServiceItemSelectListener(listPosition, mAction, mModel[listPosition], mMenuModel)
        }

        holder.tv_item_price.text = mModel[listPosition].price
        holder.tv_item_title.text = mModel[listPosition].name
    }


    override fun getItemCount(): Int {
        return mModel.size
    }
}