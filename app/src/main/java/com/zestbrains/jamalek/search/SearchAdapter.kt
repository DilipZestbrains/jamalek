package com.zestbrains.jamalek.search

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import clipz.customer.interfaces.ClickFilterListener
import com.zestbrains.jamalek.Interfaces.OnItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.SalonListData
import com.zestbrains.jamalek.utils.inflate


class SearchAdapter(private val onClick: OnItemSelectListener, private val mActivity: Activity, var countries: List<SalonListData>, var listner: ClickFilterListener) :
        RecyclerView.Adapter<SearchAdapter.MyViewHolder>(), Filterable {


    private var countriesCopy: List<SalonListData> = countries
    private var filter: CustomFilter? = null

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvSalonName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvSalonName)
        var rvMain: RelativeLayout = itemView.findViewById(R.id.cv_main)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(parent.inflate(R.layout.rv_search))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {
        holder.tvSalonName.text = countries[listPosition].name
        holder.rvMain.setOnClickListener { onClick.OnItemSelectListener(listPosition, countries[listPosition].id) }

    }

    override fun getItemCount(): Int {
        return countries.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = CustomFilter(countriesCopy, this)
        }

        return filter as CustomFilter
    }


    inner class CustomFilter(internal var filterList: List<SalonListData>, internal var adapter: SearchAdapter) : Filter()
    {
        @RequiresApi(api = Build.VERSION_CODES.N)
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            var constraint = constraint
            val results = FilterResults()
            if (constraint != null && constraint.isNotEmpty()) {
                constraint = constraint.toString().toUpperCase()
                val filteredPlayers = java.util.ArrayList<SalonListData>()
                for (i in filterList.indices) {
                    if (filterList[i].name.toString().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList[i])
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = filterList.size
                results.values = filterList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            adapter.countries = results.values as List<SalonListData>
            adapter.notifyDataSetChanged()
            if (adapter.itemCount == 0) { listner.onEmpty(1001) } else { listner.onEmpty(1002) }
        }
    }

}