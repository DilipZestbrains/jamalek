package com.zestbrains.jamalek.search


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import clipz.customer.interfaces.ClickFilterListener
import com.zestbrains.jamalek.Interfaces.OnItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.salon.SalonListResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.salons.ProviderDetailsActivity
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : BaseActivity(), ApiResponseInterface, OnItemSelectListener, ClickFilterListener {

    private lateinit var maAdepter: SearchAdapter
    private lateinit var mSalonResponse: SalonListResponse


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        img_back.setSafeOnClickListener { finish() }

        callAvailabilityAPI()

        iv_home_f_filter.setSafeOnClickListener {}

        actext_namaprov.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable)
            {
                maAdepter.filter.filter(s.toString())
                if (maAdepter.itemCount == 0)
                {
                    tvEmptyData.visible()
                } else {
                    tvEmptyData.gone()
                }
            }
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }
        })

    }


    private fun callAvailabilityAPI() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().getAllSalon(),
                    TYPE = WebConstant.GET_ALL_SALON,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        }
        else {
            SnackBar.show(this@SearchActivity, true,
                    getStr(this@SearchActivity, R.string.str_network_error), false, "", null)
        }
    }

    override fun OnItemSelectListener(pos: Int, title: String?) {

        Helper.hideKeyboard(this)

        val detailIntent = Intent(this@SearchActivity, ProviderDetailsActivity::class.java)
        detailIntent.putExtra(AppConstant.SALON_ID, title)
        startActivity(detailIntent)
        finish()
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_ALL_SALON -> {
                mSalonResponse = apiResponseManager.response as SalonListResponse
                when (mSalonResponse.status) {
                    200 -> {
                        setAdapter(mSalonResponse)
                    }
                    else -> showToast(mSalonResponse.message!!, this)
                }
            }
        }
    }

    private fun setAdapter(mData: SalonListResponse) {

        maAdepter = SearchAdapter(this, this, mData.data, this)
        val mLayoutManager = LinearLayoutManager(this@SearchActivity, RecyclerView.VERTICAL, false)
        rvSearch!!.layoutManager = mLayoutManager
        rvSearch!!.itemAnimator = DefaultItemAnimator()
        rvSearch!!.adapter = maAdepter
    }

    override fun onEmpty(mTitle: Int) {
        if (mTitle == 1001) {
            tvEmptyData.visible()
        } else if (mTitle == 1002) {
            tvEmptyData.gone()

        }
    }
}
