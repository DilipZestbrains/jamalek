package com.zestbrains.jamalek.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.facebook.FacebookSdk
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.zestbrains.jamalek.Interfaces.PermissionStatus
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.common.CustomInfoWindowGoogleMap
import com.zestbrains.jamalek.dashboard.DashboardFragment
import com.zestbrains.jamalek.dashboard.DashboardFragment.Companion.mSalonAdapter
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.permission.PermissionChecker
import com.zestbrains.jamalek.salons.ProviderDetailsActivity
import com.zestbrains.jamalek.utils.*


class MapsFragment : Fragment(), OnMapReadyCallback, PermissionStatus {

    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private var mActivity: MainActivity? = null
    private var mMap: GoogleMap? = null

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0

        mMap!!.isMyLocationEnabled = true

        val mDrawable = FacebookSdk.getApplicationContext().resources.getDrawable(R.drawable.ic_pin_salon)
        val myLocation = LatLng(GPSTracker(mActivity!!).latitude, GPSTracker(mActivity!!).longitude)
        //mMap?.addMarker(MarkerOptions().position(myLocation).title(GPSTracker(mActivity!!).city).icon(BitmapDescriptorFactory.fromBitmap(drawableToBitmap(mDrawable))))
        val cameraPosition = CameraPosition.Builder().target(myLocation).zoom(7f).build()
        mMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun allGranted() {

    }


    override fun onDenied() {
        ShowToast.show(mActivity!!, CommonFunctions.getStr(mActivity!!, R.string.deny_permission))

    }

    override fun foreverDenied() {
        SnackBar.show(mActivity, CommonFunctions.getStr(mActivity!!, R.string.enable_location_permission), CommonFunctions.getStr(mActivity!!, R.string.enable), View.OnClickListener {

        })
    }

    private fun checkForPermission() {
        PermissionChecker(mActivity!!, this@MapsFragment,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val rootView = inflater.inflate(R.layout.fragment_blank, container, false)

        mMapView = rootView.findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)

        mMapView!!.onResume()

        try {
            MapsInitializer.initialize(activity!!.getApplicationContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap

            // For showing a move to my location button
            googleMap!!.isMyLocationEnabled = false
            val myLocation = LatLng(GPSTracker(mActivity!!).latitude, GPSTracker(mActivity!!).longitude)
            val cameraPosition = CameraPosition.Builder().target(myLocation).zoom(12f).build()
            googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            onSalonLoad()
        }

        return rootView
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mActivity = this.activity as MainActivity
    }

    fun onSalonLoad() {
        Thread {
            try {
                if (mSalonAdapter.itemCount > 0) {
                    for (i in 0 until DashboardFragment.mSalonAdapter.itemCount) {
                        val bitmapDescriptor = LoadMarkerBitmapDescriptor()
                        bitmapDescriptor.execute(mSalonAdapter.getITEMs(i))
                    }
                    Prefs.setObject(context = mActivity!!, key = AppConstant.HOME_DATA, value = mSalonAdapter.currentList as Any)
                }
            } finally {
                Log.e("MAPPPA ","Finellle==="+mSalonAdapter.itemCount )
            }
        }.start()
    }

    @SuppressLint("StaticFieldLeak")
    private open inner class LoadMarkerBitmapDescriptor : AsyncTask<Data, Void, MarkerOptions>() {

        var model: Data? = null

        @SafeVarargs
        override fun doInBackground(vararg params: Data): MarkerOptions {

            val kmlmarkeroptions = params[0]
            model = kmlmarkeroptions
            var snowqualmie: LatLng? = null
            snowqualmie = if (model!!.latitude == "" && model!!.longitude == "") {
                LatLng(23.54, 72.51)
            } else {
                LatLng(model!!.latitude!!.toDouble(), model!!.longitude!!.toDouble())
            }


            val markerOptions = MarkerOptions()
            val mDrawable = FacebookSdk.getApplicationContext().resources.getDrawable(R.drawable.ic_pin_salon)
            markerOptions.position(snowqualmie).title(model!!.name).icon(BitmapDescriptorFactory.fromBitmap(Helper.drawableToBitmap(mDrawable)))

            return markerOptions
        }

        override fun onPostExecute(markerOptionsList: MarkerOptions) {
            addIcontoMap(markerOptionsList)
        }

        protected fun addIcontoMap(markerOptionsList: MarkerOptions) {

            val m = googleMap!!.addMarker(markerOptionsList)
            m.tag = model
            val customInfoWindow = CustomInfoWindowGoogleMap(mActivity!!)
            googleMap!!.setInfoWindowAdapter(customInfoWindow)
            googleMap!!.setOnInfoWindowClickListener { marker ->
                marker.showInfoWindow()

                val mModel = marker.tag as Data?

                val detailIntent = Intent(mActivity, ProviderDetailsActivity::class.java)
                detailIntent.putExtra(AppConstant.SALON_DATA, mModel)
                mActivity!!.startActivity(detailIntent)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MapsFragment().apply {

                }
    }
}
