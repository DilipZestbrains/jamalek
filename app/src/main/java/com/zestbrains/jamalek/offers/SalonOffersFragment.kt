package com.zestbrains.jamalek.offers

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.zestbrains.jamalek.Interfaces.OnOfferItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.OfferData
import com.zestbrains.jamalek.model.salon.OfferResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.fragment_salon_offers.*


class SalonOffersFragment : Fragment(), OnOfferItemSelectListener, ApiResponseInterface {

    private var Id: String? = null
    private var mAction: String? = ""
    private lateinit var mAppointmentAdapter: SalonOffersAdapter
    var mView: View? = null
    var mActivity: Activity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            Id = it.getString(AppConstant.SALON_ID)
            mAction = it.getString(AppConstant.OFFER_SELECT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_salon_offers, container, false)
        return mView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = activity

        mAppointmentAdapter = SalonOffersAdapter(this, mActivity!!)
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        rvcOffers.layoutManager = linearLayoutManager
        rvcOffers.adapter = mAppointmentAdapter

        callAPI()

    }


    private fun callAPI() {

        if (isNetworkAvailable(mActivity!!)) {
            Helper.hideKeyboard(mActivity!!)
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().getOfferSlon(ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(mActivity!!,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!, type = 1, salon_id = Id.toString(), offset = 0, limit = 10),
                    TYPE = WebConstant.GET_SALON_OFFER,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(mActivity, true, getStr(mActivity!!, R.string.str_network_error), false, "", null)
        }
    }


    override fun OnOfferItemSelectListener(pos: Int, title: String?, mModel: OfferData?) {
        if (mAction == "Select")
        {
            val intent = Intent()
            intent.putExtra("Action", mAction)
            intent.putExtra(AppConstant.OFFER_DATA, mModel)
            mActivity?.setResult(200, intent)
            mActivity?.finish()
        }

    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_SALON_OFFER -> {
                val response = apiResponseManager.response as OfferResponse
                when (response.status) {
                    200 -> {
                        Log.e("List : ", response.data.size.toString() + "----")
                        mAppointmentAdapter.load(response.data)

                        tvNodata.setVisible(response.data.size <= 0)


                    }
                    else -> showToast(response.message!!, mActivity!!)
                }
            }

        }
    }
}
