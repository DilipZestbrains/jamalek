package com.zestbrains.jamalek.offers


import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.utils.AppConstant
import com.zestbrains.jamalek.utils.AppConstant.ID
import kotlinx.android.synthetic.main.activity_offers_and_promotion.*


class OffersAndPromotionActivity : BaseActivity() {

    private var mAction: String? = null
    private var mSlonID: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        setContentView(R.layout.activity_offers_and_promotion)

        iv_offer_promo_back.setOnClickListener { onBack() }

        val b = intent.extras
        if (b == null || !b.containsKey(AppConstant.OFFER_SELECT))
            onBackPressed()
        mAction = b?.getString(AppConstant.OFFER_SELECT)
        mSlonID = b?.getString(ID).toString()

        loadList()

    }

    override fun onBackPressed() {
        onBack()
    }


    fun onBack() {
        if (mAction == "Select") {
            val intent = Intent()
            intent.putExtra("Action", "NoSelecte")
            setResult(200, intent)
        }
        finish()
    }

    private fun loadList() {

        val bd = Bundle()
        bd.putString(AppConstant.SALON_ID, mSlonID)
        bd.putString(AppConstant.OFFER_SELECT, mAction)

        val adapter = FragmentPagerItemAdapter(
                supportFragmentManager, FragmentPagerItems.with(this)
                .add(R.string.offers, SalonOffersFragment::class.java, bd)
                .add(R.string.jamlekoffers, JamalekOffersFragment::class.java, bd)
                .create())

        viewpager.adapter = adapter

        viewPagerTab.setViewPager(viewpager)
    }
}
