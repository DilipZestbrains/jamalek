package com.zestbrains.jamalek.offers

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.Interfaces.OnOfferItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.OfferData
import com.zestbrains.jamalek.utils.Helper.getOfferEndTime
import com.zestbrains.jamalek.utils.inflate
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class JamalekOffersAdapter(private val onClick: OnOfferItemSelectListener, private val mActivity: Context) :
        RecyclerView.Adapter<JamalekOffersAdapter.MyViewHolder>() {


    private var mModel = ArrayList<OfferData>()


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_title: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_title)
        //var tv_details: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_details)
        var tv_item_offers: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tv_item_offers)
        var tvValidTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvValidTime)
        var tvValidDate: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvValidDate)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(parent.inflate(R.layout.rv_jamalek_offers))
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        viewHolder.tv_title.text = mModel[listPosition].title
        // viewHolder.tv_details.text = mModel[listPosition].description
        viewHolder.tv_item_offers.text = mModel[listPosition].code
        viewHolder.tvValidTime.text = mModel[listPosition].description
        viewHolder.tvValidDate.text = "Offer valid till "+getOfferEndTime(mModel[listPosition].end_date.toString())

        viewHolder.tv_item_offers.setSafeOnClickListener {
            onClick.OnOfferItemSelectListener(listPosition, "Select", mModel[listPosition])
        }
    }

    override fun getItemCount(): Int {
        return mModel.size
    }

    fun load(mData: ArrayList<OfferData>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun add(mData: OfferData, pos: Int) {
        if (pos == 0) {
            mModel.add(mData)
            notifyItemInserted(mModel.size - 1)
            notifyDataSetChanged()
        } else {
            mModel.add(pos, mData)
            notifyItemChanged(pos)
        }

    }


}