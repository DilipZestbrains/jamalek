package com.zestbrains.jamalek.common


import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.salons.ServicesListAdapter
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.setVisible1
import com.zestbrains.jamalek.utils.visible
import java.text.DecimalFormat

class CustomInfoWindowGoogleMap(private val mActivity: Context) : GoogleMap.InfoWindowAdapter {


    internal lateinit var view: View

    override fun getInfoWindow(marker: Marker): View {


        view = (mActivity as Activity).layoutInflater.inflate(R.layout.map_custom_infowindow, null)

        val root = view.findViewById<androidx.constraintlayout.widget.ConstraintLayout>(R.id.root)
        val ivLogo = view.findViewById<de.hdodenhof.circleimageview.CircleImageView>(R.id.img_logo)
        val tvName = view.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_item_shop_main_name)
        val tvRreview = view.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_start_review)
        val tvLocation = view.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_location)
        val tvOpenTime = view.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_open_time)
        val rvServices = view.findViewById<RecyclerView>(R.id.rvServices)

        val ivMen = view.findViewById<ImageView>(R.id.iv_men_icon)
        val ivWoman = view.findViewById<ImageView>(R.id.iv_female_icon)
        val ivKids = view.findViewById<ImageView>(R.id.iv_kid_icon)
        val icHome = view.findViewById<ImageView>(R.id.iv_home_icon)
        val ivSalon = view.findViewById<ImageView>(R.id.iv_salon_icon)

        val mModel = marker.tag as Data?


        if (mModel!!.vendorCategory!!.isNotEmpty()) {
            val myServicesAdapter = ServicesListAdapter(mModel.vendorCategory!!, mActivity)
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
            rvServices.layoutManager = linearLayoutManager
            rvServices.adapter = myServicesAdapter
            rvServices.visible()
        }


        //cv_root!!.setSafeOnClickListener { onClick.onSalonItemSelectListener(0, btnshow_item_available, mModel) }
        tvName!!.text = mModel.name
        tvLocation!!.text = mModel.address + " -" + DecimalFormat("##.##").format(mModel.distance!!.toDouble()) + getStr(mActivity, R.string.km)
        tvRreview!!.text = mModel.ratings + " (" + mModel.review + " " + getStr(mActivity, R.string.reviews) + ")"

        if (mModel.vendorTiming!!.isNotEmpty())
            tvOpenTime!!.text = getStr(mActivity, R.string.open) + " (" + Helper.getTimeWithAMPM(mModel.vendorTiming!![0].startTime!!) + " to " +
                    Helper.getTimeWithAMPM(mModel.vendorTiming!![0].endTime!!) + ")"


        ivKids!!.setVisible1((mModel.forKids != "0"))
        ivMen!!.setVisible1((mModel.forMen != "0"))
        ivWoman!!.setVisible1((mModel.forWomen != "0"))
        icHome!!.setVisible1((mModel.inHouse != "0"))
        ivSalon!!.setVisible1((mModel.salon != "0"))

        Glide.with(mActivity).load(mModel.logo)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(ivLogo!!)

        return view
    }

    override fun getInfoContents(marker: Marker): View {

        return view
    }
}