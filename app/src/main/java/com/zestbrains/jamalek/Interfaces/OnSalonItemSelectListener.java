package com.zestbrains.jamalek.Interfaces;



import android.view.View;

import com.zestbrains.jamalek.model.salon.Data;

public interface OnSalonItemSelectListener {

    void onSalonItemSelectListener(int pos, View mView, Data mModel);
    void onSalonLoad();


}
