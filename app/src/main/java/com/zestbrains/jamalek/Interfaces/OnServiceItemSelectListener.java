package com.zestbrains.jamalek.Interfaces;



import android.view.View;

import com.zestbrains.jamalek.model.salon.Data;
import com.zestbrains.jamalek.model.salon.MenuData;

public interface OnServiceItemSelectListener {

    void onServiceItemSelectListener(int pos, String title, MenuData.Services mModel, MenuData mMune);

}
