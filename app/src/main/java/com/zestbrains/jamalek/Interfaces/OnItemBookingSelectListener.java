package com.zestbrains.jamalek.Interfaces;


import com.zestbrains.jamalek.model.salon.AvailabilityData;

public interface OnItemBookingSelectListener {

    void OnItemSelectListener(int pos, String profile,String name);
    void OnItemSelectListener(int pos, String title, AvailabilityData mData);
    void OnItemSelectListener(int pos, String title, AvailabilityData.Time mData);

}
