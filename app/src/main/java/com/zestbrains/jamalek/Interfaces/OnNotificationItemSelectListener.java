package com.zestbrains.jamalek.Interfaces;


import com.zestbrains.jamalek.model.notification.NotificationData;

public interface OnNotificationItemSelectListener {

    void OnItemSelectListener(int pos, String title, NotificationData mData);


}
