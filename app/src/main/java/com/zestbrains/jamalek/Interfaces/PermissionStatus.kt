package com.zestbrains.jamalek.Interfaces

interface PermissionStatus {
    fun allGranted()
    fun onDenied()
    fun foreverDenied()
}