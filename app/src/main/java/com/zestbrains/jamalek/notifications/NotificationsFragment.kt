package com.zestbrains.jamalek.notifications

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jcodecraeer.xrecyclerview.XRecyclerView
import com.zestbrains.jamalek.Interfaces.OnNotificationItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.booking.BookingDetailsActivity
import com.zestbrains.jamalek.model.notification.NotificationData
import com.zestbrains.jamalek.model.salon.NotificationResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.fragment_notifications.*

class NotificationsFragment : Fragment(), OnNotificationItemSelectListener, ApiResponseInterface {
    override fun OnItemSelectListener(pos: Int, title: String?, mData: NotificationData?) {

        var intt = Intent(mActivity!!, BookingDetailsActivity::class.java)
        intt.putExtra(AppConstant.CALL_FROM, "NotificationsFragment")
        intt.putExtra(AppConstant.ID, mData!!.booking_id)
        startActivity(intt)
    }


    private lateinit var notificationsViewModel: NotificationsViewModel

    private lateinit var mAppointmentAdapter: NotificationsAdapter

    private var mActivity: MainActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel::class.java)

        return container!!.inflate(R.layout.fragment_notifications)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = this.activity as MainActivity

        setData()

        callAPI(true, 0)


        rvcNotification?.setLoadingListener(object : XRecyclerView.LoadingListener {
            override fun onRefresh() {
                mAppointmentAdapter.clear()
                callAPI(false, 0)
            }

            override fun onLoadMore() {
                callAPI(false, mAppointmentAdapter.itemCount)// load more data here
            }
        })


    }


    private fun callAPI(mShowDia: Boolean, mOffset: Int) {

        if (isNetworkAvailable(mActivity!!)) {
            Helper.hideKeyboard(mActivity!!)

            if (mOffset == 0) {
                ApiRequest<Any>(activity = mActivity!!,
                        objectType = ApiInitialize.initialize().notificationList(ws_lang = Prefs.getValue(mActivity!!, AppConstant.TIME_ZONE, "").toString()
                                , authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!
                                , offset = 0
                                , limit = 10),
                        TYPE = WebConstant.GET_NOTIFICATION,
                        isShowProgressDialog = mShowDia,
                        apiResponseInterface = this)
            } else {
                ApiRequest<Any>(activity = mActivity!!,
                        objectType = ApiInitialize.initialize().notificationList(ws_lang = Prefs.getValue(mActivity!!, AppConstant.TIME_ZONE, "").toString()
                                , authtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")!!
                                , offset = mOffset
                                , limit = 10),
                        TYPE = WebConstant.GET_NOTIFICATION_MORE,
                        isShowProgressDialog = mShowDia,
                        apiResponseInterface = this)
            }

        } else {
            SnackBar.show(mActivity!!, true,
                    getStr(mActivity!!, R.string.str_network_error), false, "", null)
        }
    }


    private fun setData() {
        mAppointmentAdapter = NotificationsAdapter(this, mActivity!!)
        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        rvcNotification?.layoutManager = linearLayoutManager
        rvcNotification?.adapter = mAppointmentAdapter


        mActivity!!.navView!!.menu.findItem(R.id.navigation_dashboard).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_booking).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_notifications).title = mActivity!!.resources.getString(R.string.title_notifications)
        mActivity!!.navView!!.menu.findItem(R.id.navigation_profile).title = ""
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        if (apiResponseManager.type == WebConstant.GET_NOTIFICATION) {
            val response = apiResponseManager.response as NotificationResponse

            when {
                response.status == 200 -> {
                    try {
                        rvcNotification?.refreshComplete()
                    } catch (e: Exception) {
                    }
                    if (response.data.size > 0) {
                        tvNodata?.gone()
                        mAppointmentAdapter.addAllItem(response.data)
                    } else {
                        tvNodata?.visible()
                    }

                }
                else -> showToast(response.message!!, mActivity!!)
            }
        } else if (apiResponseManager.type == WebConstant.GET_NOTIFICATION_MORE) {
            val response = apiResponseManager.response as NotificationResponse

            when {
                response.status == 200 -> {
                    try {
                        rvcNotification?.loadMoreComplete()
                    } catch (e: Exception) {
                    }
                    if (response.data.size < 0) {
                        mAppointmentAdapter.addAllItem(response.data)
                    }

                }
                else -> showToast(response.message!!, mActivity!!)
            }
        } else if (apiResponseManager.type == 412) {
            val response = apiResponseManager.response as ApiStringError
            showToast(response.message!!, mActivity!!)
        }
    }
}