package com.zestbrains.jamalek.notifications

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnNotificationItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.notification.NotificationData
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.inflate
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class NotificationsAdapter(private val onClick: OnNotificationItemSelectListener, private val mActivity: Context) : RecyclerView.Adapter<NotificationsAdapter.MyViewHolder>() {


    private var mModel = ArrayList<NotificationData>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var clDtails: androidx.constraintlayout.widget.ConstraintLayout = itemView.findViewById(R.id.clDtails)
        var ivImage: de.hdodenhof.circleimageview.CircleImageView = itemView.findViewById(R.id.ivImage)
        var mTitle: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvNotificationText)
        var mTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvNotificationTime)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(parent.inflate(R.layout.rv_notification))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.mTime.text = Helper.getDateAndTimeForOrderDetails(mModel[listPosition].insert_date.toString())
        holder.mTitle.text = mModel[listPosition].push_message
        Glide.with(mActivity).load(mModel[listPosition].profile_image)
                .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(holder.ivImage)

        holder.clDtails.setSafeOnClickListener { onClick.OnItemSelectListener(listPosition, "Details", mModel.get(listPosition)) }

    }


    override fun getItemCount(): Int {
        return mModel.size
    }


    fun addAllItem(mData: ArrayList<NotificationData>) {
        for (i in 0 until mData.size) {
            mModel.add(mData[i])
            notifyItemInserted(mModel.size - 1)
            notifyDataSetChanged()
        }
    }


    fun addItem(mData: NotificationData) {
        mModel.add(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun removeItem(pos: Int) {
        if (pos < mModel.size) {
            mModel.removeAt(pos)
            notifyDataSetChanged()
        }
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }


}