package com.zestbrains.jamalek.viewModels


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.zestbrains.jamalek.DataSource.HomePostDataSourceFactory
import com.zestbrains.jamalek.DataSource.SalonsTheaterDataSource
import com.zestbrains.jamalek.model.login.FilterModelData
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.restapi.ApiInterface
import com.zestbrains.jamalek.restapi.NetworkState
import com.zestbrains.jamalek.restapi.ServiceGenerator
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class SalonsInTheaterViewModel(application: Application) : AndroidViewModel(application) {

    var moviesInTheaterList: LiveData<PagedList<Data>>
    val networkStateLiveData: LiveData<NetworkState>
    val executor: Executor
    private val dataSource: LiveData<SalonsTheaterDataSource>


    init {


        executor = Executors.newFixedThreadPool(5)
        val webService = ServiceGenerator.createService(ApiInterface::class.java)
        val factory = HomePostDataSourceFactory(executor, webService, getApplication<Application>(), mData!!)
        dataSource = factory.mutableLiveData

        networkStateLiveData = Transformations.switchMap(factory.mutableLiveData) { source -> source.networkState }

        val pageConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(10).build()

        moviesInTheaterList = LivePagedListBuilder(factory, pageConfig).setFetchExecutor(executor).build()

    }

    fun getMoviesInTheaterList(data: FilterModelData): LiveData<PagedList<Data>> {

        return moviesInTheaterList
    }

    companion object {
        private val TAG = "TheaterViewModel"
        var mData: FilterModelData? = null
    }


    fun refresh() = dataSource.value!!.invalidate()
}

