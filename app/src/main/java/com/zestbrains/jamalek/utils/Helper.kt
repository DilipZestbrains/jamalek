package com.zestbrains.jamalek.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.zestbrains.jamalek.utils.CommonFunctions.getReadTimeMessage
import org.json.JSONException
import org.json.JSONObject


import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.HashMap
import java.util.Locale
import java.util.TimeZone

@SuppressLint("StaticFieldLeak")
object Helper {
    private val TAG = "Helper"


    fun hideKeyboard(context: Activity) {
        val view = context.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard(context: Activity) {
        val view = context.currentFocus
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun checkEmailValidation(context: Context, email: String): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        return !email.matches(emailPattern.toRegex()) || email.length <= 0
    }

    fun updateResources(context: Context, language: String): Boolean {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = context.resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        //        AppSharedPreferences.getInstance(mContext).setString(AppSharedPreferences.PREFERENCE_LANGUAGE, language);
        return true
    }

    fun pxToDp(heght: Int, fContext: Activity): Float {
        return heght / fContext.resources.displayMetrics.density
    }

    fun getTimeWithAMPM(originalString: String): String {
        var newstr = ""
        try {
            val simpleDateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
            //            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            val date = simpleDateFormat.parse(originalString)
            newstr = SimpleDateFormat("hh:mm a", Locale.getDefault()).format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return newstr
    }

    fun getOfferEndTime(originalString: String): String {
        var newstr = ""
        try {//2020-02-29
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = simpleDateFormat.parse(originalString)
            newstr = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("Exception ", "$originalString---$e")
        }
        Log.e("originalString ", "$newstr-----$originalString")
        return newstr
    }


    fun getDay(originalString: String): String {
        var newstr = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
            val date = simpleDateFormat.parse(originalString)
            newstr = SimpleDateFormat("dd", Locale.getDefault()).format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("Exception ", "$originalString---$e")
        }
        return newstr
    }

    fun getStartTime(originalString: String): String {
        var newstr = ""
        try {
            val simpleDateFormat = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
            val date = simpleDateFormat.parse(originalString)
            newstr = SimpleDateFormat("dd", Locale.getDefault()).format(date!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("Exception ", "$originalString---$e")
        }
        return newstr
    }

    fun getServerTimeFormatFromMilisecond(milliSeconds: Long, dateFormat: String): String {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        formatter.timeZone = TimeZone.getTimeZone("UTC")

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    fun getDateAndTimeForOrderDetails(originalString: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null
        try {
            date = dateFormat.parse(originalString)
            return getReadTimeMessage(date.time)

        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return ""
    }

    fun whenDate(originalString: String?): CharSequence? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var date: Date? = null

        val dateFormat5 = SimpleDateFormat("hh:mm a',' d'th' 'of' MMMM',' yyyy", Locale.getDefault())
        //        dateFormat5.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            date = dateFormat.parse(originalString!!)

            val cal = Calendar.getInstance()
            cal.time = date!!
            val day = cal.get(Calendar.DATE)

            val dateFormat1 = SimpleDateFormat("EEE d'st' MMMM yyyy", Locale.getDefault())
            //            dateFormat1.setTimeZone(TimeZone.getTimeZone("UTC"));

            val dateFormat2 = SimpleDateFormat("EEE d'nd' MMMM yyyy", Locale.getDefault())
            //            dateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));

            val dateFormat3 = SimpleDateFormat("EEE d'rd' MMMM yyyy", Locale.getDefault())
            //            dateFormat3.setTimeZone(TimeZone.getTimeZone("UTC"));

            val dateFormat4 = SimpleDateFormat("EEE d'th' MMMM yyyy", Locale.getDefault())
            //            dateFormat4.setTimeZone(TimeZone.getTimeZone("UTC"));

            if (day !in 11..18)
                return when (day % 10) {
                    1 -> dateFormat1.format(date)
                    2 -> dateFormat2.format(date)
                    3 -> dateFormat3.format(date)
                    else -> dateFormat4.format(date)
                }
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return dateFormat5.format(date!!)
    }


    fun setLan(activity: Context, passlan: String): String? {
        try {
            val locale = Locale(passlan)
            Locale.setDefault(locale)
            val resources = activity.resources
            val configuration = resources.configuration
            activity.createConfigurationContext(configuration)
            configuration.locale = locale
            resources.updateConfiguration(configuration, resources.displayMetrics)

        } catch (e: Exception) {
            Log.e("EXCE ", e.message + " RRRR " + e.toString())
        }

        return passlan
    }


    fun drawableToBitmap(drawable: Drawable): Bitmap {
        var bitmap: Bitmap? = null

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    fun getFilterTime(pos: Int): String {

        when (pos) {
            0 -> {
                return "08:00am"
            }
            1 -> {
                return "09:00am"
            }
            2 -> {
                return "10:00am"
            }
            3 -> {
                return "11:00am"
            }
            4 -> {
                return "12:00pm"
            }
            5 -> {
                return "01:00pm"
            }
            6 -> {
                return "02:00pm"
            }
            7 -> {
                return "03:00pm"
            }
            8 -> {
                return "04:00pm"
            }
            9 -> {
                return "05:00pm"
            }
            10 -> {
                return "06:00pm"
            }
            11 -> {
                return "07:00pm"
            }
            12 -> {
                return "08:00pm"
            }
        }
        return "08:00am"

    }

    @Throws(JSONException::class)
    fun getDataHashMap(jsonData: JSONObject): HashMap<String, String> {
        val getData = HashMap<String, String>()
        val keysItr = jsonData.keys()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            getData[key] = jsonData.getString(key)
        }
        return getData
    }

}
