package com.zestbrains.jamalek.utils

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import com.zestbrains.jamalek.common.MyCustomProgressDialog


val TAG = "Util"

fun getProgressDialog(context: Context): ProgressDialog {
    val myCustomProgressDialog = MyCustomProgressDialog(context)
    myCustomProgressDialog.isIndeterminate = true
    myCustomProgressDialog.setCancelable(false)
    myCustomProgressDialog.show()
    return myCustomProgressDialog

}

fun dismissDialog1(context: Context, mProgressDialog: ProgressDialog) {
    try {
        if (mProgressDialog.isShowing) {
            mProgressDialog.dismiss()
        }
    } catch (e: Exception) {

    }

}

fun openSoftKeyboard(context: Context) {
    try {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    } catch (e: NullPointerException) {
        e.printStackTrace()
    } catch (e: Exception) {
        e.printStackTrace()
    }

}


fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo

    return activeNetworkInfo != null
}


fun isNetWork(context: Context): Boolean {
    var flag = false
    flag = isNetworkAvailable(context)
    return flag
}

fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

@SuppressLint("showToast")
fun showToast(response: String, activity: Activity) {
    Toast.makeText(activity.applicationContext, response, Toast.LENGTH_SHORT).show()
}

fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}



fun getNavBarHeight(mContext: Activity): Int {

    if (!hasNavBar(mContext)) {
        return 0
    }

    val resourceId = mContext.resources.getIdentifier("navigation_bar_height", "dimen", "android")
    return if (resourceId > 0) {
        mContext.resources.getDimensionPixelSize(resourceId)
    } else 0
}


internal fun hasNavBar(context: Context): Boolean {
    val decorView = (context as Activity).window.decorView
    var has = false
    decorView.setOnSystemUiVisibilityChangeListener { visibility ->
        if (visibility and View.SYSTEM_UI_FLAG_HIDE_NAVIGATION == 0) {
            // TODO: The navigation bar is visible. Make any desired
            // adjustments to your UI, such as showing the action bar or
            // other navigational controls.
            has = true

        }
    }
    return has
}

fun getStatusBarHeight(context: Activity): Int {
    var result = 0
    val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        result = context.resources.getDimensionPixelSize(resourceId)
    }
    return result
}


fun getClr(mContext: Activity, id: Int): Int {
    return ResourcesCompat.getColor(mContext.resources, id, null)
}



fun getClr(mContext: Context, id: Int): Int {
    return ResourcesCompat.getColor(mContext.resources, id, null)
}
fun getStr(mContext: Context, id: Int): String {
    return mContext.resources.getString(id)
}

fun getRes(mContext: Activity, id: Int): Drawable? {
    return ResourcesCompat.getDrawable(mContext.resources, id, mContext.theme)!!
}

fun prettyCount(number: Float): String {
    return when {
        number > 10000 -> "NA km"
        number > 99 -> "%.1f".format(number / 1000f).plus("k km")
        else -> "%.1f".format(number).plus(" km")
    }
}

fun clearFilter(mContext: Activity){

}








