package com.zestbrains.jamalek.utils



object AppConstant {


    const val ACCOUNT_DATA = "user_data"

    const val AUTHORIZATION_TOKEN = "auth_token"
    const val TIME_ZONE = "time_zone"
    const val FILTER_MAN = "filter_man"
    const val FILTER_WOMAN = "filter_woman"
    const val FILTER_KIDS = "filter_kids"
    const val FILTER_CATEGORY = "filter_category"
    const val FILTER_TIME = "filter_time"
    const val FILTER_HOUSE = "filter_house"

    const val FILTER_STORE= "filter_store"
    const val FILTER_DISTANCE = "filter_distance"
    const val FILTER_STYLIST = "filter_stylist"
    const val FILTER_ADDRESS = "filter_address"

    const val SELECT_LNG = "select_lag"
     var NOTIFICATION_TOKEN = "notification_token"
    const val USER_ID = "user_id"

    const val LAT = "latitude"
    const val LONG = "longitude"

    const val HOME_DATA = "home_data"
    const val MENU_DATA = "menu_data"

    const val SALON_ID = "salon_id"
    const val SALON_RATINGS = "salon_ratings"
    const val SALON_REVIEW = "salon_review"
    const val SALON_OWN_RATING = "salon_own_ratings"
    const val SALON_DESCRIPTION = "salon_description"
    const val SALON_DATA = "salon_data"
    const val SALON_CATEGORY = "salon_category"

    const val ADDRESS_DATA = "address_data"
    const val ADDRESS_ACTION = "address_action"
    const val OFFER_SELECT = "offer_select"
    const val OFFER_DATA = "offer_data"

    const val SALON_SERVICE = "salon_service"

    const val BOOKING_DATA = "booking_data"

    const val SALON_INHOUSE = "salon_insouse"
    const val SALON_FOR_MAN_WOMAN_KIDS = "salon_for_man_woman_kids"

    const val SERVICE_FOR = "service_for"

    const val ID = "id"
    const val CALL_FROM = "call_from"



}
