package com.zestbrains.jamalek.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.paging.PagedList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.zestbrains.jamalek.model.salon.Data
import org.json.JSONArray



object Prefs {

    private var sharedPreferences: SharedPreferences? = null

    private fun openPrefs(context: Context) {
        sharedPreferences = context.getSharedPreferences("nowayste", Context.MODE_PRIVATE)
    }

    fun setValue(context: Context, key: String, value: String) {
        openPrefs(context)
        var prefsEdit: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsEdit!!.putString(key, value)
        prefsEdit.apply()

        prefsEdit = null
        sharedPreferences = null
    }

    fun setValue(context: Context, key: String, value: Long) {
        openPrefs(context)
        var prefsEdit: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsEdit!!.putLong(key, value)
        prefsEdit.apply()

        prefsEdit = null
        sharedPreferences = null
    }

    fun setValue(context: Context, key: String, value: Boolean) {
        openPrefs(context)
        var prefsEdit: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsEdit!!.putBoolean(key, value)
        prefsEdit.apply()

        prefsEdit = null
        sharedPreferences = null
    }


    fun setValue(context: Context, key: String, value: Int) {
        openPrefs(context)
        var prefsEdit: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsEdit!!.putInt(key, value)
        prefsEdit.apply()

        prefsEdit = null
        sharedPreferences = null
    }


    fun setObject(context: Context, key: String, value: Any) {
        openPrefs(context)
        val prefsEdit = sharedPreferences!!.edit()
        val gson = Gson()
        val json = gson.toJson(value)
        prefsEdit.putString(key, json)
        prefsEdit.apply()

    }



    fun getValue(context: Context, key: String, defaultValue: String): String? {
        openPrefs(context)
        val result = sharedPreferences!!.getString(key, defaultValue)
        sharedPreferences = null
        return result
    }

    fun getValue(context: Context, key: String, defaultValue: Int): Int {
        openPrefs(context)
        val result = sharedPreferences!!.getInt(key, defaultValue)
        sharedPreferences = null
        return result
    }

    fun getValue(context: Context, key: String, defaultValue: Boolean): Boolean {
        openPrefs(context)
        val result = sharedPreferences!!.getBoolean(key, defaultValue)
        sharedPreferences = null
        return result
    }


    fun getObject(context: Context, key: String, defaultValue: String, va: Class<*>): Any {
        val gson = Gson()
        openPrefs(context)
        val result = sharedPreferences!!.getString(key, defaultValue)

        if (result == "NODATA")
            return "NODATA"
        return gson.fromJson(result, va)
    }

    fun getArray(context: Context, key: String, defaultValue: String, va: Class<*>): Any {
        val gson = Gson()
        openPrefs(context)

        val result = sharedPreferences!!.getString(key, defaultValue)

        Log.e("AAAAAc ",result)

        if (result == "NODATA")
            return "NODATA"

        var listType = object : TypeToken<PagedList<Data>>() {

        }.type

         val yourList = Gson().fromJson(result, Array<Data>::class.java)
        return yourList
    }

    fun removeValue(context: Context, key: String) {
        openPrefs(context)
        val editor = sharedPreferences!!.edit()
        editor.remove(key)
        editor.apply()
    }

    fun clearAllData(context: Context) {
        openPrefs(context)
        val editor = sharedPreferences!!.edit()
        editor.clear()
        editor.apply()
    }

    fun contain(context: Context, key: String): Boolean {
        openPrefs(context)
        return sharedPreferences!!.contains(key)
    }
}

