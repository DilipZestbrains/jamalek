package com.zestbrains.jamalek.utils

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log

import androidx.appcompat.app.AlertDialog
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import java.io.IOException
import java.util.Locale

class GPSTracker(private val mContext: Context) : LocationListener {

    private var mIsGPSEnabled: Boolean = false
    private var mIsNetworkEnabled: Boolean = false
    private var mCanGetLocation: Boolean = false

    private var mLocation: Location? = null
    private var mLatitude: Double = 0.toDouble()
    private var mLongitude: Double = 0.toDouble()

    protected var mLocationManager: LocationManager? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    val location: Location?
        @SuppressLint("MissingPermission")
        get() {
            try {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext)

                mLocationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
                mIsGPSEnabled = mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
                mIsNetworkEnabled = mLocationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

                if (mIsGPSEnabled || mIsNetworkEnabled) {
                    this.mCanGetLocation = true
                    if (mIsNetworkEnabled) {
                        mLocationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
                        Log.d("Network", "Network")
                        if (mLocationManager != null) {
                            mLocation = mLocationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                            if (mLocation != null) {
                                mLatitude = mLocation!!.latitude
                                mLongitude = mLocation!!.longitude
                            }
                        }
                    }
                    if (mIsGPSEnabled) {
                        if (mLocation == null) {
                            mLocationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this)
                            Log.d("GPS Enabled", "GPS Enabled")
                            if (mLocationManager != null) {
                                mLocation = mLocationManager!!
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                if (mLocation != null) {
                                    mLatitude = mLocation!!.latitude
                                    mLongitude = mLocation!!.longitude
                                }
                            }
                        }
                    }
                }

            } catch (e: Exception) {

                fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            if (location != null) {
                                mLatitude = location.latitude
                                mLongitude = location.longitude
                            }
                        }

                Log.e(" Exception Location ", "$e---$mLatitude---$mLongitude")
                e.printStackTrace()
            }

            return mLocation
        }

    /**
     * Function to get latitude
     */
    // return latitude
    val latitude: Double
        get() {
            if (mLocation != null) {
                mLatitude = mLocation!!.latitude
            }
            return mLatitude
        }

    /**
     * Function to get longitude
     */
    // return longitude
    val longitude: Double
        get() {
            if (mLocation != null) {
                mLongitude = mLocation!!.longitude
            }
            return mLongitude
        }

    val city: String
        get() {
            val gcd = Geocoder(mContext, Locale.getDefault())
            val addresses: List<Address>
            var cityName = ""

            try {
                addresses = gcd.getFromLocation(latitude, longitude, 1)
                if (addresses.isNotEmpty()) {
                    cityName = addresses[0].locality
                }
            } catch (e: IOException) {
                Log.d(TAG, "::getCity() -- ERROR: " + e.message)
            }

            return cityName
        }

    init {
        location
    }

    override fun onLocationChanged(arg0: Location) {
        // TODO Auto-generated method stub

    }

    override fun onProviderDisabled(provider: String) {
        // TODO Auto-generated method stub

    }

    override fun onProviderEnabled(provider: String) {
        // TODO Auto-generated method stub

    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
        // TODO Auto-generated method stub

    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    fun stopUsingGPS() {
        if (mLocationManager != null) {
            mLocationManager!!.removeUpdates(this@GPSTracker)
        }
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     */
    fun canGetLocation(): Boolean {
        return mCanGetLocation
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings")

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?")

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings") { dialog, which ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent)
        }

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

        // Showing Alert Message
        alertDialog.show()
    }

    companion object {
        private val TAG = GPSTracker::class.java.simpleName

        private val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 10
        private val MIN_TIME_BW_UPDATES = (1000 * 60 * 1).toLong()
    }

}