@file:Suppress("NAME_SHADOWING")

package com.zestbrains.jamalek.utils


import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlarmManager
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.SystemClock
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import com.zestbrains.jamalek.R
import kotlinx.android.synthetic.main.activity_booking.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs


object CommonFunctions {


    fun hideKeyboard(view: View, context: Context) {
        try {
            val inputMethodManager =
                    context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun openKeyboard(view: View, context: Context) {
        try {
            val inputMethodManager =
                    context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun buttonEffect(button: View, context: Context) {
        button.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(
                            ContextCompat.getColor(context, R.color.colorPrimary),
                            PorterDuff.Mode.SRC_ATOP
                    )
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }
    }

    fun sp2px(context: Context, spValue: Float): Int {
        return (spValue * context.resources.displayMetrics.scaledDensity + 0.5f).toInt()
    }

    fun dp2px(context: Context, dpValue: Float): Int {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dpValue,
                context.resources.displayMetrics
        ).toInt()
    }

    fun convertDeliveryDate(time: Long): String {
        val date = Date()
        val dateFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)
        dateFormat.timeZone = TimeZone.getDefault()
        date.time = time
        return dateFormat.format(date)
    }

    fun convertStringToDate(mDate: String): Long {
        var date: Date
        val dateFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)
        dateFormat.timeZone = TimeZone.getDefault()
        date = dateFormat.parse(mDate)

        return date.time
    }

    fun convertStringtoDate(mDate: String): Date {
        var date: Date
        val dateFormat = SimpleDateFormat("dd MMM yyyy", Locale.US)
        dateFormat.timeZone = TimeZone.getDefault()
        date = dateFormat.parse(mDate)

        return date
    }

    fun getConvertStringToDay(time: Long): String {
        val date = Date()
        val dateFormat = SimpleDateFormat("dd", Locale.US)
        dateFormat.timeZone = TimeZone.getDefault()
        date.time = time
        return dateFormat.format(date)
    }

    fun getNavBarHeight(mContext: Activity): Int {

        if (!hasNavBar(mContext)) {
            return 0
        }
        val resourceId =
                mContext.resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId > 0) {
            mContext.resources.getDimensionPixelSize(resourceId)
        } else 0
    }

    private fun hasNavBar(context: Context): Boolean {
        val decorView = (context as Activity).window.decorView
        var has = false
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_HIDE_NAVIGATION == 0) {
                // TODO: The navigation bar is visible. Make any desired
                // adjustments to your UI, such as showing the action bar or
                // other navigational controls.
                has = true

            }
        }
        return has
    }

    fun getStatusBarHeight(context: Activity): Int {
        var result = 0
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun getCurrentDateTime(): String {

        val dateFormat1 = SimpleDateFormat("yyyy-MM-dd")


        return dateFormat1.format(Calendar.getInstance().timeInMillis)
    }

    fun getCurrentMonthYear(): String {

        val dateFormat1 = SimpleDateFormat("MMMM yyyy")


        return dateFormat1.format(Calendar.getInstance().timeInMillis)
    }

    fun getReadTimeMessage(tim: Long?): String {

        try {

            val seconds: Long?
            val minutes: Long?
            val hours: Long?
            var days: Long?
            var strTime: String

            val currentTime = Calendar.getInstance().time

            val diffInMillisec = currentTime.time - tim!!
            var diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec)

            seconds = diffInSec % 60
            diffInSec /= 60
            minutes = diffInSec % 60
            diffInSec /= 60
            hours = diffInSec % 24
            diffInSec /= 24
            days = diffInSec

            days = abs(days)


            strTime = if (days == 0L && hours == 0L && minutes == 0L && seconds < 60) {
                "Just Now"
            } else if (hours == 0L && days == 0L) {
                "$minutes min ago"
            } else if (hours != 0L && days == 0L) {
                "$hours hours ago"
            } else {
                "$days days ago"
            }


            return strTime
        } catch (e: Exception) {
            Log.e("Exception : ", e.message)
            return "--"
        }

    }

    fun getStr(mContext: Activity, id: Int): String {
        return mContext.resources.getString(id)
    }

}