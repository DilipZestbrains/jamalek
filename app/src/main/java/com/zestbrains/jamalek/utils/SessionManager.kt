package com.zestbrains.jamalek.utils


import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.util.Log
import com.zestbrains.jamalek.R


class SessionManager(private val context: Context) {
    private val resources: Resources

    companion object {

        private lateinit var pref: SharedPreferences
        private lateinit var editor: SharedPreferences.Editor
        lateinit var instance: SessionManager

        // Shared pref mode
        private val PRIVATE_MODE = 0

    }



    init {
        instance = this
        resources = context.resources
        pref = context.getSharedPreferences(resources.getString(R.string.app_shared_pref), PRIVATE_MODE)
        editor = pref.edit()
    }



    fun put(key: String, value: String): String {
        editor.putString(key, value)
        editor.commit()
        return key
    }

    fun put(key: String, value: Boolean): String {
        editor.putBoolean(key, value)
        editor.commit()
        return key
    }

    fun put(key: String, value: Int): String {
        editor.putInt(key, value)
        editor.commit()
        return key
    }

    operator fun get(key: String, value: String): String = pref.getString(key, value)!!

    operator fun get(key: String, value: Boolean): Boolean = pref.getBoolean(key, value)


    operator fun get(key: String, value: Int): Int = pref.getInt(key, value)


    fun logout() {
        editor.clear()
        editor.commit()
    }


    fun setString(key: String, value: String) {
        val editor = pref.edit()
        editor.putString(key, value)
        editor.apply()
    }





}
