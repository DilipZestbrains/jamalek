package com.zestbrains.jamalek.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.iid.FirebaseInstanceId
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), (View) -> Unit, ApiResponseInterface {


    private var googleSignInClient: GoogleSignInClient? = null
    private val SIGN_IN = 123
    private lateinit var socialType: String
    private var googleData: GoogleSignInAccount? = null
    private lateinit var facebookData: FirebaseUser
    private lateinit var callbackManager: CallbackManager


    private var Name: String = ""
    private var GoogleId: String = ""
    private var FBId: String = ""
    private var Email: String = ""
    private var Profile: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Helper.setLan(this, Prefs.getValue(this@LoginActivity, AppConstant.SELECT_LNG, "en")!!)
        setContentView(R.layout.activity_login)

        clicker()
        FirebaseAuth.getInstance().signOut()


        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val newToken = instanceIdResult.token
            NOTIFICATION_TOKEN = newToken
        }

        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
                .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                    override fun onSuccess(result: LoginResult?) {
                        handleFacebookAccessToken(result?.accessToken!!)
                    }

                    override fun onCancel() {
                        socialType = ""
                        Log.e(TAG, "onCancel::: ")
                    }

                    override fun onError(error: FacebookException?) {
                        socialType = ""
                    }
                })

    }

    private fun clicker() {

        ivFaceBook.setSafeOnClickListener(this)
        ivGoogle.setSafeOnClickListener(this)
        tvSignUp.setSafeOnClickListener(this)
        btnLogin.setSafeOnClickListener(this)
        tvForgotPassword.setSafeOnClickListener(this)
        btnSkip.setSafeOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    override fun invoke(p1: View) {
        when (p1) {
            ivFaceBook -> {
                if (isNetworkAvailable(mContext)) {
                    socialType = "facebook"
                    loginWithFB()
                } else SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)


            }
            ivGoogle -> {
                if (isNetworkAvailable(mContext)) {
                    socialType = "google"
                    loginWithGoogle()
                } else SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)

            }
            btnLogin -> {
                login()
            }
            btnSkip -> {
                start<MainActivity>()
            }
            tvForgotPassword -> {
                start<ForgotPasswordActivity>("Action" to "ForgotPassword")
            }
            tvSignUp -> {
                start<RegisterActivity>()
            }
        }

    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }


    private fun login() {
        if (isValid()) {
            if (isNetworkAvailable(mContext)) {
                Helper.hideKeyboard(mContext)

                ApiRequest<Any>(
                        activity = mContext,
                        objectType = ApiInitialize.initialize()
                                .login(Prefs.getValue(this@LoginActivity, AppConstant.SELECT_LNG, "en")!!,
                                        country_code = "+" + ccp_login_country.selectedCountryCode.toString(),
                                        mobile = cl_login_number.text.toString().trim(),
                                        password = cl_login_password.text.toString().trim(),
                                        eDeviceType = "Android",
                                        vPushToken = NOTIFICATION_TOKEN),
                        TYPE = WebConstant.LOGIN_API,
                        isShowProgressDialog = true,
                        apiResponseInterface = this@LoginActivity
                )
            } else
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)

        }
    }

    private fun loginWithGoogle() {
        if (isNetworkAvailable(mContext)) {
            // Configure Google Sign In
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build()

            googleSignInClient = GoogleSignIn.getClient(mContext, gso)

            val signInIntent = googleSignInClient?.signInIntent
            startActivityForResult(signInIntent, SIGN_IN)
        } else {
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
        }
    }


    private fun loginWithFB() {
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email", "public_profile"))
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    googleData = task.getResult(ApiException::class.java)
                    Log.e(TAG, "onActivityResult: email " + googleData?.email)
                    Log.e(TAG, "onActivityResult: photoUrl " + googleData?.photoUrl)
                    Log.e(TAG, "onActivityResult: id " + googleData?.id)

                    Name = googleData?.displayName!!
                    Profile = googleData?.photoUrl.toString()
                    Email = googleData?.email.ifNotNullOrElse({ it }, { "" })
                    GoogleId = googleData?.id!!


                    socialLogin(
                            googleData?.id!!,
                            "",
                            socialType,
                            email = googleData?.email!!,
                            username = googleData?.displayName!!,
                            pushToken = NOTIFICATION_TOKEN,
                            checkExist = "1",
                            profileUrl = googleData?.photoUrl!!.toString())

                } catch (e: ApiException) {
                    socialType = ""
                    showToast(e.message!!, mContext)
                    Log.e(TAG, "Google sign in failed", e)
                }
            } else {
                socialType = ""
                showToast("Please try again later", mContext)
                Log.e(TAG, "Google sign in failed else ..")
            }
        }
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return
        }
    }


    private fun handleFacebookAccessToken(token: AccessToken) {
        val dialog = getProgressDialog(mContext)
        val mAuth = FirebaseAuth.getInstance()

        val credential: AuthCredential = FacebookAuthProvider.getCredential(token.token)
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener { it ->
                    if (it.isSuccessful) {
                        // Sign in success, update UI with the signed-in facebookData's information
                        facebookData = mAuth.currentUser!!
                        Name = facebookData.displayName.toString()
                        Profile = facebookData.photoUrl.toString() + "?type=large"
                        Email = facebookData.email.ifNotNullOrElse({ it }, { "" })
                        FBId = facebookData.uid

                        Log.e(TAG, "email " + facebookData.email)
                        Log.e(TAG, "id " + facebookData.uid)
                        Log.e(TAG, "url " + facebookData.photoUrl)
                        Log.e(TAG, "url " + facebookData.displayName)

                        dismissDialog1(mContext, dialog)

                        socialLogin(
                                gId = "",
                                fId = facebookData.uid,
                                socialType = socialType,
                                email = facebookData.email.ifNotNullOrElse({ it }, { "" }),
                                username = facebookData.displayName!!,
                                pushToken = NOTIFICATION_TOKEN,
                                checkExist = "1",
                                profileUrl = facebookData.photoUrl!!.toString() + "?type=large")
                    } else {
                        socialType = ""
                        Log.e(TAG, "signInWithCredential:failure", it.exception)
                    }
                }
    }

    private fun socialLogin(gId: String,
                            fId: String,
                            socialType: String,
                            email: String,
                            username: String,
                            pushToken: String,
                            checkExist: String,
                            profileUrl: String) {
        if (isNetworkAvailable(mContext)) {
            Helper.hideKeyboard(mContext)
            ApiRequest<Any>(
                    activity = mContext,
                    objectType = ApiInitialize.initialize().socialLogin(Prefs.getValue(this@LoginActivity, AppConstant.SELECT_LNG, "en")!!, google_id = gId, facebook_id = fId,
                            social_type = socialType, email = email, name = username, country_code = "", mobile = "", eDeviceType = "Android",
                            vPushToken = pushToken, checkExist = checkExist,
                            profile_image = profileUrl),
                    TYPE = WebConstant.LOGIN_API,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "", null)
        }
    }

    private fun isValid(): Boolean {
        Helper.hideKeyboard(mContext)

        return when {
            TextUtils.isEmpty(cl_login_number.text.toString().trim()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_login_number_error), false, "", null)
                false
            }

            TextUtils.isEmpty(cl_login_password.text.toString().trim()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_login_password_error), false, "", null)
                false
            }

            else -> true
        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.LOGIN_API -> {
                val response = apiResponseManager.response as AccountResponse

                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mContext, key = AppConstant.ACCOUNT_DATA, value = response)

                        response.data?.let {

                            Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken))
                            Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())

                            start<MainActivity>()
                            finishAffinity()
                            return
                        }

                    }
                    else -> showToast(response.message!!, mContext)
                }
            }

            405 -> {
                val response = apiResponseManager.response as ApiStringError

                start<SocialLoginActivity>("Name" to Name,
                        "GoogleId" to GoogleId,
                        "FBId" to FBId,
                        "socialType" to socialType,
                        "Email" to Email,
                        "Profile" to Profile,
                        "checkExist" to "1")
            }
        }
    }


}
