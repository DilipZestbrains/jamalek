package com.zestbrains.jamalek.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.zestbrains.jamalek.BuildConfig
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_splash.*
import java.text.SimpleDateFormat
import java.util.*


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Helper.setLan(this, Prefs.getValue(this@SplashActivity, AppConstant.SELECT_LNG, "en")!!)
        setContentView(R.layout.activity_splash)

        changeText()

        Prefs.setValue(this@SplashActivity, AppConstant.TIME_ZONE, TimeZone.getDefault().id.toString())

        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_DISTANCE, "0")
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_MAN, 0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_WOMAN,0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_KIDS, 0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_STORE, 0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_HOUSE, 0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_STYLIST, 0)
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_TIME, "")
        Prefs.setValue(this@SplashActivity, AppConstant.FILTER_CATEGORY, "")

        Handler().postDelayed({
            Prefs.setValue(mContext, AppConstant.LAT, GPSTracker(mContext).latitude.toString())
            Prefs.setValue(mContext, AppConstant.LONG, GPSTracker(mContext).longitude.toString())
            if (Prefs.contain(mContext, AppConstant.AUTHORIZATION_TOKEN)) {
                start<MainActivity>()
            } else {
                start<LoginActivity>()
            }
            finish()
        }, 3000)
    }


    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    @SuppressLint("SetTextI18n")
    private fun changeText() {
        if (!BuildConfig.DEBUG) {
            root.gone()
            return
        }

        val calendar = Calendar.getInstance()
        calendar.time = BuildConfig.BUILD_TIME
        val format = SimpleDateFormat("ddMMyyy", Locale.getDefault())
        root.text = (resources.getString(R.string.app_name)
                + "_V"
                + BuildConfig.VERSION_CODE
                + "_"
                + format.format(calendar.timeInMillis))
    }

}
