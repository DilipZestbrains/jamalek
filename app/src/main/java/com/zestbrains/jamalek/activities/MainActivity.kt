package com.zestbrains.jamalek.activities

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.utils.AppConstant
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.Prefs
import com.zestbrains.jamalek.utils.getNavBarHeight

class MainActivity : BaseActivity() {

    var navView: BottomNavigationView? = null
    var mUserModel: AccountResponse? = null
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        Helper.setLan(this, Prefs.getValue(this@MainActivity, AppConstant.SELECT_LNG, "en")!!)
        setContentView(R.layout.activity_main)

        findViewById<View>(R.id.container).setPadding(0, 0, 0, getNavBarHeight(mContext))

        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment1)
        navView!!.setupWithNavController(navController)

        if (Prefs.contain(mContext, AppConstant.AUTHORIZATION_TOKEN)) {
            mUserModel = Prefs.getObject(this@MainActivity, AppConstant.ACCOUNT_DATA, "", AccountResponse::class.java) as AccountResponse
        }

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val newToken = instanceIdResult.token
            NOTIFICATION_TOKEN = newToken
        }

    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finishAffinity()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, resources.getString(R.string.back_button_show_text), Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }


}
