package com.zestbrains.jamalek.activities

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_web.*


class WebActivity : BaseActivity() {


    var mURL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }

        setContentView(R.layout.activity_web)


        val title = intent.getStringExtra("Name")

        tv_title.text = title

        if (title == CommonFunctions.getStr(this@WebActivity, R.string.register_as_service_provider)) {
            mURL = "http://zestbrains4u.site/jamalek/vendor/register"

        } else {
            mURL = "http://zestbrains4u.site/jamalek/ws/v1/user/content/" + intent.getStringExtra("Url")
        }

        if (isNetworkAvailable(this)) {

            val dialog = getProgressDialog(mContext)
            val webSetting = webView.settings
            webSetting.javaScriptEnabled = true
            webSetting.displayZoomControls = true

            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    return true
                }

                override fun onPageFinished(view: WebView, url: String) {

                    if (dialog.isShowing) {
                        dismissDialog1(this@WebActivity, dialog)
                    }
                }

                override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                    if (dialog.isShowing) {
                        dismissDialog1(this@WebActivity, dialog)
                    }

                }
            }


            webView.loadUrl(mURL)
        } else {
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
        }

        iv_webview_back.setSafeOnClickListener { onBackPressed() }
    }

}
