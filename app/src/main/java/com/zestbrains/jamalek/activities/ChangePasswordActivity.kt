package com.zestbrains.jamalek.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_change_password.*


class ChangePasswordActivity : BaseActivity(), ApiResponseInterface {


    private var mAction: String = ""
    private var USERID: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)


        mAction = intent.getStringExtra("Action").toString()
        USERID = intent.getStringExtra("USERID").toString()

        if (mAction == "1") {
            ivTitle.text = getStr(this@ChangePasswordActivity, R.string.set_password)
            cardCurrentPassword.visibility = View.INVISIBLE

        } else {
            ivTitle.text = getStr(this@ChangePasswordActivity, R.string.changes_password)
            cardCurrentPassword.visibility = View.VISIBLE
        }

        ivBack.setSafeOnClickListener {
            finish()
        }
        btnChangPassword.setSafeOnClickListener { changePassword() }
    }


    private fun changePassword() {
        if (isNetworkAvailable(mContext)) {

            if (mAction == "1") {
                if (isValidSetPassword()) {
                    ApiRequest<Any>(activity = mContext,
                            objectType = ApiInitialize.initialize().changePassword(Prefs.getValue(this@ChangePasswordActivity, AppConstant.SELECT_LNG, "en")!!, "", cl_new_password.text.toString().trim(),
                                    USERID, "1"),
                            TYPE = WebConstant.CHANGE_PASSWORD_API,
                            isShowProgressDialog = true,
                            apiResponseInterface = this@ChangePasswordActivity)
                }
            } else {
                if (isValid()) {
                    ApiRequest<Any>(activity = mContext,
                            objectType = ApiInitialize.initialize().changePassword(Prefs.getValue(this@ChangePasswordActivity, AppConstant.SELECT_LNG, "en")!!, cl_current_password.text.toString().trim(),
                                    cl_new_password.text.toString().trim(),
                                    USERID, "0"),
                            TYPE = WebConstant.RESET_PASSWORD_API,
                            isShowProgressDialog = true,
                            apiResponseInterface = this@ChangePasswordActivity)
                }
            }

        } else {
            Helper.hideKeyboard(mContext)
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
        }
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.CHANGE_PASSWORD_API -> {
                val response = apiResponseManager.response as CommonResponse
                when (response.status) {
                    200 ->
                    {
                        showToast(response.message!!, mContext)
                        start<LoginActivity>()
                    }
                    else -> showToast(response.message!!, mContext)
                }
            }

            WebConstant.RESET_PASSWORD_API -> {
                val response = apiResponseManager.response as CommonResponse
                when (response.status) {
                    200 -> {
                        showToast(response.message!!, mContext)
                        finish()
                    }
                    else -> showToast(response.message!!, mContext)
                }
            }


        }
    }


    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }


    private fun isValid(): Boolean {
        Helper.hideKeyboard(mContext)
        when {
            TextUtils.isEmpty(cl_current_password.text.toString().trim()) -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.error_enter_current_pass), false, "", null)
                return false
            }
            TextUtils.isEmpty(cl_new_password.text.toString().trim()) -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_enter_new_pass), false, "", null)
                return false
            }
            TextUtils.isEmpty(cl_confirm_password.text.toString().trim()) -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_enter_confirm_pass), false, "", null)
                return false
            }
//            cl_current_password.text.toString().trim() == cl_new_password.text.toString().trim() -> {
//                SnackBar.show(mContext, true, getStr(mContext, R.string.err_new_pass_should_differ), false, "", null)
//                return false
//            }
            cl_new_password.text.toString().trim().length < 6 -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_ne_pass_should_six_digit), false, "", null)
                return false
            }
            cl_new_password.text.toString().trim() != cl_confirm_password.text.toString().trim() -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_confirm_pass_not_match), false, "", null)
                return false
            }
            else -> return true
        }

    }

    private fun isValidSetPassword(): Boolean {
        Helper.hideKeyboard(mContext)
        when {

            TextUtils.isEmpty(cl_new_password.text.toString().trim()) -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_enter_new_pass), false, "", null)
                return false
            }
            TextUtils.isEmpty(cl_confirm_password.text.toString().trim()) -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_enter_confirm_pass), false, "", null)
                return false
            }

            cl_new_password.text.toString().trim().length < 6 -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_ne_pass_should_six_digit), false, "", null)
                return false
            }
            cl_new_password.text.toString().trim() != cl_confirm_password.text.toString().trim() -> {
                SnackBar.show(mContext, true, getStr(mContext, R.string.err_confirm_pass_not_match), false, "", null)
                return false
            }
            else -> return true
        }

    }


}
