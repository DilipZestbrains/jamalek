package com.zestbrains.jamalek.activities


import android.os.Bundle
import android.text.TextUtils
import com.google.firebase.iid.FirebaseInstanceId
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*

import kotlinx.android.synthetic.main.activity_social_login.*

class SocialLoginActivity : BaseActivity(), ApiResponseInterface {

    private var Name: String = ""
    private var GoogleId: String = ""
    private var FBId: String = ""
    private var Email: String = ""
    private var Profile: String = ""
    private var socialType: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_login)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            instanceIdResult -> val newToken = instanceIdResult.token
            NOTIFICATION_TOKEN=newToken
        }
        Name = intent.getStringExtra("Name")
        GoogleId = intent.getStringExtra("GoogleId")
        FBId = intent.getStringExtra("FBId")
        Email = intent.getStringExtra("Email")
        Profile = intent.getStringExtra("Profile")
        socialType = intent.getStringExtra("socialType")

        tvSocialName.setText(Name)
        tvSocialEmail.setText(Email)

        btnRegister.setSafeOnClickListener {

            if (isValid()) {

                socialLogin(
                        GoogleId,
                        FBId,
                        socialType,
                        email = tvSocialEmail.text.toString(),
                        username = tvSocialName.text.toString(),
                        country_code = "+" + ccp_Social_countryCode.selectedCountryCode.toString(),
                        mobile = edSocial_Number.text.toString(),
                        checkExist = "0",
                        profileUrl = Profile)
            }
        }

        ivBack.setSafeOnClickListener { onBackPressed() }

    }


    private fun socialLogin(gId: String,
                            fId: String,
                            socialType: String,
                            email: String,
                            username: String,
                            country_code: String,
                            mobile: String,
                            checkExist: String,
                            profileUrl: String) {
        if (isNetworkAvailable(mContext)) {
            Helper.hideKeyboard(mContext)
            ApiRequest<Any>(
                    activity = mContext,
                    objectType = ApiInitialize.initialize().socialLogin(Prefs.getValue(this@SocialLoginActivity, AppConstant.SELECT_LNG, "en")!!, google_id = gId, facebook_id = fId,
                            social_type = socialType, email = email, name = username, country_code = country_code, mobile = mobile, eDeviceType = "Android",
                            vPushToken = NOTIFICATION_TOKEN, checkExist = checkExist,
                            profile_image = profileUrl),
                    TYPE = WebConstant.LOGIN_API,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), mContext)
        }
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.LOGIN_API -> {
                val response = apiResponseManager.response as AccountResponse

                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mContext, key = AppConstant.ACCOUNT_DATA, value = response)

                        response.data?.let {
                            Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken!!))
                            Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())

                            start<MainActivity>()
                            finishAffinity()
                            return
                        }
                        ShowToast.show(mContext, "HEY")
                    }
                    else -> showToast(response.message!!, mContext)
                }
            }
        }
    }


    private fun isValid(): Boolean {
        Helper.hideKeyboard(mContext)

        return when {
            TextUtils.isEmpty(tvSocialName.text.toString().trim()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_name_error), false, "OK", null)
                false
            }

            TextUtils.isEmpty(edSocial_Number.text.toString().trim()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_login_number_error), false, "", null)
                false
            }

            else -> true
        }
    }
}
