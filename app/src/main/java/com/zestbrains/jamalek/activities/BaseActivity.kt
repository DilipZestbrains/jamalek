package com.zestbrains.jamalek.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.libraries.places.api.Places
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.utils.AppConstant
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.Prefs
import com.zestbrains.jamalek.utils.SessionManager
import java.util.*


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    lateinit var sessionManager: SessionManager
    protected var TAG: String = ""
    protected var NOTIFICATION_TOKEN: String = ""
    var Selected_Language: String = "en"

    protected lateinit var mContext: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)

        mContext = this
        TAG = javaClass.name.substring(javaClass.name.lastIndexOf('.') + 1)

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, getString(R.string.google_maps_key), Locale.getDefault())
        }

    }

    override fun onResume() {
        super.onResume()
        Helper.setLan(this, Prefs.getValue(this@BaseActivity, AppConstant.SELECT_LNG, "en")!!)
        sessionManager = SessionManager(this)
    }


}