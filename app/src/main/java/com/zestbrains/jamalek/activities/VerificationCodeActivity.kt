package com.zestbrains.jamalek.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_verification_code.*

class VerificationCodeActivity : BaseActivity(), ApiResponseInterface {


    private var Password: String = ""
    private var Name: String = ""
    private var Email: String = ""
    private var Number: String = ""
    private var CountryCode: String = ""
    private var OTP: String = ""
    private var isRegister: String = ""
    private var USERID: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_verification_code)


        isRegister = intent.getStringExtra("IsRegister")

        //for register
        when (isRegister) {
            "1" -> {
                Name = intent.getStringExtra("Name")
                Password = intent.getStringExtra("Password")
                Number = intent.getStringExtra("Number")
                Email = intent.getStringExtra("Email")
                OTP = intent.getStringExtra("OTP")
                CountryCode = intent.getStringExtra("CountryCode")
            }

            //for Changes Mobile Number
            "2" -> {
                OTP = intent.getStringExtra("OTP")
                USERID = intent.getStringExtra("USERID")
                Number = intent.getStringExtra("Number")
                CountryCode = intent.getStringExtra("CountryCode")
            }
            //for Forgot  Password
            else -> {
                OTP = intent.getStringExtra("OTP")
                USERID = intent.getStringExtra("USERID")
            }
        }

        pinView.setText(OTP)


        btnVerify.setSafeOnClickListener {
            if (isNetworkAvailable(mContext)) {
                Helper.hideKeyboard(mContext)

                if (isValid())
                    checkOTP()

            } else {
                showToast(resources.getString(R.string.str_network_error), mContext)
            }
        }
        tvVerficationBack.setSafeOnClickListener { onBackPressed() }
    }

    private fun checkOTP() {
        if (pinView.text.toString() == OTP) {

//for Register
            if (isRegister == "1") {
                ApiRequest<Any>(
                        activity = mContext,
                        objectType = ApiInitialize.initialize().register(Prefs.getValue(this@VerificationCodeActivity, AppConstant.SELECT_LNG, "en")!!, Name, Email, CountryCode, Number, Password, "Android", NOTIFICATION_TOKEN),
                        TYPE = WebConstant.SIGN_UP_API,
                        isShowProgressDialog = true,
                        apiResponseInterface = this)
            }
            //for Change Mobile Number
            else if (isRegister == "2") {

                ApiRequest<Any>(
                        activity = mContext,
                        objectType = ApiInitialize.initialize().changeMobileNo(Prefs.getValue(this@VerificationCodeActivity, AppConstant.SELECT_LNG, "en")!!, Prefs.getValue(this@VerificationCodeActivity,
                                AppConstant.AUTHORIZATION_TOKEN, "").toString(), Number, CountryCode),
                        TYPE = WebConstant.CHANGE_MOBILE_NO,
                        isShowProgressDialog = true,
                        apiResponseInterface = this)

            }

            //for Forgot Password
            else {
                start<ChangePasswordActivity>(
                        "USERID" to USERID,
                        "Action" to "1")
            }


        } else {
            Helper.hideKeyboard(mContext)
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_enter_valid_otp), false, "OK", null)
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (

                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)


    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.SIGN_UP_API -> {
                val response = apiResponseManager.response as AccountResponse

                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mContext, key = AppConstant.ACCOUNT_DATA, value = response)

                        response.data?.let {
                            Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken!!))
                            Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())

                            start<MainActivity>()
                            finishAffinity()
                            return
                        }
                        ShowToast.show(mContext, "HEY")
                    }
                    else -> showToast(response.message!!, mContext)
                }
            }

            WebConstant.CHANGE_MOBILE_NO -> {
                val response = apiResponseManager.response as AccountResponse
                showToast(response.message!!, mContext)

                when (response.status) {
                    200 -> {
                        Prefs.setObject(context = mContext, key = AppConstant.ACCOUNT_DATA, value = response)
                        response.data?.let {
                            Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken!!))
                            Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())
                            finish()
                        }
                    }

                }
            }
        }
    }

    private fun isValid(): Boolean {
        Helper.hideKeyboard(mContext)

        return when {
            TextUtils.isEmpty(pinView.text.toString().trim { it <= ' ' }) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_enter_otp), false, "OK", null)
                false
            }

            else -> true
        }
    }
}
