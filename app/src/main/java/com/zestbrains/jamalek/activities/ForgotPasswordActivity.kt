package com.zestbrains.jamalek.activities

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.login.AccountOTPResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_forgot_password.*


class ForgotPasswordActivity : BaseActivity(), ApiResponseInterface {

    private var mAction: String = ""
    private var Number: String = ""
    private var CountryCode: String = ""
    private var isRegister: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        tvBack.setSafeOnClickListener {
            finish()
        }

        mAction = intent.getStringExtra("Action").toString()
        btnVerify.setSafeOnClickListener { forgotPassword() }

        if (mAction == "MobileNo") {
            isRegister = 1
            Number = intent.getStringExtra("NUMBER").toString()
            CountryCode = intent.getStringExtra("COUNTRYCODE").toString()

            ccp_forgotpassword_country.setCountryForPhoneCode(CountryCode.toInt())
            cl_forgotpassword_Number.setText(Number)

            txtTitle.text = getStr(this@ForgotPasswordActivity, R.string.changes_mobile_number)
            txtSubTitle.text = getStr(this@ForgotPasswordActivity, R.string.change_mobile_description)
        } else {
            isRegister = 0
            txtTitle.text = getStr(this@ForgotPasswordActivity, R.string.forgotpassword_title)
            txtSubTitle.text = getStr(this@ForgotPasswordActivity, R.string.forgotpassword_description)

        }

    }

    private fun forgotPassword() {
        if (isValid()) {
            if (isNetworkAvailable(mContext)) {

                ApiRequest<Any>(activity = mContext, objectType = ApiInitialize.initialize().send_otp(Prefs.getValue(this@ForgotPasswordActivity, AppConstant.SELECT_LNG, "en")!!, "+" +
                        ccp_forgotpassword_country.selectedCountryCode,
                        cl_forgotpassword_Number.text.toString().trim(),
                        isRegister.toString(), ""),
                        TYPE = WebConstant.FORGOT_PASSWORD_API,
                        isShowProgressDialog = true,
                        apiResponseInterface = this@ForgotPasswordActivity)
            } else {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.FORGOT_PASSWORD_API -> {
                val response = apiResponseManager.response as AccountOTPResponse
                when (response.status) {

                    200 -> {
                        if (mAction == "MobileNo") {

                            val countryCode = "+" + ccp_forgotpassword_country.selectedCountryCode

                            start<VerificationCodeActivity>("OTP" to response.data?.otp.toString(),
                                    "USERID" to response.data?.user_id.toString(),
                                    "IsRegister" to "2",
                                    "Number" to cl_forgotpassword_Number.text.toString().trim(),
                                    "CountryCode" to countryCode)

                            finish()

                        } else {
                            start<VerificationCodeActivity>("OTP" to response.data?.otp.toString(),
                                    "USERID" to response.data?.user_id.toString(),
                                    "IsRegister" to "0")

                        }

                    }
                    else -> showToast(response.message!!, mContext)
                }
            }


        }
    }

    private fun isValid(): Boolean {
        Helper.hideKeyboard(mContext)

        return when {
            TextUtils.isEmpty(cl_forgotpassword_Number.text.toString().trim()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_login_number_error), false, "OK", null)
                false
            }
            (cl_forgotpassword_Number.text.toString().trim() == Number && mAction == "MobileNo") -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_mobile_number), false, "OK", null)
                false
            }
            else -> true
        }
    }


}
