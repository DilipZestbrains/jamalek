package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

public class SalonListData(`in`: Parcel) : Serializable
{

    @SerializedName("id")
    var id: String? = null

    @SerializedName("profile_image")
    var profile_image: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("name")
    var name: String? = null

}
