package com.zestbrains.jamalek.model.booking

import com.google.gson.annotations.SerializedName

data class SingleBookingResponse(

        @field:SerializedName("data")
        val data: MyBookingData,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)