package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

data class SalonResponse(

        @field:SerializedName("data")
        val data: ArrayList<Data>,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)