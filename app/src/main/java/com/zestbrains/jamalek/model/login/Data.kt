package com.zestbrains.jamalek.model.login

import com.google.gson.annotations.SerializedName

data class Data(


        @field:SerializedName("id")
        val id: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("type")
        val type: String? = null,

        @field:SerializedName("email")
        val email: String? = null,

        @field:SerializedName("country_code")
        val countryCode: String? = null,

        @field:SerializedName("mobile")
        val mobile: String? = null,

        @field:SerializedName("online")
        val online: String? = null,

        @field:SerializedName("longitude")
        val longitude: String? = null,

        @field:SerializedName("latitude")
        val latitude: String? = null,

        @field:SerializedName("notification")
        val notification: Int? = 0,

        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("profile_image")
        val profileImage: String? = null,

        @field:SerializedName("profile_image_zoom")
        val profileImageZoom: String? = null,

        @field:SerializedName("vAuthToken")
        val vAuthToken: String? = null,

        @field:SerializedName("upcoming_orders")
        val upcoming_orders: String? = null,

        @field:SerializedName("completed_orders")
        val completed_orders: String? = null,

        @field:SerializedName("canceled_orders")
        val canceled_orders: String? = null)


