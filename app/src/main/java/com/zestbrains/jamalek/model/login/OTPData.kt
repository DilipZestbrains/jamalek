package com.zestbrains.jamalek.model.login

import com.google.gson.annotations.SerializedName

data class OTPData(

        @field:SerializedName("otp")
        val otp: String? = null,

        @field:SerializedName("user_id")
        val user_id: String? = null

)