package com.zestbrains.jamalek.model.booking

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ServiceData constructor(`in`: Parcel) : Serializable {


    @SerializedName("id")
    var id: String? = null
    @SerializedName("booking_id")

    var bookingId: String? = null
    @SerializedName("service_id")

    var serviceId: String? = null
    @SerializedName("stylist_id")

    var stylistId: String? = null
    @SerializedName("start_time")

    var startTime: String? = null
    @SerializedName("end_start")

    var endStart: String? = null
    @SerializedName("price")

    var price: String? = null
    @SerializedName("service_for")

    var serviceFor: Int? = 1
    @SerializedName("created")

    var created: String? = null
    @SerializedName("upadated")

    var upadated: String? = null
    @SerializedName("name")

    var name: String? = null
    @SerializedName("profile_image")

    var profileImage: String? = null
    @SerializedName("service_name")

    var serviceName: String? = null

}
