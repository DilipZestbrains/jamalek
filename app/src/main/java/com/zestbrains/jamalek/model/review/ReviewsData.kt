package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ReviewsData protected constructor(`in`: Parcel) : Serializable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profile_image")
    var profile_image: String? = null

    @SerializedName("salon_id")
    var salon_id: String? = null

    @SerializedName("user_id")
    var user_id: String? = null

    @SerializedName("rating")
    var rating: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("updated")
    var updated: String? = null


    @SerializedName("like_description")
    var like_description: String? = null

    @SerializedName("dislike_description")
    var dislike_description: String? = null

}
