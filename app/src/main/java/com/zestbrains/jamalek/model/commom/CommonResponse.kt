package com.zestbrains.jamalek.model.commom

import com.google.gson.annotations.SerializedName

data class CommonResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)