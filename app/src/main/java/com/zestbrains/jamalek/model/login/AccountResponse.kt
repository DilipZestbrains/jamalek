package com.zestbrains.jamalek.model.login

import com.google.gson.annotations.SerializedName

data class AccountResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)