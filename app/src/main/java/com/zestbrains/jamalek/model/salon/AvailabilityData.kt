package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AvailabilityData(`in`: Parcel) : Serializable {

    @SerializedName("date")
    var date: String? = null

    @SerializedName("available")
    var available: Int? = 0


    @SerializedName("time")
    var mTime: ArrayList<Time>? = null


    inner class Time protected constructor(`in`: Parcel) : Serializable
    {

        @SerializedName("start")
        var start: String? = null

        @SerializedName("end")
        var end: String? = null

        @SerializedName("stylist")
        var mStylist: ArrayList<Stylist>? = null
    }


    inner class Stylist protected constructor(`in`: Parcel) : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("profile_image")
        var profile_image: String? = null

        @SerializedName("isSelected")
        var isSelected: Boolean = false
    }

}
