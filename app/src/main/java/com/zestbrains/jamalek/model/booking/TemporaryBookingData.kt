package com.zestbrains.jamalek.model.booking;

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import com.zestbrains.jamalek.model.salon.Data


import java.io.Serializable


class TemporaryBookingData protected constructor(`in`: Parcel) : Serializable {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("booking_id")
    var bookingId: String? = null

    @SerializedName("user_id")
    var userId: String? = null

    @SerializedName("salon_id")
    var salonId: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("promocode")
    var promocode: String? = null

    @SerializedName("promocode_type")
    var promocodeType: String? = null

    @SerializedName("promocode_price")
    var promocodePrice: String? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("service_charge")
    var serviceCharge: String? = null

    @SerializedName("service_charge_price")
    var serviceChargePrice: String? = null

    @SerializedName("total_price")
    var totalPrice: String? = null

    @SerializedName("transaction_id")
    var transactionId: String? = null

    @SerializedName("location_type")
    var locationType: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("house_number")
    var houseNumber: String? = null

    @SerializedName("landmark")
    var landmark: String? = null

    @SerializedName("instructions")
    var instructions: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("updated")
    var updated: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("profile_image")
    var profileImage: String? = null


    @field:SerializedName("salon_data")
    val mSalonData: Data? = null

    @SerializedName("service_list")
    var serviceList: ArrayList<ServiceData>? = null


}
