package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName
import com.zestbrains.jamalek.model.address.AddressData

data class AddressListResponse(

        @field:SerializedName("data")
        val data: ArrayList<AddressData>,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)