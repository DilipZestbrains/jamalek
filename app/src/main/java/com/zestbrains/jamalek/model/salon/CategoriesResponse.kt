package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CategoriesResponse {


    @SerializedName("status")
    var status: Int? = null


    @SerializedName("message")
    var message: String? = null


    @SerializedName("data")
    var data: ArrayList<Data>? = null


    inner class Data protected constructor(`in`: Parcel) : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("image")
        var image: String? = null

    }
}
