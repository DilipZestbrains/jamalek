package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

data class SalonByIDResponse(

        @field:SerializedName("data")
        val data: Data,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)
