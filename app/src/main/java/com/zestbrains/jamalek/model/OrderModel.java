package com.zestbrains.jamalek.model;



public class OrderModel {


  public String news_title1;
  public String news_body1;

    OrderModel(){}

    public OrderModel(String news_title1, String news_body1) {
        this.news_title1 = news_title1;
        this.news_body1 = news_body1;
    }

    public String getNewsTitle() {
        return news_title1;
    }

    public void setNewsTitle(String news_title1) {
        this.news_title1 = news_title1;
    }

    public String getNewsBody() {
        return news_body1;
    }

    public void setNewsBody(String news_body1) {
        this.news_body1 = news_body1;
    }
}
