package com.zestbrains.jamalek.model.login

import com.google.gson.annotations.SerializedName

data class AccountOTPResponse(

	@field:SerializedName("data")
	val data: OTPData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)