package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MenuData protected constructor(`in`: Parcel) : Serializable
{

    @SerializedName("id")
    var id: String? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("salon_id")
    var salon_id: String? = null

    @SerializedName("services")
    var mServices: ArrayList<Services>? = null

    inner class Services protected constructor(`in`: Parcel) : Serializable{

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("price")
        var price: String? = null

        @SerializedName("for_men")
        var for_men: Int? = 0

        @SerializedName("for_women")
        var for_women: Int? = 0

        @SerializedName("for_kids")
        var for_kids: Int? = 0


    }

}
