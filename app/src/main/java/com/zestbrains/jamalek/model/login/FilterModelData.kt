package com.zestbrains.jamalek.model.login

import com.google.gson.annotations.SerializedName

data class FilterModelData(

        @field:SerializedName("ws_lang")
        var ws_lang: String? = "en",

        @field:SerializedName("Vauthtoken")
        var Vauthtoken: String? = "",

        @field:SerializedName("lat")
        var lat: String? = "23.46",

        @field:SerializedName("lng")
        var lng: String? = "72.36",

        @field:SerializedName("time_zone")
        var time_zone: String? = "Asia/Kolkata",


        @field:SerializedName("for_men")
        var for_men: String? = "",

        @field:SerializedName("for_women")
        var for_women: String? = "",

        @field:SerializedName("for_kids")
        var for_kids: String? = "",

        @field:SerializedName("salon")
        var salon: String? = "",

        @field:SerializedName("in_house")
        var in_house: String? = "",

        @field:SerializedName("stylist")
        var stylist: String? = "",

        @field:SerializedName("distance")
        var distance: String? = "",

        @field:SerializedName("start_time")
        var start_time: String? = "",

        @field:SerializedName("category_ids")
        var category_ids: String? = "")


