package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

data class WriteReviewsResponse(

        @field:SerializedName("data")
        val data: ReviewsData,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)