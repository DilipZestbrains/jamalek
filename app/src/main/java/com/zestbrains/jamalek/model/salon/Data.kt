package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import android.os.Parcelable
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

public class Data(`in`: Parcel) : Serializable
{

    @SerializedName("id")
    var id: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("salon")
    var salon: String? = null

    @SerializedName("in_house")
    var inHouse: String? = null

    @SerializedName("for_men")
    var forMen: String? = null

    @SerializedName("for_women")
    var forWomen: String? = null

    @SerializedName("for_kids")
    var forKids: String? = null

    @SerializedName("category_ids")
    var categoryIds: String? = null

    @SerializedName("details")
    var details: String? = null

    @SerializedName("logo")
    var logo: String? = null

    @SerializedName("distance")
    var distance: String? = ""

    @SerializedName("ratings")
    var ratings: String? = null

    @SerializedName("review")
    var review: String? = null

    @SerializedName("own_rated")
    var own_rated: String? = null


    @SerializedName("vendor_timing")
    var vendorTiming: List<VendorTiming>? = null

    @SerializedName("vendor_category")
    var vendorCategory: ArrayList<VendorCategoies>? = null

    @SerializedName("gallery_images")
    var gallery_images: ArrayList<GalleryImages>? = null


    inner class VendorTiming protected constructor(`in`: Parcel) : Serializable{

        @SerializedName("id")
        var id: String? = null

        @SerializedName("vendor_id")
        var vendorId: String? = null

        @SerializedName("day")
        var day: String? = null

        @SerializedName("start_time")
        var startTime: String? = null

        @SerializedName("end_time")
        var endTime: String? = null

        @SerializedName("is_working_day")
        var isWorkingDay: String? = null

        @SerializedName("insert_date")
        var insertDate: String? = null

        @SerializedName("fday")
        var fday: String? = null

        @SerializedName("open")
        var open: String? = null

        @SerializedName("today")
        var today: String? = null
    }

    inner class GalleryImages protected constructor(`in`: Parcel) : Serializable{

        @SerializedName("id")
        var id: String? = null

        @SerializedName("image")
        var image: String? = null

    }

    companion object {

       @JvmField
       var  DIFF_CALL: DiffUtil.ItemCallback<Data> = object : DiffUtil.ItemCallback<Data>() {
           override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
                return oldItem.id == newItem.id
            }

          override  fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
                return oldItem.id == newItem.id
            }
    }
    }
}
