package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

data class OfferResponse(

        @field:SerializedName("data")
        val data: ArrayList<OfferData>,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)