package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

class BookedDataModel {

    @SerializedName("servicecategory")
    var mCategoryId: String? = null

    @SerializedName("servicecategory_name")
    var mCategory_name: String? = null

    @SerializedName("serviceid")
    var mService_id: String? = null

    @SerializedName("servicename")
    var mService_name: String? = null

    @SerializedName("serviceprice")
    var mService_price: String? = null

    @SerializedName("start")
    var startTime: String? = null

    @SerializedName("end")
    var endTime: String? = null

    @SerializedName("stylist_id")
    var stylist_id: String? = null

    @SerializedName("stylist_name")
    var stylist_name: String? = null

    @SerializedName("stylist_profile_image")
    var stylist_profile_image: String? = null

    @SerializedName("day")
    var mDay: String? = null

    @SerializedName("date")
    var mDate: String? = null

}
