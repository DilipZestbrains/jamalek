package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName

data class AvailabilityResponse(

        @field:SerializedName("data")
        val data: ArrayList<AvailabilityData>,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)