package com.zestbrains.jamalek.model.salon

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OfferData(`in`: Parcel) : Serializable {
    @field:SerializedName("id")
    val id: String? = null

    @field:SerializedName("user_id")
    val user_id: String? = null

    @field:SerializedName("start_date")
    val start_date: String? = null

    @field:SerializedName("end_date")
    val end_date: String? = null

    @field:SerializedName("code")
    val code: String? = null

    @field:SerializedName("title")
    val title: String? = null

    @field:SerializedName("description")
    val description: String? = null

    @field:SerializedName("type")
    val type: String? = null

    @field:SerializedName("discount")
    val discount: String? = null

    @field:SerializedName("status")
    val status: String? = null

    @field:SerializedName("created")
    val created: String? = null

    @field:SerializedName("updated")
    val updated: String? = null

    @field:SerializedName("name")
    val name: String? = null

    @field:SerializedName("profile_image")
    val profile_image: String? = null


    @field:SerializedName("user_type")
    val user_type: String? = null


}
