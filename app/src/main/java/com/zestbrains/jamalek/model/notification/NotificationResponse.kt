package com.zestbrains.jamalek.model.salon

import com.google.gson.annotations.SerializedName
import com.zestbrains.jamalek.model.address.AddressData
import com.zestbrains.jamalek.model.notification.NotificationData

data class NotificationResponse(

        @field:SerializedName("data")
        val data: ArrayList<NotificationData>,

        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: Int? = null
)