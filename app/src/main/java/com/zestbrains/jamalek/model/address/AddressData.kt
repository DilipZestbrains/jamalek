package com.zestbrains.jamalek.model.address

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AddressData(`in`: Parcel) : Serializable {
    @field:SerializedName("id")
    val id: String? = null

    @field:SerializedName("user_id")
    val user_id: String? = null

    @field:SerializedName("address")
    val address: String? = null

    @field:SerializedName("house_number")
    val house_number: String? = null

    @field:SerializedName("landmark")
    val landmark: String? = null

    @field:SerializedName("instructions")
    val instructions: String? = null

    @field:SerializedName("latitude")
    val latitude: Double? = null

    @field:SerializedName("longitude")
    val longitude: Double? = null

    @field:SerializedName("status")
    val status: String? = null

    @field:SerializedName("created")
    val created: String? = null

    @field:SerializedName("updated")
    val updated: String? = null
}
