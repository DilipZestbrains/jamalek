package com.zestbrains.jamalek.model.notification

import android.os.Parcel
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NotificationData(`in`: Parcel) : Serializable {
    @field:SerializedName("id")
    val id: String? = null

    @field:SerializedName("push_type")
    val push_type: String? = null

    @field:SerializedName("name")
    val name: String? = null

    @field:SerializedName("profile_image")
    val profile_image: String? = null

    @field:SerializedName("read_flag")
    val read_flag: String? = null

    @field:SerializedName("user_id")
    val user_id: String? = null

    @field:SerializedName("from_user_id")
    val from_user_id: String? = null

    @field:SerializedName("booking_id")
    val booking_id: String? = null

    @field:SerializedName("push_message")
    val push_message: String? = null

    @field:SerializedName("push_from")
    val push_from: String? = null

    @field:SerializedName("insert_date")
    val insert_date: String? = null


}
