package com.zestbrains.jamalek.address


import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.zestbrains.jamalek.Interfaces.OnItemAddressSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.address.AddressData
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.model.salon.AddressListResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.ADDRESS_ACTION
import kotlinx.android.synthetic.main.activity_selecte_address.*


class SelectAddressActivity : BaseActivity(), OnItemAddressSelectListener, ApiResponseInterface {

    private lateinit var mAddressListAdapter: AdderssListAdapter
    private var mSelectedPos: Int = 0
    private var mAction: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        setContentView(R.layout.activity_selecte_address)

        val b = intent.extras
        if (b == null || !b.containsKey(ADDRESS_ACTION))
            onBackPressed()
        mAction = b!!.getString(ADDRESS_ACTION).toString()

        setData(mAction)

        btnAddNewAddress.setSafeOnClickListener {
            mSelectedPos = -90
            val intent = Intent(this, NewAddressActivity::class.java).putExtra("Action", "ADD")
            startActivityForResult(intent, 100)
        }

        btnBack.setSafeOnClickListener { onBack() }

        callAPI()
    }


    override fun onBackPressed() {
        onBack()
    }

    private fun setData(mAction: String) {

        mAddressListAdapter = AdderssListAdapter(this, this@SelectAddressActivity, mAction)
        val linearLayoutManager = LinearLayoutManager(this@SelectAddressActivity, LinearLayoutManager.VERTICAL, false)
        rvcAddressList.layoutManager = linearLayoutManager
        rvcAddressList.adapter = mAddressListAdapter
    }


    private fun callAPI() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().getAddress(ws_lang = Prefs.getValue(this@SelectAddressActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!, limit = 10, offset = 0),
                    TYPE = WebConstant.GET_ADDRESS_LIST,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), this)
        }
    }

    private fun deleteAPI(mModel: AddressData?) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().deleteAddress(ws_lang = Prefs.getValue(this@SelectAddressActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!, id = mModel!!.id.toString()),
                    TYPE = WebConstant.DELETE_ADDRESS,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), this)
        }
    }


    fun onBack() {
        if (mAction == "Select") {
            intent.putExtra("ACTION", "NOSELECT")
            val intent = Intent()
            setResult(100, intent)
        }
        finish()
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {

        when (apiResponseManager.type) {
            WebConstant.GET_ADDRESS_LIST -> {
                val response = apiResponseManager.response as AddressListResponse
                when (response.status) {
                    200 -> {
                        mAddressListAdapter.load(response.data)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
            WebConstant.DELETE_ADDRESS -> {
                val response = apiResponseManager.response as CommonResponse
                when (response.status) {
                    200 -> {

                        mAddressListAdapter.remove(mSelectedPos)
                        mSelectedPos = 0
                        showToast(response.message!!, this)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
        }
    }

    override fun OnItemSelectListener(pos: Int, title: String?, mModel: AddressData?) {
        mSelectedPos = pos

        when (title) {
            "Select" -> {

                val intent = Intent()
                intent.putExtra("ACTION", "SELECT")
                intent.putExtra(AppConstant.ADDRESS_DATA, mModel)
                setResult(100, intent)
                finish()
            }
            "Delete" -> {
                deleteAPI(mModel)
            }
            else -> {
                val intent = Intent(this, NewAddressActivity::class.java).putExtra("Action", "Edit").putExtra(AppConstant.ADDRESS_DATA, mModel)
                startActivityForResult(intent, 100)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> {
                if(data!=null){
                    val b = data!!.extras
                    if (b != null || b!!.containsKey("BACK"))
                    {
                        val bb = b.getString("BACK")
                        if (bb != "NOChanges")
                        {
                            val data = b.getSerializable(AppConstant.ADDRESS_DATA) as AddressData
                            mAddressListAdapter.add(pos = mSelectedPos, mData = data)
                        }
                    }
                }

                return
            }

        }
    }

}
