package com.zestbrains.jamalek.address

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.Interfaces.OnItemAddressSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.address.AddressData
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.setSafeOnClickListener
import java.util.*

class AdderssListAdapter(private val onClick: OnItemAddressSelectListener, private val mActivity: Context, private val mAction: String) : RecyclerView.Adapter<AdderssListAdapter.MyViewHolder>() {

    private var mModel = ArrayList<AddressData>()

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var btnSelectAddress: com.google.android.material.button.MaterialButton = itemView.findViewById(R.id.btnSelectAddress)
        var tvAddress: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvAddress)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_address, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        holder.tvAddress.text = mModel[listPosition].address

        if (mAction == "Select") {
            holder.btnSelectAddress.text = getStr(mActivity, R.string.choose)
        } else
            holder.btnSelectAddress.text = getStr(mActivity, R.string.delete)

        holder.btnSelectAddress.setSafeOnClickListener {
            onClick.OnItemSelectListener(listPosition, mAction, mModel[listPosition])
        }

        holder.tvAddress.setSafeOnClickListener {
            if (mAction == "Select")
                onClick.OnItemSelectListener(listPosition, mAction, mModel[listPosition])
            else
                onClick.OnItemSelectListener(listPosition, "Edit", mModel[listPosition])
        }
    }


    override fun getItemCount(): Int {
        return mModel.size
    }

    fun load(mData: ArrayList<AddressData>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun add(mData: AddressData, pos: Int) {

        Log.e("pos" ,pos.toString())
        if (pos == -90) {
            mModel.add(mData)
            notifyItemInserted(mModel.size - 1)
            notifyDataSetChanged()
        } else {
            mModel[pos] = mData
            notifyItemChanged(pos)
        }

    }

    fun remove(pos: Int) {
        if (pos < mModel.size) {
            mModel.removeAt(pos)
            notifyDataSetChanged()
        }
    }


    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }

}