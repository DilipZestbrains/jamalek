package com.zestbrains.jamalek.address


import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.zestbrains.jamalek.Interfaces.PermissionStatus
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.model.address.AddressData
import com.zestbrains.jamalek.model.salon.NewAddressResponse
import com.zestbrains.jamalek.permission.PermissionChecker
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_new_address.*
import java.util.*


class NewAddressActivity : BaseActivity(), OnMapReadyCallback, PermissionStatus, ApiResponseInterface {


    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private var myLocation: LatLng? = null
    private var mAction: String = "Add"
    private var mData: AddressData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        setContentView(R.layout.activity_new_address)

        btnBack.setSafeOnClickListener {
            onBack()
        }

        val b = intent.extras
        if (b == null || !b.containsKey("Action"))
            onBackPressed()
        mAction = b!!.getString("Action").toString()

        viewFlat.visible()
        viewLandmark.visible()
        viewHowtoReach.visible()

        when (mAction) {
            "Edit" -> {
                mData = b.getSerializable(AppConstant.ADDRESS_DATA) as AddressData
                ivChange.text = mData!!.address
                edtFalt.setText(mData!!.house_number)
                edtLandmark.setText(mData!!.landmark)
                tvHowtoReach.setText(mData!!.instructions)
                myLocation = LatLng(mData!!.latitude!!, mData!!.longitude!!)
                btnSave.text = getStr(this@NewAddressActivity, R.string.save_proceed)
            }
            "Filter" -> {
                viewFlat.invisible()
                viewLandmark.invisible()
                viewHowtoReach.invisible()
                btnSave.text = getStr(this@NewAddressActivity, R.string.select_address)
            }
            else -> {
                btnSave.text = getStr(this@NewAddressActivity, R.string.save_proceed)
            }
        }


        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment!!.onCreate(savedInstanceState)

        checkForPermission()


        btnSave.setSafeOnClickListener {

            if (mAction == "Filter") {
                val intent = Intent()
                intent.putExtra(AppConstant.ADDRESS_DATA, getAddress())
                intent.putExtra(AppConstant.LAT, myLocation!!.latitude.toString())
                intent.putExtra(AppConstant.LONG, myLocation!!.longitude.toString())
                setResult(100, intent)
                finish()
            } else
                callAPI()
        }
    }

    override fun onBackPressed() {
        onBack()
    }

    private fun initMap() {

        try {
            MapsInitializer.initialize(this@NewAddressActivity.applicationContext)
            mapFragment!!.getMapAsync(this)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun checkForPermission() {
        PermissionChecker(this@NewAddressActivity, this@NewAddressActivity,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        mMap?.isMyLocationEnabled = true

        if (mAction == "ADD" || mAction == "Filter") {
            myLocation = LatLng(GPSTracker(this@NewAddressActivity).latitude, GPSTracker(this@NewAddressActivity).longitude)
            setAddress()
        }
        val cameraPosition = CameraPosition.Builder().target(myLocation).zoom(15f).build()
        mMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        mMap!!.setOnCameraIdleListener {
            myLocation = mMap!!.cameraPosition.target
            setAddress()
        }


    }

    override fun allGranted() {
        initMap()
    }

    override fun onDenied() {

    }

    override fun foreverDenied() {
        SnackBar.show(this@NewAddressActivity, "Please enable permission", "ENABLE", View.OnClickListener {
            ShowToast.show(this@NewAddressActivity, "click")
        })
    }

    @SuppressLint("SetTextI18n")
    private fun setAddress() {
        val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
        val addresses: List<Address>

        if (isNetworkAvailable(this)) {
            addresses = geocoder.getFromLocation(myLocation!!.latitude, myLocation!!.longitude, 1)

            if (addresses.isNotEmpty()) {
                val address = addresses[0].getAddressLine(0)
                val city = addresses[0].locality
                val state = addresses[0].adminArea
                val country = addresses[0].countryName
                val postalCode = addresses[0].postalCode
                val knownName = addresses[0].featureName

                ivChange.text = address
            } else
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
        }


    }

    private fun callAPI() {
        if (isNetworkAvailable(this)) {

            Helper.hideKeyboard(this)
            if (isValid()) {
                when (mAction) {
                    "ADD" -> ApiRequest<Any>(
                            activity = this,
                            objectType = ApiInitialize.initialize().addEditAddress(ws_lang = Prefs.getValue(this@NewAddressActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                                    AppConstant.AUTHORIZATION_TOKEN, "")!!, id = "0", address = getAddress(), house_number = getFlatNo(),
                                    landmark = getLanmark(), instructions = getHowtoReach(), latitude = myLocation!!.latitude, longitude = myLocation!!.longitude),
                            TYPE = WebConstant.EDIT_ADDRESS,
                            isShowProgressDialog = true,
                            apiResponseInterface = this)
                    else -> ApiRequest<Any>(
                            activity = this,
                            objectType = ApiInitialize.initialize().addEditAddress(ws_lang = Prefs.getValue(this@NewAddressActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                                    AppConstant.AUTHORIZATION_TOKEN, "")!!, id = mData!!.id!!, address = getAddress(), house_number = getFlatNo(),
                                    landmark = getLanmark(), instructions = getHowtoReach(), latitude = myLocation!!.latitude, longitude = myLocation!!.longitude),
                            TYPE = WebConstant.EDIT_ADDRESS,
                            isShowProgressDialog = true,
                            apiResponseInterface = this)
                }
            }
        } else {
            SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "", null)
        }

    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.EDIT_ADDRESS -> {
                val response = apiResponseManager.response as NewAddressResponse
                when (response.status) {
                    200 -> {
                        val intent = Intent()
                        intent.putExtra("BACK", "Changes")
                        intent.putExtra(AppConstant.ADDRESS_DATA, response.data)
                        setResult(100, intent)
                        finish()

                        showToast(response.message!!, this)
                    }
                    else -> showToast(response.message!!, this)
                }
            }
        }

    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)


    }


    private fun isValid(): Boolean {
        Helper.hideKeyboard(this@NewAddressActivity)

        return when {
            TextUtils.isEmpty(getAddress()) -> {
                Helper.hideKeyboard(this@NewAddressActivity)
                SnackBar.show(mContext, true, getStr(this@NewAddressActivity, R.string.str_address), false, "OK", null)
                false
            }

            TextUtils.isEmpty(getFlatNo()) -> {
                Helper.hideKeyboard(this@NewAddressActivity)
                SnackBar.show(mContext, true, getStr(this@NewAddressActivity, R.string.str_flat_house_no), false, "", null)
                false
            }
            TextUtils.isEmpty(getLanmark()) -> {
                Helper.hideKeyboard(this@NewAddressActivity)
                SnackBar.show(mContext, true, getStr(this@NewAddressActivity, R.string.str_landmark), false, "", null)
                false
            }

            else -> true
        }
    }

    private fun getAddress(): String {
        return ivChange.text.toString().trim()
    }

    private fun getFlatNo(): String {
        return edtFalt.text.toString().trim()
    }

    private fun getLanmark(): String {
        return edtLandmark.text.toString().trim()
    }

    private fun getHowtoReach(): String {
        return tvHowtoReach.text.toString().trim()
    }

    private fun onBack() {

        val intent = Intent()
        if (mAction == "Filter") {
            intent.putExtra(AppConstant.ADDRESS_DATA, getAddress())
            intent.putExtra(AppConstant.LAT, myLocation!!.latitude.toString())
            intent.putExtra(AppConstant.LONG, myLocation!!.longitude.toString())
            setResult(100, intent)
            finish()
        } else {
            intent.putExtra("BACK", "NOChanges")
            setResult(100, intent)
            finish()
        }


    }

}
