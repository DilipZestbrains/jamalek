package com.zestbrains.jamalek.restapi

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Parcel
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.LoginActivity
import com.zestbrains.jamalek.restapi.WebConstant.ADD_SERVICE
import com.zestbrains.jamalek.utils.dismissDialog1
import com.zestbrains.jamalek.utils.getProgressDialog
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.showToast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("ParcelCreator")
class ApiRequest<T>(private val activity: Activity,
                    objectType: T,
                    private val TYPE: Int,
                    private val isShowProgressDialog: Boolean,
                    private val apiResponseInterface: ApiResponseInterface) : Callback<T>,
        Parcelable {
    private var mProgressDialog: ProgressDialog? = null
    private var retryCount = 0
    private var call: Call<T>? = null

    init {
        showProgress()
        call = objectType as Call<T>?
        if (call?.isExecuted!!) call?.cancel()
        call!!.enqueue(this)
    }

    private fun showProgress() {
        if (isShowProgressDialog) {
            mProgressDialog = getProgressDialog(activity)
        }
    }

    private fun dismissProgress() {
        if (isShowProgressDialog) {
            dismissDialog1(activity, mProgressDialog!!)
        }
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        dismissProgress()
        Log.e("API : ", call.request().url.toString() + " CODe " + response.code())
        Log.e("API : ", "response : " +  response)
        when {
            response.isSuccessful -> apiResponseInterface.getApiResponse(ApiResponseManager(response.body(), TYPE))
            response.code() == 503 -> Toast.makeText(activity, response.message().toString(), Toast.LENGTH_SHORT).show()
            response.code() == 500 -> Toast.makeText(activity, response.message().toString(), Toast.LENGTH_SHORT).show()

            else -> {
                val error = ErrorUtils.parseError(response)
                when {
                    response.code() == 401 -> {
                        showToast(getStr(activity, R.string.please_login_to_continue), activity)
                        val mIntent = Intent(activity, LoginActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                    }

                    response.code() == 405 -> {
                        apiResponseInterface.getApiResponse(ApiResponseManager(error, 405))
                    }

                    (TYPE == ADD_SERVICE && response.code() == 412) -> {
                        showToast(error.message!!, activity)
                        apiResponseInterface.getApiResponse(ApiResponseManager(response, 1000))
                    }

                    error.message != null -> {
                        showToast(error.message!!, activity)
                    }
                }
            }
        }
    }

    override fun onFailure(call: Call<T>, error: Throwable) {
        error.printStackTrace()
        Log.e(TAG, "onFailure: " + error.message)
        if (retryCount++ < TOTAL_RETRIES) {
            Log.v(TAG, "Retrying... ($retryCount out of $TOTAL_RETRIES)")
            retry()
            return
        }
        dismissProgress()
    }

    private fun retry() {
        call!!.clone().enqueue(this)
    }

    companion object {
        private const val TAG = "ApiRequest"
        private const val TOTAL_RETRIES = 2
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(TYPE)
        parcel.writeByte(if (isShowProgressDialog) 1 else 0)
        parcel.writeInt(retryCount)
    }

    override fun describeContents(): Int {
        return 0
    }

}
