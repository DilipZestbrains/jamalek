package com.zestbrains.jamalek.restapi


import com.zestbrains.jamalek.model.booking.MyBookingResponse
import com.zestbrains.jamalek.model.booking.SingleBookingResponse
import com.zestbrains.jamalek.model.booking.TemporaryBookingResponse
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.model.login.AccountOTPResponse
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.model.salon.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("user/login")
    fun login(
            @Header("ws_lang") ws_lang: String,
            @Field("country_code") country_code: String,
            @Field("mobile") mobile: String,
            @Field("password") password: String,
            @Field("eDeviceType") eDeviceType: String,
            @Field("vPushToken") vPushToken: String
    ): Call<AccountResponse>

    /*--------------------------------------------------------------------------------------------*/


    @FormUrlEncoded
    @POST("user/send_otp")
    fun send_otp(
            @Header("ws_lang") ws_lang: String,
            @Field("country_code") country_code: String,
            @Field("mobile") mobile: String,
            @Field("is_registration") is_registration: String,
            @Field("email") email: String
    ): Call<AccountOTPResponse>

    /*--------------------------------------------------------------------------------------------*/


    @FormUrlEncoded
    @POST("user/register_user")
    fun register(
            @Header("ws_lang") ws_lang: String,
            @Field("name") name: String,
            @Field("email") email: String,
            @Field("country_code") country_code: String,
            @Field("mobile") mobile: String,
            @Field("password") password: String,
            @Field("eDeviceType") eDeviceType: String,
            @Field("vPushToken") vPushToken: String

    ): Call<AccountResponse>


    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/change_password")
    fun changePassword(
            @Header("ws_lang") ws_lang: String,
            @Field("vOldPassword") vOldPassword: String,
            @Field("vNewPassword") vNewPassword: String,
            @Field("id") id: String,
            @Field("is_forgot") is_forgot: String
    ): Call<CommonResponse>


    /*--------------------------------------------------------------------------------------------*/

    @Multipart
    @POST("user/edit_profile")
    fun editProfile(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Part profile_image: MultipartBody.Part,
            @Part("email") email: RequestBody,
            @Part("name") name: RequestBody
    ): Call<AccountResponse>

    /*--------------------------------------------------------------------------------------------*/

    @Multipart
    @POST("user/edit_profile")
    fun editProfile(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Part("email") email: RequestBody,
            @Part("name") name: RequestBody
    ): Call<AccountResponse>


    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/change_number")
    fun changeMobileNo(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("mobile") name: String,
            @Field("country_code") country_code: String
    ): Call<AccountResponse>


    /*--------------------------------------------------------------------------------------------*/

    @GET("user/logout")
    fun logout(
            @Header("Vauthtoken") authtoken: String
    ): Call<CommonResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/social_login")
    fun socialLogin(
            @Header("ws_lang") ws_lang: String,
            @Field("google_id") google_id: String,
            @Field("facebook_id") facebook_id: String,
            @Field("social_type") social_type: String,
            @Field("email") email: String,
            @Field("name") name: String,
            @Field("country_code") country_code: String,
            @Field("mobile") mobile: String,
            @Field("eDeviceType") eDeviceType: String,
            @Field("vPushToken") vPushToken: String,
            @Field("checkExist") checkExist: String,
            @Field("profile_image") profile_image: String
    ): Call<AccountResponse>


    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/get_salon")
    fun homeDetails(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("lat") lat: String,
            @Field("lng") lng: String,
            @Field("limit") glimit: Int,
            @Field("offset") goffset: Int,
            @Field("time_zone") time_zone: String,
            @Field("for_men") for_men: String,
            @Field("for_women") for_women: String,
            @Field("for_kids") for_kids: String,
            @Field("salon") salon: String,
            @Field("in_house") in_house: String,
            @Field("stylist") stylist: String,
            @Field("distance") distance: String,
            @Field("start_time") start_time: String,
            @Field("category_ids") category_ids: String): Call<SalonResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/get_salon")
    fun homeDetails(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("lat") lat: String,
            @Field("lng") lng: String,
            @Field("limit") glimit: Int,
            @Field("offset") goffset: Int,
            @Field("time_zone") time_zone: String,
            @Field("for_men") for_men: String,
            @Field("for_women") for_women: String,
            @Field("for_kids") for_kids: String,
            @Field("salon") salon: String): Call<SalonResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/get_menu_by_salon")
    fun getMenuBySalonId(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("limit") glimit: Int,
            @Field("offset") goffset: Int,
            @Field("id") id: String
    ): Call<MenuResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/get_salon_review")
    fun getReviewBySalonId(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("limit") glimit: Int,
            @Field("offset") goffset: Int,
            @Field("salon_id") id: String
    ): Call<ReviewsResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/add_edit_salon_review")
    fun writeReviewBySalonId(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("rating") glimit: Float,
            @Field("like_description") like_description: String,
            @Field("dislike_description") dislike_description: String,
            @Field("id") id: Int,
            @Field("salon_id") salon_id: String
    ): Call<WriteReviewsResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/salon_availability")
    fun getSalonAvailability(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("id") id: String,
            @Field("date") date: String,
            @Field("service_id") service_id: String
    ): Call<AvailabilityResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/address_listing")
    fun getAddress(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("limit") limit: Int,
            @Field("offset") offset: Int
    ): Call<AddressListResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/add_edit_address")
    fun addEditAddress(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("id") id: String,
            @Field("address") address: String,
            @Field("house_number") house_number: String,
            @Field("landmark") landmark: String,
            @Field("instructions") instructions: String,
            @Field("latitude") latitude: Double,
            @Field("longitude") longitude: Double): Call<NewAddressResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/delete_address")
    fun deleteAddress(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("id") id: String
    ): Call<CommonResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/notification")
    fun notificationOnoff(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("type") type: Int
    ): Call<AccountResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/list_coupon")
    fun getOfferSlon(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("type") type: Int,
            @Field("salon_id") salon_id: String,
            @Field("limit") limit: Int,
            @Field("offset") offset: Int
    ): Call<OfferResponse>

    @FormUrlEncoded
    @POST("user/list_coupon")
    fun getOffer(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("type") type: Int,
            @Field("limit") limit: Int,
            @Field("offset") offset: Int
    ): Call<OfferResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/generate_temporary_booking")
    fun generateTemporaryBooking(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("salon_id") salon_id: String,
            @Field("date") date: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/add_service_to_booking")
    fun addService(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String,
            @Field("service_id") service_id: String,
            @Field("stylist_id") stylist_id: String,
            @Field("date") date: String,
            @Field("start_time") start_time: String,
            @Field("end_time") end_time: String,
            @Field("salon_id") salon_id: String,
            @Field("service_for") service_for: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/remove_service_from_booking")
    fun removeServices(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String,
            @Field("additional_items_id") additional_items_id: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/add_address_to_booking")
    fun addAddressToBooking(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String,
            @Field("address") address: String,
            @Field("house_number") house_number: String,
            @Field("landmark") landmark: String,
            @Field("instructions") instructions: String,
            @Field("latitude") latitude: String,
            @Field("longitude") longitude: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/add_promocode_to_booking")
    fun applyPromocode(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String,
            @Field("promocode_id") promocode_id: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/complete_booking")
    fun completeBooking(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String,
            @Field("stripe_token") stripe_token: String
    ): Call<TemporaryBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/my_booking")
    fun myBooking(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("limit") limit: Int,
            @Field("offset") offset: Int,
            @Field("type") type: Int
    ): Call<MyBookingResponse>

    /*--------------------------------------------------------------------------------------------*/

    @FormUrlEncoded
    @POST("user/get_single_salon")
    fun getSingleSalon(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("time_zone") time_zone: String,
            @Field("id") id: String
    ): Call<SalonByIDResponse>

    /*--------------------------------------------------------------------------------------------*/

    @GET("user/get_all_salon_stylist_listing")
    fun getAllSalon(): Call<SalonListResponse>

    /*--------------------------------------------------------------------------------------------*/

    @GET("user/get_category")
    fun getCategories(): Call<CategoriesResponse>

    /*--------------------------------------------------------------------------------------------*/


    @FormUrlEncoded
    @POST("user/notification_listing")
    fun notificationList(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("limit") limit: Int,
            @Field("offset") offset: Int

    ): Call<NotificationResponse>

    /*--------------------------------------------------------------------------------------------*/


    @FormUrlEncoded
    @POST("user/get_single_booking")
    fun get_single_booking(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String,
            @Field("booking_id") booking_id: String
    ): Call<SingleBookingResponse>

    /*--------------------------------------------------------------------------------------------*/


    @POST("user/delete_booking")
    fun deleteBooking(
            @Header("Vauthtoken") authtoken: String
    ): Call<CommonResponse>

    /*--------------------------------------------------------------------------------------------*/


    @POST("user/get_user_data")
    fun getUserData(
            @Header("ws_lang") ws_lang: String,
            @Header("Vauthtoken") authtoken: String
    ): Call<AccountResponse>

    /*--------------------------------------------------------------------------------------------*/
}


