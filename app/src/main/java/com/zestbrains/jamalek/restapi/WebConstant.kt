package com.zestbrains.jamalek.restapi

object WebConstant {

    //http://zestbrains4u.site/jamalek/ws/v1/user/login
    const val BASEURL = "http://zestbrains4u.site/jamalek/ws/v1/"
    const val MAP_KEY = "AIzaSyC1rD42cARv_dnXDPc-PFxcuPdkbmFGzQk"
    const val TOKEN = "Vauthtoken"

    const val LOGIN_API = 100
    const val SIGN_UP_API = 101
    const val CHANGE_PASSWORD_API = 103
    const val LOGOUT_API = 104
    const val FORGOT_PASSWORD_API = 105
    const val EDIT_PROFILE_API = 107
    const val HOME_DETAILS = 109
    const val GET_SALON_MENU_ITEM_LIST = 110
    const val GET_SALON_REVIEW = 111
    const val PUT_SALON_REVIEW = 112
    const val GET_SALON_MENU_ITEM_LIST_MORE = 113
    const val GET_STYLIST_AVAILABILITY = 114
    const val TEMPORARY_BOOKING = 115
    const val DELETE_ADDRESS = 116
    const val EDIT_ADDRESS = 117
    const val NOTIFICATION = 118
    const val GET_SALON_OFFER = 119
    const val CHANGE_MOBILE_NO = 120
    const val ADD_SERVICE = 121
    const val GET_ADDRESS_LIST = 123
    const val REMOVE_SERVICE = 124
    const val ADD_ADDRESS_BOOKING = 125
    const val APPLY_PROMOCODE = 126
    const val COMPLETED_BOOKING = 127
    const val MY_BOOKING = 128
    const val MY_CANCEL_BOOKING = 129
    const val MY_SHAEDULE_BOOKING = 129
    const val MY_COMPLETED_BOOKING = 130
    const val GET_ALL_SALON = 131
    const val GET_SALON_BY_ID = 132
    const val GET_ALL_CVATEGORY = 133
    const val SIGN_UP_OTP_API = 135
    const val SOCIAL_SIGN_UP_API = 136
    const val RESET_PASSWORD_API = 137
    const val GET_ADD_NEW_STYLIST_AVAILABILITY = 138
    const val GET_NOTIFICATION = 139
    const val GET_NOTIFICATION_MORE = 140
    const val BOOKING_DETAIL = 141
    const val BOOKING_REMOVE = 142
    const val BOOKING_DAY_REMOVE = 143
    const val USER_RESPONSE = 144
    const val MY_BOOKING_ALL_LOAD_MORE = 145
    const val MY_BOOKING_CANCLE_LOAD_MORE = 146
    const val MY_BOOKING_Completed_LOAD_MORE = 147
    const val MY_BOOKING_Shedule_LOAD_MORE = 147

    const val PUSH_BOOKING = 1
    const val PUSH_NOTIFICATION = 2
}
