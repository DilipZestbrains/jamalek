package com.zestbrains.jamalek.restapi


interface ApiResponseInterface {
    fun getApiResponse(apiResponseManager: ApiResponseManager<*>)
}
