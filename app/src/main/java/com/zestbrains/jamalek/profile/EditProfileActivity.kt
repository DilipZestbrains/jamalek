package com.zestbrains.jamalek.profile


import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.yalantis.ucrop.UCrop
import com.zestbrains.jamalek.Interfaces.PermissionStatus
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.camerapicker.ImagePicker
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.permission.PermissionChecker
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class EditProfileActivity : BaseActivity(), (View) -> Unit, PermissionStatus, ApiResponseInterface {

    override fun allGranted() {
        chooseProfileDialog()
    }

    override fun onDenied() {
        ShowToast.show(mContext, getStr(mContext, R.string.deny_permission))
    }

    override fun foreverDenied() {
        SnackBar.show(mContext, false, getStr(mContext, R.string.str_allow_storage), true, getStr(mContext, R.string.enable),
                View.OnClickListener {

                })
    }

    companion object {
        private const val REQ_GALLERY: Int = 1
    }


    private var profilePath: String = ""
    private val listPermissionsNeeded = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_edit_profile)

        clicks()
        if (Prefs.contain(this@EditProfileActivity, AppConstant.AUTHORIZATION_TOKEN)) {
            loadData()
        }
    }

    private fun clicks() {
        iv_edit_profile_back.setSafeOnClickListener(this)
        iv_edit_change_profile_image.setSafeOnClickListener(this)
        btn_edit_profile_update.setSafeOnClickListener(this)
    }

    private fun loadData() {

        val mUserModel = Prefs.getObject(this@EditProfileActivity, AppConstant.ACCOUNT_DATA, "", AccountResponse::class.java) as AccountResponse
        tvEditName.setText(mUserModel.data!!.name)
        tvEdiEmail.setText(mUserModel.data.email)


        Glide.with(this@EditProfileActivity).load(mUserModel.data.profileImage).placeholder(resources.getDrawable(R.drawable.img_placeholder))
                .error(resources.getDrawable(R.drawable.img_placeholder)).into(civ_edit_profile_profile)
    }


    override fun invoke(p1: View) {
        when (p1) {
            btn_edit_profile_update -> {
                updateProfileData()
            }
            iv_edit_profile_back -> onBackPressed()
            iv_edit_change_profile_image -> PermissionChecker(mContext, this@EditProfileActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA)
        }
    }


    private fun updateProfileData() {
        if (isValid()) {

            val requestFile: RequestBody
            var profileImage: MultipartBody.Part? = null
            if ((profilePath != "")) {
                requestFile = File(profilePath).asRequestBody("image/*".toMediaTypeOrNull())
                profileImage = MultipartBody.Part.createFormData("profile_image", "profile.jpg", requestFile)
            }
            val name = getName().toRequestBody("text/plain".toMediaTypeOrNull())
            val email = getEmail().toRequestBody("text/plain".toMediaTypeOrNull())

            Helper.hideKeyboard(mContext)

            if (profilePath == "") {
                ApiRequest(activity = mContext,
                        objectType = ApiInitialize.initialize()
                                .editProfile(
                                        authtoken = Prefs.getValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, defaultValue = "")!!,
                                        ws_lang = Prefs.getValue(this@EditProfileActivity, AppConstant.SELECT_LNG, "en")!!,
                                        email = email,
                                        name = name),
                        TYPE = WebConstant.EDIT_PROFILE_API,
                        isShowProgressDialog = true,
                        apiResponseInterface = this@EditProfileActivity)
            } else {
                ApiRequest(activity = mContext,
                        objectType = ApiInitialize.initialize().editProfile(
                                authtoken = Prefs.getValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, defaultValue = "")!!,
                                ws_lang =Prefs.getValue(this@EditProfileActivity, AppConstant.SELECT_LNG, "en")!!,
                                profile_image = profileImage!!,
                                email = email,
                                name = name),
                        TYPE = WebConstant.EDIT_PROFILE_API,
                        isShowProgressDialog = true,
                        apiResponseInterface = this@EditProfileActivity)
            }
        }
    }

    private fun isValid(): Boolean {
        return when {
            TextUtils.isEmpty(getName()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_name_error), false, "OK", null)
                false
            }
            getEmail().isNotEmpty() && Helper.checkEmailValidation(mContext, getEmail()) -> {
                Helper.hideKeyboard(mContext)
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_validation_email_valid_error), false, "", null)
                false

            }

            else -> true
        }
    }


    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        hideSystemUI()
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (

                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)


    }

    private fun chooseProfileDialog() {
        AlertDialog.Builder(mContext).setTitle(getStr(mContext, R.string.choose_from))
                .setItems(arrayOf(getStr(mContext, R.string.camera), getStr(mContext, R.string.gallery))) { _dialog, i ->
                    run {
                        _dialog.dismiss()
                        when (i) {
                            1 -> chooseFromGallery()
                            0 -> chooseFromCamera()
                        }
                    }
                }.create().show()
    }

    private fun chooseFromCamera() {
        ImagePicker.pickImage(this, getStr(mContext, R.string.str_select_image))
    }

    private fun chooseFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, REQ_GALLERY)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RESULT_CANCELED -> {
                return
            }
            REQ_GALLERY -> if (data != null) {
                profilePath = getProfilePath()!!
                val op = UCrop.Options()
                op.setCircleDimmedLayer(true)
                UCrop.of(data.data!!, Uri.parse(profilePath)).withAspectRatio(1f, 1f).withOptions(op).start(mContext as AppCompatActivity)
            }
            UCrop.REQUEST_CROP -> {
                try {
                    val resultUri: Uri = UCrop.getOutput(data!!)!!
                    civ_edit_profile_profile.setImageBitmap(null)
                    civ_edit_profile_profile!!.setImageURI(resultUri)
                } catch (e: Exception) {
                    profilePath = ""
                    Log.e(TAG, "onActivityResult: ${e.message}")
                }
            }
            ImagePicker.PICK_IMAGE_REQUEST_CODE -> {
                try {
                    val bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data)
                    profilePath = saveImage(bitmap!!)
                } catch (e: Exception) {
                    profilePath = ""
                    Log.e(TAG, "onActivityResult: ${e.message}")
                }
            }
        }
    }

    private fun getProfilePath(): String? {
        val folder = File(filesDir, ".Jamlek'ste")
        if (!folder.exists()) {
            folder.mkdirs()
        }
        val f = File(folder, "profile.jpg")
        f.createNewFile()
        return f.absolutePath
    }

    private fun saveImage(myBitmap: Bitmap): String {

        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
        val lyftedFolder = File(filesDir, ".NoWay'ste")
        // have the object build the directory structure, if needed.
        if (!lyftedFolder.exists()) {
            lyftedFolder.mkdirs()
        }

        try {
            val f = File(lyftedFolder, "profile.jpg")
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this, arrayOf(f.path), arrayOf("image/*"), null)
            fo.close()

            val op = UCrop.Options()
            op.setCircleDimmedLayer(true)

            UCrop.of(Uri.fromFile(f), Uri.parse(getProfilePath())).withAspectRatio(16f, 16f)
                    .withOptions(op).start(mContext as AppCompatActivity)

            return f.absolutePath
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {

            val perm = ArrayList<Boolean>()
            for (grantResult in grantResults) {
                perm.add(grantResult == PackageManager.PERMISSION_GRANTED)
            }
            if (!perm.contains(false)) {
                chooseProfileDialog()
            } else {
                val perm1 = ArrayList<Boolean>()
                for (j in listPermissionsNeeded.indices) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        perm1.add(shouldShowRequestPermissionRationale(listPermissionsNeeded[j]))
                    }
                }
                if (perm1.contains(true)) {
                    ShowToast.show(mContext, getStr(mContext, R.string.deny_permission))
                } else {
                    SnackBar.show(mContext, false, getStr(mContext, R.string.str_allow_storage), true, getStr(mContext, R.string.enable),
                            View.OnClickListener {

                            })
                }
            }
        }
    }


    private fun getEmail(): String {
        return tvEdiEmail.text.toString().trim()
    }


    private fun getName(): String {
        return tvEditName.text.toString().trim()
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        if (apiResponseManager.type == WebConstant.EDIT_PROFILE_API) {
            val response = apiResponseManager.response as AccountResponse

            when {
                response.status == 200 -> {

                    response.data?.let {
                        Prefs.setObject(context = mContext,
                                key = AppConstant.ACCOUNT_DATA,
                                value = response)

                        Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken!!))
                        Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())
                        showToast(response.message!!, mContext)

                        setResult(100, Intent())

                        finish()
                    }
                }
                else -> showToast(response.message!!, mContext)
            }
        } else if (apiResponseManager.type == 412) {
            val response = apiResponseManager.response as ApiStringError
            showToast(response.message!!, mContext)
        }
    }

}
