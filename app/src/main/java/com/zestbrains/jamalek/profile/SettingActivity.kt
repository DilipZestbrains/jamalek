package com.zestbrains.jamalek.profile


import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.activities.ChangePasswordActivity
import com.zestbrains.jamalek.activities.ForgotPasswordActivity
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : BaseActivity(), ApiResponseInterface {


    var mUserModel: AccountResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        setContentView(R.layout.activity_setting)


        if (Prefs.contain(mContext, AppConstant.AUTHORIZATION_TOKEN)) {
            mUserModel = Prefs.getObject(this@SettingActivity, AppConstant.ACCOUNT_DATA, "", AccountResponse::class.java) as AccountResponse
            switchNotification.isChecked = mUserModel!!.data!!.notification == 1
        }

        switchNotification.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                callAPI(1)
            } else {
                callAPI(0)
            }
            true
        }

        viewChangePassword.setSafeOnClickListener { start<ChangePasswordActivity>("Action" to "0", "USERID" to mUserModel!!.data!!.id.toString()) }
        iv_settings_back.setSafeOnClickListener { finish() }
        viewChangeMobileNumber.setSafeOnClickListener { start<ForgotPasswordActivity>("Action" to "MobileNo",
                "NUMBER" to mUserModel!!.data!!.mobile.toString(),
                "COUNTRYCODE" to mUserModel!!.data!!.countryCode.toString()) }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        if (apiResponseManager.type == WebConstant.NOTIFICATION) {
            val response = apiResponseManager.response as AccountResponse
            when {
                response.status == 200 -> {
                    response.data?.let {
                        Prefs.setObject(context = mContext, key = AppConstant.ACCOUNT_DATA, value = response)
                        Prefs.setValue(context = mContext, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken!!))
                        Prefs.setValue(mContext, AppConstant.USER_ID, response.data.id.toString())
                        showToast(response.message!!, mContext)
                    }
                }
                else -> showToast(response.message!!, mContext)
            }
        }
    }

    private fun callAPI(type: Int) {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().notificationOnoff(ws_lang = Prefs.getValue(this@SettingActivity, AppConstant.SELECT_LNG, "en")!!, authtoken = Prefs.getValue(this,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!, type = type),
                    TYPE = WebConstant.NOTIFICATION,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            showToast(resources.getString(R.string.str_network_error), this)
        }

    }
}
