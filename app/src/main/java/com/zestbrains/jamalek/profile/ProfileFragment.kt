package com.zestbrains.jamalek.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.LoginActivity
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.activities.WebActivity
import com.zestbrains.jamalek.address.SelectAddressActivity
import com.zestbrains.jamalek.model.login.AccountResponse
import com.zestbrains.jamalek.offers.OffersAndPromotionActivity
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.ADDRESS_ACTION
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment(), (View) -> Unit, ApiResponseInterface {

    private var mActivity: MainActivity? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container!!.inflate(R.layout.fragment_profile, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mActivity = this.activity as MainActivity
        loadData()
        click()

        if (Prefs.contain(mActivity!!, AppConstant.AUTHORIZATION_TOKEN)) {
            callAvailabilityAPI()
        }
    }

    private fun callAvailabilityAPI() {
        if (isNetworkAvailable(mActivity!!)) {
            Helper.hideKeyboard(mActivity!!)
            ApiRequest<Any>(
                    activity = mActivity!!,
                    objectType = ApiInitialize.initialize().getUserData(ws_lang = "en", authtoken = Prefs.getValue(mActivity!!,
                            AppConstant.AUTHORIZATION_TOKEN, "")!!),
                    TYPE = WebConstant.USER_RESPONSE,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(mActivity!!, true,
                    getStr(mActivity!!, R.string.str_network_error), false, "", null)
        }
    }

    fun click() {
        viewManAdd?.setSafeOnClickListener(this)
        viewOfferAndPromo?.setSafeOnClickListener(this)
        btn_profile_edit?.setSafeOnClickListener(this)
        viewSettings?.setSafeOnClickListener(this)
        viewAbout?.setSafeOnClickListener(this)
    }

    override fun invoke(p1: View) {

        when (p1) {
            viewManAdd -> {
                mActivity!!.start<SelectAddressActivity>(ADDRESS_ACTION to "Delete")
            }
            viewOfferAndPromo -> {
                val intent = Intent(mActivity!!, OffersAndPromotionActivity::class.java)
                        .putExtra(AppConstant.OFFER_SELECT, "View")
                        .putExtra(AppConstant.ID, "0")
                startActivityForResult(intent, 200)
            }
            btn_profile_edit -> {
                startActivityForResult(Intent(mActivity, EditProfileActivity::class.java), 100)
            }
            viewSettings -> {
                mActivity!!.start<SettingActivity>()
            }
            viewAbout -> {
                mActivity!!.start<WebActivity>("Name" to CommonFunctions.getStr(mActivity!!, R.string.menu_aboutus), "Url" to "about")
            }
        }

    }

    private fun loadData() {
        if (Prefs.contain(mActivity!!, AppConstant.AUTHORIZATION_TOKEN)) {
            tvName?.text = mActivity!!.mUserModel!!.data!!.name
            tvEmail?.text = mActivity!!.mUserModel!!.data!!.email
            tvUpcomingCount?.text = mActivity!!.mUserModel!!.data!!.upcoming_orders
            tvCompletedCount?.text = mActivity!!.mUserModel!!.data!!.completed_orders
            tvCancelCount?.text = mActivity!!.mUserModel!!.data!!.canceled_orders

            Glide.with(mActivity!!).load(mActivity!!.mUserModel!!.data!!.profileImage).placeholder(getRes(mActivity!!, R.drawable.img_placeholder))
                    .error(getRes(mActivity!!, R.drawable.img_placeholder)).into(image_user_profile)


        } else {
            showToast(getStr(mActivity!!, R.string.please_login_to_continue), mActivity!!)
            mActivity!!.finishAffinity()
            mActivity!!.start<LoginActivity>()
        }
        mActivity!!.navView!!.menu.findItem(R.id.navigation_dashboard).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_booking).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_notifications).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_profile).title = mActivity!!.resources.getString(R.string.title_profile)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> {
                try {
                    mActivity!!.mUserModel = Prefs.getObject(mActivity!!, AppConstant.ACCOUNT_DATA, "", AccountResponse::class.java) as AccountResponse
                    loadData()
                } catch (e: Exception) {

                }

                return
            }

        }
    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        if (apiResponseManager.type == WebConstant.USER_RESPONSE) {
            val response = apiResponseManager.response as AccountResponse

            when {
                response.status == 200 -> {
                    Prefs.setObject(context = mActivity!!, key = AppConstant.ACCOUNT_DATA, value = response)

                    response.data?.let {

                        Prefs.setValue(context = mActivity!!, key = AppConstant.AUTHORIZATION_TOKEN, value = "Bearer ".plus(response.data.vAuthToken))
                        Prefs.setValue(mActivity!!, AppConstant.USER_ID, response.data.id.toString())
                        tvUpcomingCount?.text = response.data.upcoming_orders
                        tvCompletedCount?.text = response.data.completed_orders
                        tvCancelCount?.text = response.data.canceled_orders

                        return
                    }
                }
                else -> showToast(response.message!!, mActivity!!)
            }
        }

    }


}