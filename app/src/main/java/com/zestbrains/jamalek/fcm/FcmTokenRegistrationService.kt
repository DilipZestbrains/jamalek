package com.zestbrains.jamalek.fcm

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.zestbrains.jamalek.utils.AppConstant.NOTIFICATION_TOKEN


class FcmTokenRegistrationService : IntentService("FcmTokenRegistrationService") {

    override fun onHandleIntent(intent: Intent?) {

        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {

                        return@OnCompleteListener
                    }
                     NOTIFICATION_TOKEN = task.result!!.token

                })

    }


}