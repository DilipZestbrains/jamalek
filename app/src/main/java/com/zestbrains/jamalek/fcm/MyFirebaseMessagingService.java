package com.zestbrains.jamalek.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zestbrains.jamalek.R;
import com.zestbrains.jamalek.activities.MainActivity;
import com.zestbrains.jamalek.booking.BookingDetailsActivity;
import com.zestbrains.jamalek.restapi.WebConstant;
import com.zestbrains.jamalek.utils.AppConstant;
import com.zestbrains.jamalek.utils.Helper;
import com.zestbrains.jamalek.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Objects;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("onMessageReceived", remoteMessage.getData().toString());


        String msg = "";
        String title = "";
        String pushType = remoteMessage.getData().get("push_type");
        String booking_id = remoteMessage.getData().get("booking_id");

        title = remoteMessage.getData().get("push_from");
        msg = remoteMessage.getData().get("push_message");

        if (remoteMessage.getData().size() > 0) {

            try {
                HashMap<String, String> dataMap = getDataHashMap(new JSONObject(remoteMessage.getData()));

                sendNotification(title, msg, Integer.parseInt(pushType), booking_id);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    private void sendNotification(String title, String notificationBody, int pushType, String BookingId) {

        NotificationCompat.Builder notificationBuilder;

        int mainInterval = 3;
        int interval = mainInterval * 2;
        long totalTime = 1000 * 10;
        Intent intent;


        if (pushType == WebConstant.PUSH_BOOKING && !BookingId.isEmpty() && !BookingId.equals("0")) {
            SessionManager sessionManager = new SessionManager(this);

            intent = new Intent(this, BookingDetailsActivity.class);
            intent.putExtra(AppConstant.CALL_FROM, "Push");
            intent.putExtra(AppConstant.ID, BookingId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        } else {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("type", WebConstant.PUSH_NOTIFICATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        int notifId = (int) System.currentTimeMillis();


        long[] vibrateInterval;

        String CHANNEL_ID = "my_channel_01";

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.logo);
            notificationBuilder.setColor(this.getResources().getColor(R.color.colorAccent));
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher));
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(notificationBody);
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setContentIntent(pendingIntent);

        defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        interval = 4;
        totalTime = 1000;

        vibrateInterval = new long[interval + 1];
        vibrateInterval[0] = totalTime / interval;
        for (int i = 1; i < interval + 1; i++) {
            vibrateInterval[i] = totalTime / interval;
        }
        notificationBuilder.setSound(defaultSoundUri);

        if (null != notificationManager) {
            notificationManager.notify(0, notificationBuilder.build());
        }


    }

    public HashMap<String, String> getDataHashMap(JSONObject jsonData) throws JSONException {
        HashMap<String, String> getData = new HashMap<>();
        Iterator<String> keysItr = jsonData.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            getData.put(key, jsonData.getString(key));
        }
        return getData;
    }


}