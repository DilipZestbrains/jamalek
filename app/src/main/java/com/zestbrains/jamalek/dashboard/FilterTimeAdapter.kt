package com.zestbrains.jamalek.dashboard

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.Helper.getFilterTime

class FilterTimeAdapter(private val mActivity: Activity, private var mTime: String) : RecyclerView.Adapter<FilterTimeAdapter.MyViewHolder>() {

    var isOpenPos: Int = -100

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvTime: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvTime)
        var rvMain: RelativeLayout = itemView.findViewById(R.id.cv_main)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(parent.inflate(R.layout.rv_filter_time))//
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {


        if (mTime == getFilterTime(listPosition)) {
            isOpenPos = listPosition
            mTime=""
        }

        if (isOpenPos == listPosition) {
            holder.rvMain.background = getRes(mActivity, R.drawable.bg_time_border)
        } else {
            holder.rvMain.background = getRes(mActivity, R.drawable.bg_time_unselected_border)
        }

        holder.rvMain.setSafeOnClickListener {
            Prefs.setValue(mActivity, AppConstant.FILTER_TIME, "")
            isOpenPos = if (isOpenPos == listPosition) {
                -100
            } else {
                listPosition
            }
            notifyDataSetChanged()
        }
        holder.tvTime.text = getFilterTime(listPosition)


    }

    override fun getItemCount(): Int {
        return 12
    }

    fun setSelected(mString: String) {
        if (mString.isNotEmpty()) {
            for (i in 0 until 12) {
                if (getFilterTime(i) == mString) {
                    isOpenPos == i
                    notifyDataSetChanged()
                    break
                }
            }
        }
    }


    fun getSelectedTime(): String {
        return if (isOpenPos != -100)
            getFilterTime(isOpenPos)
        else
            ""
    }
}