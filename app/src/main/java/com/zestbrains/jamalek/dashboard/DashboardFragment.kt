package com.zestbrains.jamalek.dashboard


import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnSalonItemSelectListener
import com.zestbrains.jamalek.Interfaces.PermissionStatus
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.LoginActivity
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.activities.WebActivity
import com.zestbrains.jamalek.fragments.MapsFragment
import com.zestbrains.jamalek.model.commom.CommonResponse
import com.zestbrains.jamalek.model.login.FilterModelData
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.permission.PermissionChecker
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.salons.ProviderDetailsActivity
import com.zestbrains.jamalek.search.SearchActivity
import com.zestbrains.jamalek.utils.*
import com.zestbrains.jamalek.utils.AppConstant.SALON_DATA
import com.zestbrains.jamalek.utils.CommonFunctions.getStr
import com.zestbrains.jamalek.viewModels.SalonsInTheaterViewModel
import kotlinx.android.synthetic.main.app_bar_deshboard.*
import kotlinx.android.synthetic.main.content_deshboard.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.nav_header_home.*


class DashboardFragment : Fragment(), OnSalonItemSelectListener, PermissionStatus, ApiResponseInterface,
        AdapterView.OnItemSelectedListener, (View) -> Unit, SwipeRefreshLayout.OnRefreshListener {

    override fun onRefresh() {
        swipe_container?.isRefreshing = true
        mSalonsViewModel!!.refresh()

    }

    override fun onSalonLoad() {}

    private var mActivity: MainActivity? = null
    private var IsMapView: Boolean = false
    private var mSalonsViewModel: SalonsInTheaterViewModel? = null
    private var mProgressDialog: ProgressDialog? = null
    private var isfirstTime: Boolean = false
    private lateinit var fragmentTransaction: FragmentTransaction
    val fragment2 = MapsFragment()

    companion object {
        lateinit var mSalonAdapter: SalonsAdapter
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return try {
            container!!.inflate(R.layout.fragment_dashboard, false, inflater.context)
        } catch (e: Exception) {
            e.printStackTrace()
            inflater.inflate(R.layout.fragment_dashboard, container, false)
        }
    }

    @SuppressLint("RtlHardcoded", "WrongConstant")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mActivity = this.activity as MainActivity

        initialise()
        checkForPermission()


    }

    private fun initialise() {
        mActivity!!.navView!!.menu.findItem(R.id.navigation_dashboard).title = mActivity!!.resources.getString(R.string.title_dashboard)
        mActivity!!.navView!!.menu.findItem(R.id.navigation_booking).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_notifications).title = ""
        mActivity!!.navView!!.menu.findItem(R.id.navigation_profile).title = ""

        if (Prefs.contain(mActivity!!, AppConstant.AUTHORIZATION_TOKEN)) {
            btnLogout?.visible()
        } else {
            btnLogout?.gone()
        }
        if (Prefs.contain(mActivity!!, AppConstant.AUTHORIZATION_TOKEN)) {
            Glide.with(mActivity!!).load(mActivity!!.mUserModel!!.data!!.profileImage)
                    .placeholder(resources.getDrawable(R.drawable.img_placeholder)).error(resources.getDrawable(R.drawable.img_placeholder)).into(imageView)
        }
        if (Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en") == "en") {
            spLanguage?.setSelection(0)
        } else {
            spLanguage?.setSelection(1)
        }
        spLanguage?.onItemSelectedListener = this


        mSalonAdapter = SalonsAdapter(this, mActivity!!)

        val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        home_item_list?.layoutManager = linearLayoutManager
        home_item_list?.adapter = mSalonAdapter


        swipe_container?.setOnRefreshListener(this)
        swipe_container?.setColorSchemeResources(R.color.colorPrimary, android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark, android.R.color.holo_blue_dark)
        clicks()
    }

    private fun clicks() {
        btnSearch?.setSafeOnClickListener(this)
        btnTermsUse?.setSafeOnClickListener(this)
        btnHelp?.setSafeOnClickListener(this)
        btnAboutUs?.setSafeOnClickListener(this)
        btnProfile?.setSafeOnClickListener(this)
        btnLogout?.setSafeOnClickListener(this)
        btnContactUs?.setSafeOnClickListener(this)
        btnList?.setSafeOnClickListener(this)
        btnFilter?.setSafeOnClickListener(this)
        btnMenu?.setSafeOnClickListener(this)
        btnRegisterWeb?.setSafeOnClickListener(this)

        fragmentTransaction = fragmentManager!!.beginTransaction()
    }


    @SuppressLint("WrongConstant")
    override fun invoke(p1: View) {
        when (p1) {
            btnSearch -> {
                mActivity!!.start<SearchActivity>()
            }
            btnTermsUse -> {
                mActivity!!.start<WebActivity>("Name" to getStr(mActivity!!, R.string.menu_termsanduse), "Url" to "term")
            }
            btnHelp -> {
                mActivity!!.start<WebActivity>("Name" to getStr(mActivity!!, R.string.menu_help), "Url" to "help")
            }
            btnAboutUs -> {
                mActivity!!.start<WebActivity>("Name" to getStr(mActivity!!, R.string.menu_aboutus), "Url" to "about")
            }
            btnLogout -> {
                onClickLogout()
            }
            btnList -> {
                initMap()
            }
            btnFilter -> {
                val intent = Intent(mActivity!!, FiltersActivity::class.java).putExtra(AppConstant.ID, "0")
                startActivity(intent)

            }
            btnMenu -> {
                mDrawerLayout.openDrawer(Gravity.START)
            }
            btnProfile -> {
                mActivity!!.navView!!.selectedItemId = mActivity!!.navView!!.menu.findItem(R.id.navigation_profile).itemId
            }
            btnContactUs -> {
                sendEmail("info@jamalek.com", getStr(mActivity!!, R.string.menu_contactus), "")
            }
            btnRegisterWeb -> {
                mActivity!!.start<WebActivity>("Name" to getStr(mActivity!!, R.string.register_as_service_provider), "Url" to "about")
            }
        }
    }


    override fun onResume() {
        super.onResume()
        mDrawerLayout?.closeDrawer(Gravity.LEFT)
    }

    private fun initMap() {
        try {
            if (IsMapView) {
                btnList?.setImageResource(R.drawable.ic_location)
                IsMapView = false
                fragmentManager!!.popBackStack()
                fragmentTransaction = fragmentManager!!.beginTransaction()

            } else {
                btnList?.setImageResource(R.drawable.ic_list)
                IsMapView = true
                fragmentTransaction.add(R.id.nav_host_fragment, fragment2, "MapsFragment")
                fragmentTransaction.isAddToBackStackAllowed
                fragmentTransaction.addToBackStack("MapsFragment")
                fragmentTransaction.commit()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun checkForPermission() {
        PermissionChecker(mActivity!!, this@DashboardFragment,
                Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    override fun allGranted() {
        loadHomeData()
    }

    override fun onDenied() {
        ShowToast.show(mActivity!!, getStr(mActivity!!, R.string.deny_permission))

    }

    override fun foreverDenied() {
        SnackBar.show(mActivity, getStr(mActivity!!, R.string.enable_location_permission), getStr(mActivity!!, R.string.enable), View.OnClickListener {

        })
    }

    private fun onClickLogout() {
        if (isNetworkAvailable(mActivity!!)) {
            AlertDialog.Builder(mActivity!!).setMessage(getStr(mActivity!!, R.string.str_confirm_logout))
                    .setPositiveButton(getStr(mActivity!!, R.string.menu_logout)) { dialogInterface, i ->
                        dialogInterface.dismiss()

                        ApiRequest<Any>(mActivity!!,
                                objectType = ApiInitialize.initialize().logout(
                                        Prefs.getValue(context = mActivity!!, key = AppConstant.AUTHORIZATION_TOKEN, defaultValue = "")!!),
                                TYPE = WebConstant.LOGOUT_API,
                                isShowProgressDialog = true,
                                apiResponseInterface = this@DashboardFragment)
                    }
                    .setNegativeButton(getStr(mActivity!!, R.string.cancel)) { dialogInterface, i -> dialogInterface.dismiss() }
                    .create().show()
        }
        else {
            SnackBar.show(mActivity, true, com.zestbrains.jamalek.utils.getStr(mActivity!!, R.string.str_network_error), false, "", null)
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (isfirstTime) {
            if (position == 0) {
                mActivity!!.Selected_Language = "en"
            } else if (position == 1) {
                mActivity!!.Selected_Language = "ar"
            }
            if (Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en") != mActivity!!.Selected_Language) {
                Prefs.setValue(mActivity!!, AppConstant.SELECT_LNG, mActivity!!.Selected_Language)
                mActivity!!.start<MainActivity>()
            }
        }
        isfirstTime = true
    }

    override fun onSalonItemSelectListener(pos: Int, mView: View, mModel: Data?) {

        val detailIntent = Intent(mActivity, ProviderDetailsActivity::class.java)
        detailIntent.putExtra(SALON_DATA, mModel)
        startActivity(detailIntent)

    }

    private fun showProgress() {
        mProgressDialog = getProgressDialog(mActivity!!)
    }

    private fun dismissProgress() {

        swipe_container?.isRefreshing = false
        dismissDialog1(mActivity!!, mProgressDialog!!)

    }

    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        if (apiResponseManager.type == WebConstant.LOGOUT_API) {
            val response = apiResponseManager.response as CommonResponse

            when {
                response.status == 200 -> {
                    Prefs.removeValue(context = mActivity!!, key = AppConstant.ACCOUNT_DATA)
                    Prefs.removeValue(context = mActivity!!, key = AppConstant.AUTHORIZATION_TOKEN)
                    Prefs.removeValue(context = mActivity!!, key = AppConstant.USER_ID)

                    mActivity!!.start<LoginActivity>()
                    mActivity!!.finishAffinity()
                }
                else -> showToast(response.message!!, mActivity!!)
            }
        }
    }

    private fun sendEmail(recipient: String, subject: String, message: String) {

        val mIntent = Intent(Intent.ACTION_SENDTO)
        mIntent.data = Uri.parse("mailto:")
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        } catch (e: Exception) {
            Toast.makeText(mActivity!!, e.message, Toast.LENGTH_LONG).show()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            300 -> {
                val b = data!!.extras
                if (b != null) {
                    if (b.containsKey("Action"))
                        if (b.get("Action").toString() == "APPLY") {
                            loadHomeData()
                        }
                    return

                }

            }
        }
    }

    private fun loadHomeData() {
        if (isNetworkAvailable(mActivity!!)) {
            val businessListModelList = FilterModelData()
            businessListModelList.Vauthtoken = Prefs.getValue(mActivity!!, AppConstant.AUTHORIZATION_TOKEN, "")

            businessListModelList.ws_lang = Prefs.getValue(mActivity!!, AppConstant.SELECT_LNG, "en")

            businessListModelList.time_zone = Prefs.getValue(mActivity!!, AppConstant.TIME_ZONE, "")
            businessListModelList.for_men = Prefs.getValue(mActivity!!, AppConstant.FILTER_MAN, 0).toString()
            businessListModelList.for_women = Prefs.getValue(mActivity!!, AppConstant.FILTER_WOMAN, 0).toString()
            businessListModelList.for_kids = Prefs.getValue(mActivity!!, AppConstant.FILTER_KIDS, 0).toString()
            businessListModelList.in_house = Prefs.getValue(mActivity!!, AppConstant.FILTER_HOUSE, 0).toString()
            businessListModelList.salon = Prefs.getValue(mActivity!!, AppConstant.FILTER_STORE, 0).toString()
            businessListModelList.stylist = Prefs.getValue(mActivity!!, AppConstant.FILTER_STYLIST, 0).toString()
            businessListModelList.distance = Prefs.getValue(mActivity!!, AppConstant.FILTER_DISTANCE, "0")
            businessListModelList.category_ids = Prefs.getValue(mActivity!!, AppConstant.SALON_CATEGORY, "")
            businessListModelList.start_time = Prefs.getValue(mActivity!!, AppConstant.FILTER_TIME, "")

            businessListModelList.lat = GPSTracker(mActivity!!).latitude.toString()
            businessListModelList.lng = GPSTracker(mActivity!!).longitude.toString()


            SalonsInTheaterViewModel.mData = businessListModelList


            showProgress()

            mSalonsViewModel = ViewModelProviders.of(this).get(SalonsInTheaterViewModel::class.java)

            mSalonsViewModel!!.moviesInTheaterList.observe(this, Observer<PagedList<Data>> { movies ->
                mSalonAdapter.submitList(movies)
            })
            mSalonsViewModel!!.networkStateLiveData.observe(this, Observer<NetworkState> { networkState ->
                mSalonAdapter.setNetworkState(networkState)
                if (networkState.msg != "Running") {
                    dismissProgress()
                    mEmptyViewFeatured?.setVisible((networkState.msg == "NODATA"))
                    if (networkState.msg != "Success" && networkState.msg != "NODATA") {
                        showToast(networkState.msg, mActivity!!)
                    }
                }
            })
        } else {
            SnackBar.show(mActivity, true, com.zestbrains.jamalek.utils.getStr(mActivity!!, R.string.str_network_error), false, "", null)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dismissProgress()
    }

}