package com.zestbrains.jamalek.dashboard


import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.res.ResourcesCompat.getDrawable
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.Interfaces.OnSalonItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.salons.ServicesListAdapter
import com.zestbrains.jamalek.utils.Helper
import com.zestbrains.jamalek.utils.getStr
import com.zestbrains.jamalek.utils.gone
import com.zestbrains.jamalek.utils.visible
import java.text.DecimalFormat
import java.util.*

class HomeItemListAdapter(private val onClick: OnSalonItemSelectListener, private val mActivity: Activity) :
        RecyclerView.Adapter<HomeItemListAdapter.MyViewHolder>() {

    private val mModel = ArrayList<Data>()



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_item_shop_main_name: androidx.appcompat.widget.AppCompatTextView? = null
        var tv_start_review: androidx.appcompat.widget.AppCompatTextView? = null
        var tv_location: androidx.appcompat.widget.AppCompatTextView? = null
        var tv_open_time: androidx.appcompat.widget.AppCompatTextView? = null
        var iv_men_icon: androidx.appcompat.widget.AppCompatImageView? = null
        var iv_female_icon: androidx.appcompat.widget.AppCompatImageView? = null
        var iv_kid_icon: androidx.appcompat.widget.AppCompatImageView? = null
        var iv_salon_icon: androidx.appcompat.widget.AppCompatImageView? = null
        var iv_home_icon: androidx.appcompat.widget.AppCompatImageView? = null
        var btnshow_item_available: ImageView? = null


        var cv_root: RelativeLayout? = null
        var rvServices: RecyclerView

        init {
            this.tv_item_shop_main_name = itemView.findViewById(R.id.tv_item_shop_main_name)
            this.tv_start_review = itemView.findViewById(R.id.tv_start_review)
            this.tv_location = itemView.findViewById(R.id.tv_location)
            this.tv_open_time = itemView.findViewById(R.id.tv_open_time)
            this.iv_men_icon = itemView.findViewById(R.id.iv_men_icon)
            this.iv_female_icon = itemView.findViewById(R.id.iv_female_icon)
            this.iv_kid_icon = itemView.findViewById(R.id.iv_kid_icon)
            this.iv_salon_icon = itemView.findViewById(R.id.iv_salon_icon)
            this.iv_home_icon = itemView.findViewById(R.id.iv_home_icon)
            this.cv_root = itemView.findViewById(R.id.cv_root)

            this.rvServices = itemView.findViewById(R.id.rvServices)
            this.btnshow_item_available = itemView.findViewById(R.id.btnshow_item_available)


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.rv_oder, parent, false)

        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {

        if (mModel[listPosition].vendorCategory!!.isNotEmpty()) {

            val myServicesAdapter = ServicesListAdapter(mModel[listPosition].vendorCategory!!,  mActivity)
            val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
            holder.rvServices.layoutManager = linearLayoutManager
            holder.rvServices.adapter = myServicesAdapter
            holder.rvServices.visible()
        } else {
            holder.rvServices.gone()
        }

        holder.cv_root!!.setOnClickListener { onClick.onSalonItemSelectListener(0, holder.btnshow_item_available, mModel[listPosition]) }

        holder.tv_item_shop_main_name!!.text = mModel[listPosition].name
        holder.tv_location!!.text = mModel[listPosition].address + " - " + DecimalFormat("##.##").format(mModel[listPosition].distance!!.toDouble()) + getStr(mActivity, R.string.km)
        holder.tv_start_review!!.text = mModel[listPosition].ratings + " (" + mModel[listPosition].review + " " + getStr(mActivity, R.string.reviews) + ")"

        if (mModel[listPosition].vendorTiming!!.isNotEmpty())
            holder.tv_open_time!!.text = getStr(mActivity, R.string.open) + " (" + Helper.getTimeWithAMPM(mModel[listPosition].vendorTiming!![0].startTime!!) + " to " + Helper.getTimeWithAMPM(mModel[listPosition].vendorTiming!![0].endTime!!) + ")"

        if (mModel[listPosition].forKids == "0")
            holder.iv_kid_icon!!.gone()
        else
            holder.iv_kid_icon!!.visible()

        if (mModel[listPosition].forMen == "0")
            holder.iv_men_icon!!.gone()
        else
            holder.iv_men_icon!!.visible()


        if (mModel[listPosition].forWomen == "0")
            holder.iv_female_icon!!.gone()
        else
            holder.iv_female_icon!!.visible()

        if (mModel[listPosition].inHouse == "0")
            holder.iv_home_icon!!.gone()
        else
            holder.iv_home_icon!!.visible()


        if (mModel[listPosition].salon == "0")
            holder.iv_salon_icon!!.gone()
        else
            holder.iv_salon_icon!!.visible()


        Glide.with(mActivity).load(mModel[listPosition].logo)
                .placeholder(getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                .error(getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(holder.btnshow_item_available!!)

    }


    fun addData(mData: ArrayList<Data>) {
        mModel.addAll(mData)
        notifyItemInserted(mModel.size - 1)
        notifyDataSetChanged()
    }

    fun clear() {
        if (mModel.size > 0) {
            mModel.clear()
            notifyDataSetChanged()
        }
    }


    override fun getItemCount(): Int {
        return mModel.size
    }

}