package com.zestbrains.jamalek.dashboard


import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.zestbrains.jamalek.Interfaces.OnSalonItemSelectListener
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.Data
import com.zestbrains.jamalek.restapi.NetworkState
import com.zestbrains.jamalek.salons.ServicesListAdapter
import com.zestbrains.jamalek.utils.*
import java.text.DecimalFormat


class SalonsAdapter(private val onClick: OnSalonItemSelectListener, private val mActivity: Activity) :
        PagedListAdapter<Data, RecyclerView.ViewHolder>(Data.DIFF_CALL) {


    private var mNetworkState: NetworkState? = null

    private val isLoadingData: Boolean
        get() = mNetworkState != null && mNetworkState !== NetworkState.LOADED


    override fun getItemViewType(position: Int): Int {
        return if (isLoadingData && position == itemCount - 1) LOAD_ITEM_VIEW_TYPE else MOVIE_ITEM_VIEW_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View
        if (viewType == MOVIE_ITEM_VIEW_TYPE) {
            view = inflater.inflate(R.layout.rv_oder, parent, false)
            return MovieViewHolder(view)
        } else {
            view = inflater.inflate(R.layout.load_progress_item, parent, false)
            return ProgressViewHolder(view)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            val movie = getItem(position)
            holder.bind(movie!!, mActivity, position, onClick)

            if (position == 0)
                onClick.onSalonLoad()

        }
    }

    fun setNetworkState(networkState: NetworkState) {
        val prevState = networkState
        val wasLoading = isLoadingData
        mNetworkState = networkState
        val willLoad = isLoadingData
        if (wasLoading != willLoad) {
            if (wasLoading) notifyItemRemoved(itemCount) else notifyItemInserted(itemCount)
        }
    }

    private class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_item_shop_main_name: AppCompatTextView? = null
        var tv_start_review: AppCompatTextView? = null
        var tv_location: AppCompatTextView? = null
        var tv_open_time: AppCompatTextView? = null
        var iv_men_icon: AppCompatImageView? = null
        var iv_female_icon: AppCompatImageView? = null
        var iv_kid_icon: AppCompatImageView? = null
        var iv_salon_icon: AppCompatImageView? = null
        var iv_home_icon: AppCompatImageView? = null
        var btnshow_item_available: ImageView? = null


        var cv_root: RelativeLayout? = null
        var rvServices: RecyclerView

        init {
            this.tv_item_shop_main_name = itemView.findViewById(R.id.tv_item_shop_main_name)
            this.tv_start_review = itemView.findViewById(R.id.tv_start_review)
            this.tv_location = itemView.findViewById(R.id.tv_location)
            this.tv_open_time = itemView.findViewById(R.id.tv_open_time)
            this.iv_men_icon = itemView.findViewById(R.id.iv_men_icon)
            this.iv_female_icon = itemView.findViewById(R.id.iv_female_icon)
            this.iv_kid_icon = itemView.findViewById(R.id.iv_kid_icon)
            this.iv_salon_icon = itemView.findViewById(R.id.iv_salon_icon)
            this.iv_home_icon = itemView.findViewById(R.id.iv_home_icon)
            this.cv_root = itemView.findViewById(R.id.cv_root)

            this.rvServices = itemView.findViewById(R.id.rvServices)
            this.btnshow_item_available = itemView.findViewById(R.id.btnshow_item_available)


        }

        fun bind(mModel: Data, mActivity: Activity, position: Int, onClick: OnSalonItemSelectListener) {


            if (mModel.vendorCategory!!.isNotEmpty()) {
                val myServicesAdapter = ServicesListAdapter(mModel.vendorCategory!!,  mActivity)
                val linearLayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)
                rvServices.layoutManager = linearLayoutManager
                rvServices.adapter = myServicesAdapter
                rvServices.visible()
            } else {
                rvServices.gone()
            }


            cv_root!!.setSafeOnClickListener { onClick.onSalonItemSelectListener(0, btnshow_item_available, mModel) }
            tv_item_shop_main_name!!.text = mModel.name
            tv_location!!.text = mModel.address + " -" + DecimalFormat("##.##").format(mModel.distance!!.toDouble()) + getStr(mActivity, R.string.km)
            tv_start_review!!.text = mModel.ratings + " (" + mModel.review + " " + getStr(mActivity, R.string.reviews) + ")"

            if (mModel.vendorTiming!!.isNotEmpty())
                tv_open_time!!.text = getStr(mActivity, R.string.open) + " (" + Helper.getTimeWithAMPM(mModel.vendorTiming!![0].startTime!!) + " - " + Helper.getTimeWithAMPM(mModel.vendorTiming!![0].endTime!!) + ")"


            iv_kid_icon!!.setVisible((mModel.forKids != "0"))
            iv_men_icon!!.setVisible((mModel.forMen != "0"))
            iv_female_icon!!.setVisible((mModel.forWomen != "0"))
            iv_home_icon!!.setVisible((mModel.inHouse != "0"))
            iv_salon_icon!!.setVisible((mModel.salon != "0"))

            Glide.with(mActivity).load(mModel.logo)
                    .placeholder(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null))
                    .error(ResourcesCompat.getDrawable(mActivity.resources, R.drawable.img_place_holder, null)).into(btnshow_item_available!!)

        }
    }

    private class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    companion object {
        private val TAG = "SalonsAdapter"
        val MOVIE_ITEM_VIEW_TYPE = 1
        val LOAD_ITEM_VIEW_TYPE = 0
    }

    fun getITEMs(pon: Int): Data {
        return this.getItem(pon)!!
    }

}
