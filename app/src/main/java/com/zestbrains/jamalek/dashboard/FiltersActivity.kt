package com.zestbrains.jamalek.dashboard


import android.annotation.SuppressLint
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.xw.repo.BubbleSeekBar
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.activities.BaseActivity
import com.zestbrains.jamalek.activities.MainActivity
import com.zestbrains.jamalek.address.NewAddressActivity
import com.zestbrains.jamalek.common.AutoFitGridLayoutManager
import com.zestbrains.jamalek.model.salon.CategoriesResponse
import com.zestbrains.jamalek.restapi.*
import com.zestbrains.jamalek.utils.*
import kotlinx.android.synthetic.main.activity_filters.*
import java.util.*


class FiltersActivity : BaseActivity(), ApiResponseInterface, (View) -> Unit {

    var mDistance: Int = 0
    var mTIME: String = ""
    var mCategoryId: String = ""
    var mCategoryAdapter: FilterServicesAdapter? = null
    var mTimeAdapter: FilterTimeAdapter? = null
    var mMan = 0
    var mWoman = 0
    var mKid = 0
    var mStore = 0
    var mHouse = 0
    var mStylist = 0
    private var myLocation: LatLng? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)

        services()
        callAvailabilityAPI()
        setCurrenLocation()
    }

    private fun setCurrenLocation() {
        myLocation = LatLng(GPSTracker(this@FiltersActivity).latitude, GPSTracker(this@FiltersActivity).longitude)
        setAddress()
        Prefs.setValue(this@FiltersActivity, AppConstant.LAT, myLocation!!.latitude.toString())
        Prefs.setValue(this@FiltersActivity, AppConstant.LONG, myLocation!!.longitude.toString())
    }


    private fun callAvailabilityAPI() {
        if (isNetworkAvailable(this)) {
            Helper.hideKeyboard(this)
            ApiRequest<Any>(
                    activity = this,
                    objectType = ApiInitialize.initialize().getCategories(),
                    TYPE = WebConstant.GET_ALL_CVATEGORY,
                    isShowProgressDialog = true,
                    apiResponseInterface = this)
        } else {
            SnackBar.show(this@FiltersActivity, true,
                    getStr(this@FiltersActivity, R.string.str_network_error), false, "", null)
        }
    }

    private fun services() {


        autoCompleteEditText.text = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_ADDRESS, "").toString()

        mTIME = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_TIME, "").toString()
        mCategoryId = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_CATEGORY, "").toString()

        mCategoryAdapter = FilterServicesAdapter(this, mCategoryId)
        val linearLayoutManager1 = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvServicesList.layoutManager = linearLayoutManager1
        rcvServicesList.adapter = mCategoryAdapter
        val mGridLayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rcvStartTimeList.layoutManager = mGridLayoutManager
        rcvStartTimeList.layoutManager = GridLayoutManager(this, 3)
        rcvStartTimeList.addItemDecoration(AutoFitGridLayoutManager(4))
        mTimeAdapter = FilterTimeAdapter(this, mTIME)
        rcvStartTimeList.adapter = mTimeAdapter


        mMan = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_MAN, 0)
        if (mMan == 1) {
            tvMan.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            tvMan.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        mKid = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_KIDS, 0)
        if (mKid == 1) {
            tvKids.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            tvKids.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        mWoman = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_WOMAN, 0)
        if (mWoman == 1) {
            tvWoman.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            tvWoman.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        mStore = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_STORE, 0)
        if (mStore == 1) {

            iv_Store_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_store_selected))
            tvStore.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
        } else {

            iv_Store_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_store_unselected))
            tvStore.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        mHouse = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_HOUSE, 0)

        if (mHouse == 1) {
            iv_in_house_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_house_selected))
            tvInHouse.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
        } else {
            iv_in_house_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_house_unselected))
            tvInHouse.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        mStylist = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_STYLIST, 0)
        if (mStylist == 1) {
            iv_Stylist_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_stylist_select))
            tvStylist.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
        } else {
            iv_Stylist_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_stylist))
            tvStylist.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }

        if (Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_DISTANCE, "")!!.isNotEmpty()) {

            mDistance = Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_DISTANCE, "0")!!.toInt()


            val layoutParams = clOne.layoutParams as ConstraintLayout.LayoutParams
            var bias = (mDistance.toFloat() / 100f) - 0.01f

            if (bias < 0.5) bias += 0.02f
            if (bias < 0f) bias = 0f


            layoutParams.horizontalBias = bias
            clOne.layoutParams = layoutParams

            customSeekBar.setProgress(mDistance.toFloat())
            clOne.text = mDistance.toString() + CommonFunctions.getStr(this@FiltersActivity, R.string.km)
        }
        else {
            customSeekBar.setProgress(0f)
            clOne.text = "0" + CommonFunctions.getStr(this@FiltersActivity, R.string.km)
        }

        setClick()
        customSeekBar.onProgressChangedListener = object : BubbleSeekBar.OnProgressChangedListenerAdapter() {

            override fun onProgressChanged(bubbleSeekBar: BubbleSeekBar, progress: Int, progressFloat: Float, fromUser: Boolean) {
                val layoutParams = clOne.layoutParams as ConstraintLayout.LayoutParams
                var bias = (progress.toFloat() / 100f) - 0.01f

                if (bias < 0.5) bias += 0.02f
                if (bias < 0f) bias = 0f


                layoutParams.horizontalBias = bias
                clOne.layoutParams = layoutParams

                mDistance = progress
                clOne.text = progress.toString() + CommonFunctions.getStr(this@FiltersActivity, R.string.km)
            }

            override fun getProgressOnActionUp(bubbleSeekBar: BubbleSeekBar, progress: Int, progressFloat: Float) {
                mDistance = progress
                clOne.text = progress.toString() + CommonFunctions.getStr(this@FiltersActivity, R.string.km)
            }

            override fun getProgressOnFinally(bubbleSeekBar: BubbleSeekBar, progress: Int, progressFloat: Float, fromUser: Boolean) {
                mDistance = progress
                clOne.text = progress.toString() + CommonFunctions.getStr(this@FiltersActivity, R.string.km)
            }
        }
    }

    private fun setClick() {

        viewMan.setSafeOnClickListener(this)
        viewWomen.setSafeOnClickListener(this)
        viewKids.setSafeOnClickListener(this)
        viewSalon.setSafeOnClickListener(this)
        viewInHouse.setSafeOnClickListener(this)
        viewStylista.setSafeOnClickListener(this)
        iv_filter_back.setSafeOnClickListener(this)
        btnApplyFilter.setSafeOnClickListener(this)
        tvClearFilter.setSafeOnClickListener(this)
        iv_home_f_filter.setSafeOnClickListener(this)
        autoCompleteEditText.setSafeOnClickListener(this)
    }


    override fun invoke(p1: View) {
        when (p1) {
            viewMan -> {
                forManClick()
            }
            viewWomen -> {
                forWomanClick()
            }
            viewKids -> {
                forKidsClick()
            }
            viewSalon -> {
                forSalonClick()
            }
            viewInHouse -> {
                forInHouseClick()
            }
            viewStylista -> {
                forStylistClick()
            }
            iv_filter_back -> {
                onBack()
            }
            btnApplyFilter -> {

                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_DISTANCE, mDistance.toString())
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_MAN, mMan)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_WOMAN, mWoman)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_KIDS, mKid)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_STORE, mStore)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_HOUSE, mHouse)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_STYLIST, mStylist)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_TIME, mTimeAdapter?.getSelectedTime().toString())
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_CATEGORY, mCategoryAdapter?.getCategoryId().toString())

                finishAffinity()
                start<MainActivity>()

            }
            tvClearFilter -> {

                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_DISTANCE, "0")
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_MAN, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_WOMAN, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_KIDS, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_STORE, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_HOUSE, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_STYLIST, 0)
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_TIME, "")
                Prefs.setValue(this@FiltersActivity, AppConstant.FILTER_CATEGORY, "")
                finishAffinity()
                start<MainActivity>()
            }

            iv_home_f_filter -> {
                setCurrenLocation()
            }
            autoCompleteEditText -> {
                val intent = Intent(this, NewAddressActivity::class.java).putExtra("Action", "Filter")
                startActivityForResult(intent, 100)
            }
//            placesAutocomplete -> {
////  https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Amoeba&types=establishment&location=37.76999,-122.44696&radius=500&key=AIzaSyD8ZMJinNAMuuBiueve6x07waTndPj5xlM
//                val fields = arrayOf(Place.Field.LAT_LNG, Place.Field.ID, Place.Field.NAME).toMutableList()
//
//                val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN,fields)
//                        .build(this)
//                startActivityForResult(intent, 11)
//            }
        }
    }


    override fun getApiResponse(apiResponseManager: ApiResponseManager<*>) {
        when (apiResponseManager.type) {
            WebConstant.GET_ALL_CVATEGORY -> {
                val response = apiResponseManager.response as CategoriesResponse
                when (response.status) {
                    200 -> {
                        response.data?.let { mCategoryAdapter?.addAll(it) }

                        if (Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_CATEGORY, "")!!.isNotEmpty()) {
                            mCategoryAdapter?.setSelected(Prefs.getValue(this@FiltersActivity, AppConstant.FILTER_CATEGORY, "").toString())
                        }

                    }
                    else -> showToast(response.message!!, this)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Helper.hideKeyboard(this@FiltersActivity)
        when (requestCode) {
            100 -> {
                if (data != null) {
                    val b = data.extras
                    if (b != null || b!!.containsKey(AppConstant.ADDRESS_DATA)) {


                        val address = b.getString(AppConstant.ADDRESS_DATA)
                        val lat = b.getString(AppConstant.LAT)
                        val lang = b.getString(AppConstant.LONG)

                        autoCompleteEditText.setText(address.toString())
                        Prefs.setValue(mContext, AppConstant.FILTER_ADDRESS, address.toString())
                        Prefs.setValue(mContext, AppConstant.LAT, lat.toString())
                        Prefs.setValue(mContext, AppConstant.LONG, lang.toString())
                    }
                }

                return
            }
        }
    }

    private fun hideSystemUI() {
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_IMMERSIVE
                )


    }

    private fun forManClick() {

        if (mMan == 0)
        {
            mMan = 1
            tvMan.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            mMan = 0
            tvMan.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }
    }

    private fun forWomanClick() {

        if (mWoman == 0) {
            mWoman = 1
            tvWoman.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            mWoman = 0
            tvWoman.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }
    }

    private fun forKidsClick() {

        if (mKid == 0)
        {
            mKid = 1
            tvKids.setTextColor(getClr(this@FiltersActivity, R.color.selectedTabColor))
        } else {
            mKid = 0
            tvKids.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }
    }

    private fun forSalonClick() {

        if (mStore == 0)
        {
            mStore = 1
            iv_Store_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_store_selected))
            tvStore.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
        } else {
            mStore = 0
            iv_Store_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_store_unselected))
            tvStore.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }
    }

    private fun forInHouseClick() {

        if (mHouse == 0)
        {
            mHouse = 1
            iv_in_house_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_house_selected))
            tvInHouse.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
        } else {
            mHouse = 0
            iv_in_house_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_filter_house_unselected))
            tvInHouse.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
        }
    }

    private fun forStylistClick() {

        if (mStylist == 0)
        {
            iv_Stylist_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_stylist_select))
            tvStylist.setTextColor(getClr(this@FiltersActivity, R.color.colorStrat))
            mStylist = 1
        } else {
            iv_Stylist_icon.setImageDrawable(getRes(this@FiltersActivity, R.drawable.ic_stylist))
            tvStylist.setTextColor(getClr(this@FiltersActivity, R.color.textcolor))
            mStylist = 0
        }
    }

    override fun onBackPressed() {
        onBack()
    }

    fun onBack() {
        finish()
    }

    @SuppressLint("SetTextI18n")
    private fun setAddress() {
        val geocoder: Geocoder = Geocoder(this, Locale.getDefault())
        val addresses: List<android.location.Address>

        if (isNetworkAvailable(this)) {
            addresses = geocoder.getFromLocation(myLocation!!.latitude, myLocation!!.longitude, 1)

            if (addresses.isNotEmpty()) {
                val address = addresses[0].getAddressLine(0)

                autoCompleteEditText.setText(addresses[0].subLocality + ", " + addresses[0].locality + ", " + addresses[0].adminArea)
            } else
                SnackBar.show(mContext, true, getStr(mContext, R.string.str_network_error), false, "OK", null)
        }


    }



}
