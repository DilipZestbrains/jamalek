package com.zestbrains.jamalek.dashboard

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zestbrains.jamalek.R
import com.zestbrains.jamalek.model.salon.CategoriesResponse
import com.zestbrains.jamalek.utils.*

class FilterServicesAdapter(private val mActivity: Activity, private var mId: String) : RecyclerView.Adapter<FilterServicesAdapter.MyViewHolder>() {

    var mModel = ArrayList<CategoriesResponse.Data>()
    var isOpenPos: Int = -100

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: androidx.appcompat.widget.AppCompatTextView = itemView.findViewById(R.id.tvName)
        var rvMain: RelativeLayout = itemView.findViewById(R.id.cv_main)
        var tvProfile: androidx.appcompat.widget.AppCompatImageView = itemView.findViewById(R.id.tvProfile)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(parent.inflate(R.layout.rv_filter_service))
    }

    override fun onBindViewHolder(holder: MyViewHolder, @SuppressLint("RecyclerView") listPosition: Int) {


        if (mId == mModel[listPosition].id.toString()) {
            isOpenPos = listPosition
            mId=""
        }

        if (isOpenPos == listPosition) {
            holder.rvMain.background = getRes(mActivity, R.drawable.bg_time_border)
        } else {
            holder.rvMain.background = getRes(mActivity, R.drawable.bg_time_unselected_border)
        }

        holder.rvMain.setSafeOnClickListener {

            Prefs.setValue(mActivity, AppConstant.FILTER_CATEGORY, "")
            isOpenPos = if (isOpenPos == listPosition) {
                -100
            } else {
                listPosition
            }
            notifyDataSetChanged()
        }
        holder.tvName.text = mModel[listPosition].name.toString()

        Glide.with(mActivity).load(mModel[listPosition].image).placeholder(getRes(mActivity, R.drawable.img_placeholder))
                .error(getRes(mActivity, R.drawable.img_placeholder)).into(holder.tvProfile)
    }

    fun addAll(mData: ArrayList<CategoriesResponse.Data>) {

        mModel.addAll(mData)
        notifyItemRangeInserted(itemCount, mData.size - 1)
    }

    override fun getItemCount(): Int {
        return mModel.size
    }

    fun setSelected(mId: String) {
        if (mId.isNotEmpty()) {
            for (i in 0 until mModel.size) {
                if (mModel[i].id == mId) {
                    isOpenPos == i
                    notifyDataSetChanged()
                    break
                }
            }
        }
    }

    fun getCategoryId(): String {
        return if (isOpenPos != -100)
            mModel[isOpenPos].id.toString()
        else ""
    }

}